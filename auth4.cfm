<html>
<head>
<title>Survey Application, Neill Watson</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfinclude template="ga.js">
</head>
<!---
<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="login.cfm">
	
</cfif>--->
<body link="#0000FF" vlink="#0000FF" alink="#0000FF">
<table width="100%" border="0">
  <tr valign="top"> 
    <td colspan="2"> <h2><font color="#0000FF">WELCOME TO THE RESEARCH SITE OF<br>
        NEILL WATSON, PhD<br>
        COLLEGE OF WILLIAM AND MARY</font><br>
       <font color="#0000FF"><em>for Authorized Researchers</em></font></h2></td>
  </tr>
  <tr> 
    <td colspan="2"><h4><font color="#FF0000"><br>
        <script language="JavaScript1.2">
<!-- Original:  Craig Lumley -->
<!-- Web Site:  http://www.craiglumley.co.uk -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
<!-- Begin
function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
  var rv = -1; // Return value assumes failure.
  if (navigator.appName == 'Microsoft Internet Explorer')
  {
    var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
  }
  return rv;
}
function checkVersion()
{
  var msg = "The questionnaires must be used with Internet Explorer v. 7 or above � not with any other browsers � in order to avoid scrolling and losing the view of the instructions and scale anchors at the tops of the pages.<br>";
  var ver = getInternetExplorerVersion();

  if ( ver > -1 )
  {
    if ( ver >= 7.0 ) 
      msg = "";
  }
  //alert( msg );
  if (msg != "") { 
  	document.write(msg);
  }
}

function fullScreen(theURL) {
window.open(theURL, '', 'fullscreen=yes,scrollbars=yes,left=0,top=0,status=no,toolbar=no,location=no,menubar=no,titlebar=no');
}
//  End -->
checkVersion();
var bestwidth = 1024;
var bestheight = 768;
if (screen.width < bestwidth || screen.height < bestheight) {
msg = "<hr>The surveys on this site are intended to be administered on systems with a screen resolution of at least"
+ bestwidth + "x" + bestheight + ". Your"
+ " screen resolution is " + screen.width + "x"
+ screen.height + ".  Please change your screen resolution "
+ "if possible.  You will not see this message if you screen is set to the proper resolution.";
document.write(msg);
}
//  End -->
</script>
        </font></h4></td>
  </tr>

  <tr> 
    <td colspan="2">
      <hr></td>
  </tr>
   </tr>
    <td align="center" valign="top"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle">
    <!---<h3><a href="javascript:fullScreen('pc_cc_am_imp_ref_users/login.cfm');">Self-Discrepancy Questionnaires - Researcher Log In</a></h3>--->
    <h3>Self-Discrepancy Questionnaires</a></h3>
    <blockquote>The questionnaires are designed for full-screen mode in Internet Explorer versions 7 and 8 in order to eliminate vertical scrolling and losing the view of the instructions and scale anchors at the tops of the questionnaire pages. They are not designed for full-screen mode in other browsers. The full-screen pages can be closed by pressing the Alt and F4 keys simultaneously. <br><br>
      (1) For an introduction and downloadable instructions for administering and scoring the Self-Discrepancy Questionnaires, click:<br>
        <a href="pc_cc_am_imp_ref_users/introduction.cfm">Introduction and Instructions</a>
    <br><br>
    (2) If you are unfamiliar with zip files and/or shortcut files, you may find these instructions helpful: <br>
    <a href="Download_Instructions.pdf">Instructions for Downloading Self-Discrepancy Questionnaires � Zip File with Shortcut</a>
        <!--- <a href="javascript:fullScreen('pc_cc_am_imp_ref_users/login.cfm');">this link</a>.--->
        <br><br>
        (3) To download the zip file containing the link to the questionnaires, click:<br>
        <a href="Self-Discrepancy_Research.zip">Download Self-Discrepancy Questionnaires - Zip File with Shortcut</a>
        <br><br>
      <div style="font-size: 11px"></div>
    </blockquote>
    </td>
  </tr>
  <tr> 
    <td colspan="2"><hr></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><font size="1">Copyright 2004<br>
      Neill Watson, Ph.D.<br>
      College of William &amp; Mary<br>
      <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font></td>
  </tr>
</table>

</body>
</html>
