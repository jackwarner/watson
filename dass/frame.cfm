
<html>
<head>
<title>Demographic Questionnaire</title>
</head>
<cfset myTable = "dass_individual">
<cfset mySource = "watson">

<cfif isdefined("URL.num")>
	<cfset myIndex = #URL.num#>
<cfelse>
	<cfset myIndex = 0>
</cfif>

<cfif isdefined("URL.myTo")>
	<cfset myInclude = #URL.myTo#>
<cfelse>
	<cfset myInclude = 1>
</cfif>

<cfset myQuest = "3">

<cfif isdefined("URL.myID")>
	<cfset myID = #URL.myID#>
<cfelse>
	<cfset myID = -1>
</cfif>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 3>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="20"> 
        <h5> 
          <cfif #myIndex# EQUAL 0>Introduction for Researcher
		  <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	 <cfoutput>Page #myIndex# of #myPages#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        <cfif myIndex EQUAL 0>
          <h3>
<cfelse>
          </h3>
        </cfif></td>
    </tr>
    <tr> 
      <td height="21"> 
	  <cfif myIndex EQUAL 0>
		   <h3>Please enter the participant code and gender, then press the button marked 'Begin the Questionnaire.' </h3>
		   <h4><blockquote>
		     <h5><em>Please note that both fields are required, and that the participant code must be a 3 digit numerical input with leading zeros, if necessary (e.g., you would enter 002 for participant 2).</em></h5>
		   </blockquote> </h4>
		   <hr>
	  </cfif>
      </td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
        <cfif myIndex EQUAL 0>
          <cfform action="im.cfm">
		  <h4>&nbsp;</h4>


            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant code:</strong></td>
                  <td width="47%"><input name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
				<!---
				(a) Set it up so that an introductory page asks the researcher for the participant number and 			                gender (female = 1, male = 2).
				--->
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female</td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td>&nbsp;                  </td>
                </tr>
              </table>
            <div align="center">
              <input name="submit" type="submit" value="Begin the Questionnaire">
		    </div>
          </blockquote>
          
          </cfform>
          <hr>
          <font size="1"></font> 
        </cfif> </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	
	  </td>
    </tr>
	  <cfif #myIndex# EQ 0>
	  <tr valign="top">
     	 <td><font size="1">Copyright 2006<br>
			Neill Watson, Ph.D.<br>
			College of William &amp; Mary<br>
			<a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font>
       	 </td>
      </tr>
	  </cfif>
  </table>
</center>
</body>
</html>
