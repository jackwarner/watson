<!--- use cfsetting to block output of HTML 
outside of cfoutput tags --->
<cfsetting enablecfoutputonly="Yes">
<!--- set vars for special chars --->
<cfset TabChar = ",">
<cfset NewLine = "#Chr(13)##Chr(10)#">
<!--- set content type to invoke Excel --->
<cfcontent type="application/msexcel">
<!--- suggest default name for XLS file --->
<!--- use "Content-Disposition" in cfheader for 
Internet Explorer  --->
<cfheader name="Content-Disposition" value="filename=pc_cc_am_imp_ref.csv">
<!---
PC#delim# CC#delim# AM#delim# Ref#delim# Imp#delim# IM#delim# BDI#delim# BAI#delim# DASS#delim# STAIT#delim# CESD (MySQL)
--->
<CFQUERY DATASOURCE="watson" NAME="ranks">
SELECT *
FROM pc_cc_am_imp_ref_users 
WHERE UserId = #URL.myUser#
ORDER BY Id
</CFQUERY>
<body>
<cfset delim = #TabChar#>

	<cfset myDataBase = "ParticipantCode" & delim & "TakenOn" & delim & "Gender" & delim & "Age" & delim & "Order">
	
	<cfloop index="pcrs" from="1" to="36">
		<cfset myDataBase = myDatabase & delim & "PCRS" & #pcrs#>
	</cfloop>
	<cfloop index="pcrs" from="1" to="36">
		<cfset myDataBase = myDatabase & delim & "PCIS" & #pcrs#>
	</cfloop>
	<cfloop index="pcrs" from="1" to="36">	
		<cfset myDataBase = myDataBase & delim & "PCOS" & #pcrs#>
	</cfloop>
	<cfloop index="pcrs" from="1" to="28">
		<cfset myDataBase = myDataBase & delim &"CCRS" & #pcrs#>
	</cfloop>
    <cfloop index="pcrs" from="1" to="28">
		<cfset myDataBase = myDataBase & delim & "CCIS" & #pcrs#>
	</cfloop>
	<cfloop index="pcrs" from="1" to="28">
		<cfset myDataBase = myDataBase & delim & "CCOS" & #pcrs#>
	</cfloop>
	<cfset myDataBase = myDataBase & delim & "AMRI" & delim & "AMRO" & delim & "ImpI1" & delim & "ImpI2" & delim & "ImpI3" & delim & "ImpO1" & delim & "ImpO2" & delim & "ImpO3">
	<cfset myDataBase = myDataBase & delim & "Mother" & delim & "Father" & delim & "Sibling(s)" & delim & "Friends" & delim & "Close Friend(s)" & delim & "Girlfriend" & delim & "Boyfriend" & delim & "Spouse" & delim & "Peers" & delim & "Society" & delim & "Others">
	<cfset myDataBase = myDataBase & NewLine>
	<cfoutput>#myDataBase#</cfoutput>
	  <CFOUTPUT QUERY="ranks">#Code##delim##CreatedOn##delim##Gender##delim##Age##delim##BigOrder##delim##ranks.rr1##delim##ranks.rr2##delim##ranks.rr3##delim##ranks.rr4##delim##ranks.rr5##delim##ranks.rr6##delim##ranks.rr7##delim##ranks.rr8##delim##ranks.rr9##delim##ranks.rr10##delim##ranks.rr11##delim##ranks.rr12##delim##ranks.ri1##delim##ranks.ri2##delim##ranks.ri3##delim##ranks.ri4##delim##ranks.ri5##delim##ranks.ri6##delim##ranks.ri7##delim##ranks.ri8##delim##ranks.ri9##delim##ranks.ri10##delim##ranks.ri11##delim##ranks.ri12##delim##ranks.ro1##delim##ranks.ro2##delim##ranks.ro3##delim##ranks.ro4##delim##ranks.ro5##delim##ranks.ro6##delim##ranks.ro7##delim##ranks.ro8##delim##ranks.ro9##delim##ranks.ro10##delim##ranks.ro11##delim##ranks.ro12##delim##ranks.ir1##delim##ranks.ir2##delim##ranks.ir3##delim##ranks.ir4##delim##ranks.ir5##delim##ranks.ir6##delim##ranks.ir7##delim##ranks.ir8##delim##ranks.ir9##delim##ranks.ir10##delim##ranks.ir11##delim##ranks.ir12##delim##ranks.ii1##delim##ranks.ii2##delim##ranks.ii3##delim##ranks.ii4##delim##ranks.ii5##delim##ranks.ii6##delim##ranks.ii7##delim##ranks.ii8##delim##ranks.ii9##delim##ranks.ii10##delim##ranks.ii11##delim##ranks.ii12##delim##ranks.io1##delim##ranks.io2##delim##ranks.io3##delim##ranks.io4##delim##ranks.io5##delim##ranks.io6##delim##ranks.io7##delim##ranks.io8##delim##ranks.io9##delim##ranks.io10##delim##ranks.io11##delim##ranks.io12##delim##ranks.or1##delim##ranks.or2##delim##ranks.or3##delim##ranks.or4##delim##ranks.or5##delim##ranks.or6##delim##ranks.or7##delim##ranks.or8##delim##ranks.or9##delim##ranks.or10##delim##ranks.or11##delim##ranks.or12##delim##ranks.oi1##delim##ranks.oi2##delim##ranks.oi3##delim##ranks.oi4##delim##ranks.oi5##delim##ranks.oi6##delim##ranks.oi7##delim##ranks.oi8##delim##ranks.oi9##delim##ranks.oi10##delim##ranks.oi11##delim##ranks.oi12##delim##ranks.oo1##delim##ranks.oo2##delim##ranks.oo3##delim##ranks.oo4##delim##ranks.oo5##delim##ranks.oo6##delim##ranks.oo7##delim##ranks.oo8##delim##ranks.oo9##delim##ranks.oo10##delim##ranks.oo11##delim##ranks.oo12##delim#<cfquery datasource="watson" name="ranks2">Select * from pc_cc_am_imp_ref_users_supplemental where FK = #ranks.Id#</cfquery>#ranks2.rr1##delim##ranks2.rr2##delim##ranks2.rr3##delim##ranks2.rr4##delim##ranks2.rr5##delim##ranks2.rr6##delim##ranks2.rr7##delim##ranks2.rr8##delim##ranks2.rr9##delim##ranks2.rr10##delim##ranks2.rr11##delim##ranks2.rr12##delim##ranks2.rr13##delim##ranks2.rr14##delim##ranks2.rr15##delim##ranks2.rr16##delim##ranks2.rr17##delim##ranks2.rr18##delim##ranks2.rr19##delim##ranks2.rr20##delim##ranks2.rr21##delim##ranks2.rr22##delim##ranks2.rr23##delim##ranks2.rr24##delim##ranks2.rr25##delim##ranks2.rr26##delim##ranks2.rr27##delim##ranks2.rr28##delim##ranks2.ir1##delim##ranks2.ir2##delim##ranks2.ir3##delim##ranks2.ir4##delim##ranks2.ir5##delim##ranks2.ir6##delim##ranks2.ir7##delim##ranks2.ir8##delim##ranks2.ir9##delim##ranks2.ir10##delim##ranks2.ir11##delim##ranks2.ir12##delim##ranks2.ir13##delim##ranks2.ir14##delim##ranks2.ir15##delim##ranks2.ir16##delim##ranks2.ir17##delim##ranks2.ir18##delim##ranks2.ir19##delim##ranks2.ir20##delim##ranks2.ir21##delim##ranks2.ir22##delim##ranks2.ir23##delim##ranks2.ir24##delim##ranks2.ir25##delim##ranks2.ir26##delim##ranks2.ir27##delim##ranks2.ir28##delim##ranks2.or1##delim##ranks2.or2##delim##ranks2.or3##delim##ranks2.or4##delim##ranks2.or5##delim##ranks2.or6##delim##ranks2.or7##delim##ranks2.or8##delim##ranks2.or9##delim##ranks2.or10##delim##ranks2.or11##delim##ranks2.or12##delim##ranks2.or13##delim##ranks2.or14##delim##ranks2.or15##delim##ranks2.or16##delim##ranks2.or17##delim##ranks2.or18##delim##ranks2.or19##delim##ranks2.or20##delim##ranks2.or21##delim##ranks2.or22##delim##ranks2.or23##delim##ranks2.or24##delim##ranks2.or25##delim##ranks2.or26##delim##ranks2.or27##delim##ranks2.or28##delim##ranks.ri_abstract##delim##ranks.ro_abstract##delim##ranks.is1##delim##ranks.is2##delim##ranks.is3##delim##ranks.os1##delim##ranks.os2##delim##ranks.os3##delim##ranks.mother##delim##ranks.father##delim##ranks.sibling##delim##ranks.friend##delim##ranks.closefriend##delim##ranks.Girlfriend##delim##ranks.Boyfriend##delim##ranks.Spouse##delim##ranks.Peers##delim##ranks.Society##delim#"#Replace(ranks.Others,","," ","All")#"#NewLine#</CFOUTPUT>
	  