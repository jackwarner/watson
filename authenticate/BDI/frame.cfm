
<html>
<head>
<title>BDI-II</title>
</head>
<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="login.cfm">
	
</cfif>

<cfinclude template="questions.cfm">

<cfset myIndex = #URL.num#>
<cfset myTable = "survey">
<cfset myID = #URL.myID#>


<cfquery name="Getroi" datasource="roi">
 		SELECT numpick, BigOrder FROM survey WHERE Id=#URL.myId#
</cfquery>

<cfset myQuest = "9">
	<cfif Getroi.BigOrder EQ 2 OR Getroi.BigOrder EQ 3 OR Getroi.BigOrder EQ 4 OR Getroi.BigOrder EQ 6 OR Getroi.BigOrder EQ 9 OR Getroi.BigOrder EQ 11>
			<cfset myQuest = "8">
	</cfif>
	
	
<cfset myInclude = #URL.myTo# >
<!--- If this is fer real, go ahead and update the database --->

<body><center>

  <table width="585" height="315" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="17" colspan="8"> <h5> <cfif #myIndex# LT 22>
	  Questionnaire <cfoutput>#myQuest# of 9.  Page #myIndex#</cfoutput> of 21.</cfif></h5></td>
    </tr>
    <tr> 
      <td height="10" colspan="8" valign="middle"> 
        <cfif myIndex EQUAL 0>
	      <p><strong>Instructions:</strong> This questionnaire consists of 21 groups 
            of statements. Please read each group of statements carefully, and 
            then pick out the <strong>one statement </strong>in each group that 
            best describes the way you have been feeling during the <strong>past 
            two weeks, including today</strong>. Select the radio box beside the 
            statement you have picked. If several statements in the group seem 
            to apply equally well, select the highest number for that group. Be 
            sure that you do not choose more than one statement for any group, 
            including item 16 (Changes in Sleeping Pattern) or Item 18 (Changes 
            in Appetite). </p>
	  
	 
	<cfelse>
        </cfif></td>
    </tr>
    <tr> 
      <td height="21" colspan="8"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td height="25" colspan="8">
	  <cfif myIndex EQUAL 0>
	  	<cfform action="frame.cfm?num=1&myID=1&mySkip=x">
          Participant 's 3 digit code: 
          <cfinput type="text" name="Code" maxlength="3">
		<input type="hidden" name="Code_required" value="Please enter a 3 digit code.">
		<input type="submit" value="Begin the Survey">
		</cfform>
	  <cfelseif myIndex LESS THAN 22>
        <h3><cfoutput>#myIndex#. #aTitles[myIndex]#</cfoutput></h3></td>
	  </cfif>
    </tr>
    <tr valign="top">
      <td height="46" colspan="8">
	 <cfif myIndex EQUAL 1>
	 	<cfif #URL.mySkip# EQ "x">
	 		<cfif Len(#Form.Code#) NOT EQUAL 3>
				<a href="javascript:history.go(-1)">Please enter a 3 digit researcher code.  Click to go back.</a>
			<cfelse>
				<cflock name="#CreateUUID()#" timeout="20">
				<cftransaction>
				  
				<cfquery name="myQ" datasource="roi">
				INSERT INTO #myTable#(Code) VALUES ('#Form.Code#')
				</cfquery>
				
				<CFQUERY name="getMaxID" datasource="roi">
				SELECT Max(ID) AS myID
				FROM #myTable#
				</CFQUERY>
				
				 </cftransaction>
				</cflock>
				<cfset myID = #getMaxID.myID#>
				<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
				
				<cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x&myTo=x">
				  <cfinclude template="#myIndex#.cfm">&nbsp;</td>
				</cfform>
			</cfif>
		<cfelse>	
				<cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x&myTo=x">
				  <cfinclude template="#myIndex#.cfm">&nbsp;</td>
				</cfform>
		
		</cfif>
	<cfelseif myIndex GREATER THAN 1>
	 	
		  <cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&myTo=#myIndex+1#&mySkip=x">
			<cfinclude template="#myIndex#.cfm">&nbsp;</td>
		  </cfform>
		  
	</cfif>
    </tr>
    
    <tr valign="top"> 
      <td width="48">&nbsp;</td>
      <td width="537" colspan="7"><cfif myIndex LESS THAN 22>
	   <div align="left"><font size="1"><em>
	  Beck Depression 
          Inventory-Second Edition.</em> Copyright &copy; 1996 by Aaron T. Beck. 
          Adapted and reproduced by permission of the publisher, Harcourt Assessment, 
          Inc. All rights reserved.<br>
          <br>
          <em>&quot;Beck Depression Inventory&quot;</em> and <em>&quot;BDI&quot; 
          </em>are trademarks of Harcourt Assessment, Inc registered in the United 
          States of America and/or other jurisdictions.</font></div>
		  </cfif>
		  </td>
    </tr>
  </table>
</center>
</body>
</html>
