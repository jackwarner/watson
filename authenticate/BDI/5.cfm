<!--- variable dSource found in questions.cfm --->
<cfquery name="bdi" datasource="#dSource#">
<cfset mUp = myIndex - 1>
<!--- From.qX where x should be one less than current file Y.cfm --->
<cfset myUpdate = "q" & mUp>
UPDATE #myTable#
	SET #myUpdate# = #Form.q4#
	WHERE ID = #myID#
</cfquery>

<table width="585" height="20" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="73" height="19" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="0">
      0</strong></td>
    <td colspan="7" bgcolor="#FFFFCC">
	<strong>
	I don't feel particularly guilty.
	</strong></td>
  </tr>
  <tr> 
    <td height="19"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1 </strong></td>
    <td colspan="7"><strong>
	I feel guilty over many things I have done or should have done.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2 </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong> I feel quite guilty most of the 
      time. </strong></td>
  </tr>
  <tr> 
    <td height="19"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3 </strong></td>
    <td colspan="7"><strong>
	I feel guilty all of the time.
	</strong>
	<input type="hidden" name="<cfoutput>q#myIndex#</cfoutput>_required" value= "<cfoutput>#ERROR#</cfoutput>">
	</td>
  </tr>
    <tr> 
    <td height="19"><strong> 
      &nbsp;</td>
    <td colspan="7"><strong><input type="submit" value="Continue"></strong></td>
  </tr>
</table>
