
<html>
<head>
<title>RIOPC</title>
</head>
<cfinclude template="datasource.cfm">
<cfset myTable = "survey2">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 21>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="17"> <h5>
      <cfif #myIndex# EQUAL 0>Introduction for Researcher
		  <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	  Page <cfoutput>#myIndex# of #myPages#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td height="10" valign="middle"> 
        <cfif myIndex EQUAL 0>
          <h3>This program allows a researcher to 
            measure the respondent's REAL, IDEAL, and OUGHT (other) components 
            of self-concept.</h3>
          <h3>The program<br>
            </h3><blockquote>
              <h3>1. elicits six personality characteristics for each of 
              these three components of self-concept,<br>
              2. elicits the opposite of each of the 18 characteristics, and<br>
              3. obtains ratings of all 36 characteristics in response to instructions 
              for each of the three components of self-concept. <BR>
              <BR>
              These characteristics 
              are presented in the same random order for ratings for each of the 
              three components of self-concept.</h3>
            </blockquote>
            <cfelse></p>
        </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td> <cfif myIndex EQUAL 0>
          <cfform action="frame.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="58%"><!---  <strong>Participant code:</strong>---></td>
                  <td width="42%"><input name="Code" type="hidden" value="999" maxlength="3"></td>
                </tr>
                <tr>
                  <td><!--- <strong>Choose from the following elicitation orders:</strong> --->&nbsp;</td>
                  <td>	<!---  <select name="Order">
						  <option value="1">R/I/O
						  <option value="2">R/O/I
						  <option value="3">O/R/I
						  <option value="4">O/I/R
						  <option value="5">I/O/R
						  <option value="6">I/R/O
						</select>--->&nbsp;<input type="hidden" value="1" name="Order"></td>
                </tr>
                <tr> 
                  <td><!--- <strong>Participant gender: </strong> ---></td>
                  <td><!--- <input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female ---><input type="hidden" name="Gender" value="N"> </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="2"><div align="center">
                      <input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                      <input type="hidden" name="Gender_required" value="Please enter a gender.">                    
                  </div></td>
                </tr>
              </table>
                          </blockquote>
         
          <div align="center">
            <input name="submit" type="submit" value="Begin the Survey">
		 </cfform>
          </div>
          <hr>
          </cfif> 
	  </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
			  <cftransaction>
				<cfquery name="myQ" datasource="#dataSource#">
				INSERT INTO #myTable#(Code, Gender, numpick) VALUES ('#Form.Code#', '#Form.Gender#', '#Form.Order#') 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="#dataSource#">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
			 
			<cfinclude template="#myIndex#.cfm"> &nbsp;
		  
		  
	  </cfif> 
	  	</td>
	  </tr>
	

	  
	  
	  <cfelseif #myIndex# GREATER THAN 1>     
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	  </td>
	  </tr>
	  <cfif #myIndex# EQ 0>
	  <tr valign="top">
     	 <td><font size="1">Copyright 2004<br>
			Neill Watson, Ph.D.<br>
			College of William &amp; Mary<br>
			<a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font>
       	 </td>
      </tr>
	  </cfif>
  </table>
</center>
</body>
</html>
