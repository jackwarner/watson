
<html>
<head>
<title>Reference Person(s) for Ought Self</title>
</head>
<cfset myTable = "survey2">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 2>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="20"> 
        <h5> 
          <cfif #myIndex# EQUAL 0>Introduction for Researcher
	    <cfelseif #myIndex# GTE #myPages#>
	  <cfelse>
	  <cfoutput>Page 1 of 1.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        <cfif myIndex EQUAL 0>
          <h3>This questionnaire asks the person what person(s) he or she had in mind when answering the questionnaires about the ought self (the self as OTHERS think the person ought or should be).
              <cfelse>
          </h3>
      </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
        <cfif myIndex EQUAL 0>
          <cfform action="frame.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><!---  <strong>Participant code:</strong>---></td>
                  <td width="47%"><input name="Code" type="hidden" value="999" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><!--- <strong>Participant gender: </strong> ---></td>
                  <td><!---<input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female---><input type="hidden" value="N" name="Gender"> </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td>&nbsp;                  </td>
                </tr>
              </table>
            <div align="center">
            <input name="submit" type="submit" value="Begin the Survey">
          </div>
          </blockquote>
          
          
          </cfform>
          <hr>
          <font size="1"></font> 
        </cfif> </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
			  <cftransaction>
				<cfquery name="myQ" datasource="watson">
				INSERT INTO #myTable#(Code, Gender) VALUES ('#Form.Code#', '#Form.Gender#') 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="watson">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
			 
			<cfinclude template="#myIndex#.cfm"> &nbsp;
		  
		  
	  </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>     
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	  </td>
	  </tr>
	   <cfif #myIndex# EQ 0>
	  <tr valign="top">
     	 <td><font size="1">Copyright 2004<br>
			Neill Watson, Ph.D.<br>
			College of William &amp; Mary<br>
			<a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font>
       	 </td>
      </tr>
	  </cfif>
  </table>
</center>
</body>
</html>
