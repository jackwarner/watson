<cfsetting enablecfoutputonly="Yes">
<!--- set vars for special chars --->
<cfset TabChar = Chr(44)>
<cfset NewLine = Chr(13) & Chr(10)>
<!--- set content type to invoke Excel --->
<cfcontent type="application/msexcel">
<cfheader name="Content-Disposition" value="filename=chars_pc_cc_am_imp_ref_im_bdi_bai_dass_stait_cesd_mysql.csv">
<CFQUERY DATASOURCE="watson" NAME="ranks">
SELECT *
FROM latentvar ORDER BY Id
</CFQUERY>
<cfset delim = ",">
<cfset myDataBase = "ParticipantCode" & delim & "TakenOn">
<cfloop index="j" from="1" to="6">
    <cfset myDataBase = myDatabase & delim & "REAL" & #j#>
</cfloop>
<cfloop index="k" from="1" to="6">
    <cfset myDataBase = myDatabase & delim & "IDEAL" & #k#>
</cfloop>
<cfloop index="pcrs" from="1" to="6">
    <cfset myDataBase = myDatabase & delim & "OUGHT" & #pcrs#>
</cfloop>
<cfloop index="j" from="1" to="6">
    <cfset myDataBase = myDatabase & delim & "OP_REAL" & #j#>
</cfloop>
<cfloop index="k" from="1" to="6">
    <cfset myDataBase = myDatabase & delim & "OP_IDEAL" & #k#>
</cfloop>
<cfloop index="pcrs" from="1" to="6">
    <cfset myDataBase = myDatabase & delim & "OP_OUGHT" & #pcrs#>
</cfloop>
<!--- <cfset myDataBase = myDataBase & ",ImpI1,ImpI2,ImpI3,ImpO1,ImpO2,ImpO3<BR>"> --->
<cfset myDataBase = myDataBase & #NewLine#>
<cfprocessingdirective suppressWhiteSpace="true">
    <cfoutput>#myDataBase#</cfoutput>
    <CFOUTPUT QUERY="ranks">
        #Code#,#CreatedOn#,#real1#,#real2#,#real3#,#real4#,#real5#,#real6#,#ideal1#,#ideal2#,#ideal3#,#ideal4#,#ideal5#,#ideal6#,#ought1#,#ought2#,#ought3#,#ought4#,#ought5#,#ought6#,#opr1#,#opr2#,#opr3#,#opr4#,#opr5#,#opr6#,#opid1#,#opid2#,#opid3#,#opid4#,#opid5#,#opid6#,#opou1#,#opou2#,#opou3#,#opou4#,#opou5#,#opou6##NewLine#
    </CFOUTPUT>
</cfprocessingdirective>
