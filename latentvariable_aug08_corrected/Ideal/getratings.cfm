<%@LANGUAGE="VBSCRIPT" CODEPAGE="1252"%>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body><center>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
      <td height="50%">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="center"><div align="center">
          <table width="585" height="390" border="0" cellpadding="0" cellspacing="0">
            <tr> 
              <td colspan="8"><h5> IDEAL SELF: Page 3 of 4</h5></td>
            </tr>
            <tr> 
              <td colspan="8"><h3>Please consider the following statements about 
                  your ideal self (yourself as YOU would like to be in your own 
                  eyes). For each statement, indicate how true it is for you.</h3></td>
            </tr>
            <tr> 
              <td width="50%">&nbsp;</td>
              <td colspan="7"><div align="center"><img src="../../Copy of latentvariable/trues.gif" width="291" height="61"></div></td>
            </tr>
            <tr bgcolor="#FFFFCC"> 
              <td width="262" rowspan="2">My ideal self is important in how I 
                think about myself.</td>
              <td><div align="center"><font size="2">1</font></div></td>
              <td><div align="center"><font size="2">2</font></div></td>
              <td><div align="center"><font size="2">3</font></div></td>
              <td><div align="center"><font size="2">4</font></div></td>
              <td><div align="center"><font size="2">5</font></div></td>
              <td><div align="center"><font size="2">6</font></div></td>
              <td><div align="center"><font size="2">7</font></div></td>
            </tr>
            <tr bgcolor="#FFFFCC"> 
              <td width="41"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
              <td width="41"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
              <td width="41"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
              <td width="41"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
              <td width="42"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
              <td width="41"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
              <td width="42"> <div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton" value="radiobutton1">
                  </font></div></td>
            </tr>
            <tr> 
              <td rowspan="2">My ideal self is important in how I feel about myself.</td>
              <td><div align="center"><font size="2">1</font></div></td>
              <td><div align="center"><font size="2">2</font></div></td>
              <td><div align="center"><font size="2">3</font></div></td>
              <td><div align="center"><font size="2">4</font></div></td>
              <td><div align="center"><font size="2">5</font></div></td>
              <td><div align="center"><font size="2">6</font></div></td>
              <td><div align="center"><font size="2">7</font></div></td>
            </tr>
            <tr> 
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
              <td><div align="center"> <font size="2"> 
                  <input type="radio" name="radiobutton2" value="radiobutton">
                  </font></div></td>
            </tr>
            <tr bgcolor="#FFFFCC"> 
              <td rowspan="2">My ideal self is important in what I decide to do.</td>
              <td><div align="center"><font size="2">1</font></div></td>
              <td><div align="center"><font size="2">2</font></div></td>
              <td><div align="center"><font size="2">3</font></div></td>
              <td><div align="center"><font size="2">4</font></div></td>
              <td><div align="center"><font size="2">5</font></div></td>
              <td><div align="center"><font size="2">6</font></div></td>
              <td><div align="center"><font size="2">7</font></div></td>
            </tr>
            <tr bgcolor="#FFFFCC"> 
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
              <td> <div align="center"> 
                  <input type="radio" name="radiobutton3" value="radiobutton">
                </div></td>
            </tr>
            <tr valign="top"> 
              <td>&nbsp;</td>
              <td colspan="7"> <div align="left"> 
                  <form action="../../Copy of latentvariable/Ideal/ending.cfm" method="post">
                    <input type="submit" name="Submit" value="Continue to Page 4 of 4">
                  </form>
                </div>
                <div align="center"></div>
                <div align="center"></div>
                <div align="center"></div>
                <div align="center"></div>
                <div align="center"></div>
                <div align="center"></div></td>
            </tr>
          </table>
        </div></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td height="50%">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  </center>
</body>
</html>
