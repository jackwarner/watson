<html>
<head>
<title>RIOPC</title>
</head>
<cfset myTable = "survey">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 21>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="17"> <h5>
          <cfif #myIndex# EQUAL 0>Introduction
		  <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	  Questionnaire 1 of 9.  Page <cfoutput>#myIndex-1# of #myPages-1#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td height="10" valign="middle"> 
        <cfif myIndex EQUAL 0>
          <p>This program allows a researcher to 
            measure the respondent's REAL, IDEAL, and OUGHT (other) components 
            of self-concept.</p>
          <p>The program<br>
            <blockquote>1. elicits six personality characteristics for each of 
              these three components of self-concept,<br>
              2. elicits the opposite of each of the 18 characteristics, and<br>
              3. obtains ratings of all 36 characteristics in response to instructions 
              for each of the three components of self-concept. <BR><BR>These characteristics 
              are presented in the same random order for ratings for each of the 
              three components of self-concept.</blockquote>
            <cfelse></p>
          </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td> <cfif myIndex EQUAL 0>
          <cfform action="../../Copy of latentvariable/RIOPC/frame.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="58%"><strong>Participant code:</strong></td>
                  <td width="42%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr>
                  <td><strong>Choose from the following elicitation orders:</strong>&nbsp;</td>
                  <td>	<select name="Order">
						  <option value="1">R/I/O
						  <option value="2">R/O/I
						  <option value="3">O/R/I
						  <option value="4">O/I/R
						  <option value="5">I/O/R
						  <option value="6">I/R/O
						</select>&nbsp;</td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1">Copyright 2004<br>
          Neill Watson, Ph.D.<br>
          College of William &amp; Mary<br>
          <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font> 
          <!---  <cfelseif myIndex LESS THAN 4>
        <h3><cfoutput>#myIndex#. #aTitles[myIndex]#</cfoutput></h3></td>--->
        </cfif> 
	  </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
			  <cftransaction>
				<cfquery name="myQ" datasource="roi">
				INSERT INTO #myTable#(Code, Gender, numpick) VALUES ('#Form.Code#', '#Form.Gender#', '#Form.Order#') 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="roi">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
			 
			<cfinclude template="#myIndex#.cfm"> &nbsp;
		  
		  
	  </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>     
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>