<cfinclude template="checkop2.cfm">

<cfif Len(myMsg) GT 0>
		<cfoutput>#myMsg#</cfoutput>
		&nbsp;
		Please <a href="javascript:history.back()">go back</a> and enter a different characteristic.
<cfelse>

<cfoutput>
<cfquery name="Updateideal" datasource="watson">
  UPDATE #myTable#
  SET 	opr3='#Form.opr3#',
    	opou6 ='#Form.opou6#',
    	opid5 ='#Form.opid5#',
    	opr5= '#Form.opr5#',
     	opou4= '#Form.opou4#',
     	opr2 = '#Form.opr2#',
    	opid1 = '#Form.opid1#',
     	opr4= '#Form.opr4#',
      	opou3 = '#Form.opou3#'
WHERE Id=#URL.myId#
</cfquery>
</cfoutput>

<cfquery name="Getroi" datasource="watson">
 SELECT numpick FROM #myTable# WHERE Id=#URL.myId#
</cfquery>

<cfif Getroi.numpick IS 1 OR Getroi.numpick IS 2> 
<cfset going="9">
</cfif>


<cfif Getroi.numpick IS 3 OR Getroi.numpick IS 4> 
<cfset going="11">
</cfif>

<cfif Getroi.numpick IS 5 OR Getroi.numpick IS 6> 
<cfset going="10">
</cfif>

<center>
  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="middle"> 
      <td width="50%" colspan="8"> <h3>Now, you are asked to use a numeric scale 
          to rate all the characteristics you listed according to each of the 
          following instructions.</h3></td>
    </tr>
    <tr valign="top">
      <td colspan="8" valign="middle">&nbsp;</td>
    </tr>
    <tr valign="top"> 
      <td height="116" colspan="8"> <ul>
          <li>
            <h3>Describe your REAL SELF - yourself as YOU see yourself in your 
              own eyes. </h3>
          </li>
          <li>
            <h3>Describe your IDEAL SELF - yourself as YOU would like to be in 
              your own eyes.</h3>
          </li>
          <li>
            <h3>Describe your OUGHT/SHOULD SELF - yourself as OTHERS think you 
              ought or should be.</h3>
          </li>
        </ul>
        <h3>You are asked for your own view. There are no right or wrong answers.</h3></td>
    </tr>
    <tr valign="top"> 
      <cfform action="frame_.cfm?num=#myIndex+1#&amp;myTo=#going#&amp;myID=#myID#">
        <td height="43">&nbsp;</td>
        <td colspan="7"> <div align="left"> <br> <input type="submit" name="Submit" value="Continue"> 
        </td>
      </cfform>
    </tr>
  </table>
</center>
</cfif>

