
<cfset myQuestions[1,1] = "Sadness">
<cfset myQuestions[2,1] = "Pessimism">
<cfset myQuestions[3,1] = "Past Failure">
<cfset myQuestions[4,1] = "Loss of Pleasure">
<cfset myQuestions[5,1] = "Guilty Feelings">
<cfset myQuestions[6,1] = "Punishment Feelings">
<cfset myQuestions[7,1] = "Self-Dislike">
<cfset myQuestions[8,1] = "Self-Criticalness">
<cfset myQuestions[9,1] = "Suicidal Thoughts or Wishes">
<cfset myQuestions[10,1] = "Crying">
<cfset myQuestions[11,1] = "Agitation">
<cfset myQuestions[12,1] = "Loss of Interest">
<cfset myQuestions[13,1] = "Indecisiveness">
<cfset myQuestions[14,1] = "Worthlessness">
<cfset myQuestions[15,1] = "Loss of Energy">
<cfset myQuestions[16,1] = "Changes in Sleeping Pattern">
<cfset myQuestions[17,1] = "Irritability">
<cfset myQuestions[18,1] = "Changes in Appetite">
<cfset myQuestions[19,1] = "Concentration Difficulty">
<cfset myQuestions[20,1] = "Tiredness or Fatigue">
<cfset myQuestions[21,1] = "Loss of Interest in Sex">
<!--- Q 1 Choices --->
<cfset myQuestions[1,2] = "I do not feel sad.">
<cfset myQuestions[1,3] = "I feel sad much of the time.">
<cfset myQuestions[1,4] = "I am sad all the time.">
<cfset myQuestions[1,5] = "I am so sad or unhappy that I can't stand it.">
<!--- Q 2 Choices --->
<cfset myQuestions[2,2] = "I am not discouraged about my future.">
<cfset myQuestions[2,3] = "I feel more discouraged about my future than I used to be.">
<cfset myQuestions[2,4] = "I do not expect things to work out for me.">
<cfset myQuestions[2,5] = "I feel my future is hopeless and will only get worse.">
<!--- Q 3 Choices --->
<cfset myQuestions[3,2] = "I do not feel like a failure.">
<cfset myQuestions[3,3] = "I have failed more than I should have.">
<cfset myQuestions[3,4] = "As I look back, I see a lot of failures.">
<cfset myQuestions[3,5] = "I feel I am a total failure as a person.">
<!--- Q 4 Choices --->
<cfset myQuestions[4,2] = "I get as much pleasure as I ever did from the things I enjoy.">
<cfset myQuestions[4,3] = "I don't enjoy things as much as I used to.">
<cfset myQuestions[4,4] = "I get very little pleasure from the things I used to enjoy.">
<cfset myQuestions[4,5] = "I can't get any pleasure from the things I used to enjoy.">
<!--- Q 5 Choices --->
<cfset myQuestions[5,2] = "I don't feel particularly guilty.">
<cfset myQuestions[5,3] = "I feel guilty over many things I have done or should have done.">
<cfset myQuestions[5,4] = "I feel quite guilty most of the time.">
<cfset myQuestions[5,5] = "I feel guilty all of the time.">
<!--- Q 6 Choices --->
<cfset myQuestions[6,2] = "I don't feel I am being punished">
<cfset myQuestions[6,3] = "I feel I may be punished.">
<cfset myQuestions[6,4] = "I expect to be punished.">
<cfset myQuestions[6,5] = "I feel I am being punished.">
<!--- Q 7 Choices --->
<cfset myQuestions[7,2] = "I feel the same about myself as ever.">
<cfset myQuestions[7,3] = "I have lost confidence in myself.">
<cfset myQuestions[7,4] = "I am disappointed in myself.">
<cfset myQuestions[7,5] = "I dislike myself.">
<!--- Q 8 Choices --->
<cfset myQuestions[8,2] = "I don't criticize or blame myself more than usual.">
<cfset myQuestions[8,3] = "I am more critical of myself than I used to be.">
<cfset myQuestions[8,4] = "I criticize myself for all of my faults.">
<cfset myQuestions[8,5] = "I blame myself for everything bad that happens.">
<!--- Q 9 Choices --->
<cfset myQuestions[9,2] = "I don't have any thoughts of killing myself.">
<cfset myQuestions[9,3] = "I have thoughts of killing myself, but I would not carry them out.">
<cfset myQuestions[9,4] = "I would like to kill myself.">
<cfset myQuestions[9,5] = "I would kill myself if I had the chance.">
<!--- Q 10 Choices --->
<cfset myQuestions[10,2] = "I don't cry anymore than I used to.">
<cfset myQuestions[10,3] = "I cry more than I used to.">
<cfset myQuestions[10,4] = "I cry over every little thing.">
<cfset myQuestions[10,5] = "I feel like crying, but I can't.">
<!--- Q 11 Choices --->
<cfset myQuestions[11,2] = "I am no more restless or wound up than usual.">
<cfset myQuestions[11,3] = "I feel more restless or wound up than usual.">
<cfset myQuestions[11,4] = "I am so restless or agitated that it's hard to stay still.">
<cfset myQuestions[11,5] = "I am so restless or agitated that I have to keep moving or doing something.">
<!--- Q 12 Choices --->
<cfset myQuestions[12,2] = "I have not lost interest in other people or activities.">
<cfset myQuestions[12,3] = "I am less interested in other people or things than before.">
<cfset myQuestions[12,4] = "I have lost most of my interest in other people or things.">
<cfset myQuestions[12,5] = "It's hard to get interested in anything.">
<!--- Q 13 Choices --->
<cfset myQuestions[13,2] = "I make decisions about as well as ever.">
<cfset myQuestions[13,3] = "I find it more difficult to make decisions than usual.">
<cfset myQuestions[13,4] = "I have much greater difficulty in making decisions than I used to.">
<cfset myQuestions[13,5] = "I have trouble making any decisions.">
<!--- Q 14 Choices --->
<cfset myQuestions[14,2] = "I do not feel I am worthless.">
<cfset myQuestions[14,3] = "I don't consider myself as worthwhile and useful as I used to.">
<cfset myQuestions[14,4] = "I feel more worthless as compared to other people.">
<cfset myQuestions[14,5] = "I feel utterly worthless.">
<!--- Q 15 Choices --->
<cfset myQuestions[15,2] = "I have as much energy as ever.">
<cfset myQuestions[15,3] = "I have less energy than I used to have.">
<cfset myQuestions[15,4] = "I don't have enough energy to do very much.">
<cfset myQuestions[15,5] = "I don't have enough energy to do anything.">
<!--- Q 16 Choices --->
<cfset myQuestions[16,2] = "I have not experienced any change in my sleeping pattern.">
<cfset myQuestions[16,3] = "I sleep somewhat more than usual.">
<cfset myQuestions[16,4] = "I sleep somewhat less than usual.">
<cfset myQuestions[16,5] = "I sleep a lot more than usual.">
<cfset myQuestions[16,6] = "I sleep a lot less than usual.">
<cfset myQuestions[16,7] = "I sleep most of the day.">
<cfset myQuestions[16,8] = "I wake up 1-2 hours early and can't get back to sleep.">
<!--- Q 17 Choices --->
<cfset myQuestions[17,2] = "I am no more irritable than usual.">
<cfset myQuestions[17,3] = "I am more irritable than usual.">
<cfset myQuestions[17,4] = "I am much more irritable than usual.">
<cfset myQuestions[17,5] = "I am irritable all the time.">
<!--- Q 18 Choices --->
<cfset myQuestions[18,2] = "I have not experienced any change in my appetite.">
<cfset myQuestions[18,3] = "My appetite is somewhat less than usual.">
<cfset myQuestions[18,4] = "My appetite is somewhat greater than usual.">
<cfset myQuestions[18,5] = "My appetite is much less than before.">
<cfset myQuestions[18,6] = "My appetite is much greater than usual.">
<cfset myQuestions[18,7] = "I have no appetite at all.">
<cfset myQuestions[18,8] = "I crave food all the time.">
<!--- Q 19 Choices --->
<cfset myQuestions[19,2] = "I can concentrate as well as ever.">
<cfset myQuestions[19,3] = "I can't concentrate as well as usual.">
<cfset myQuestions[19,4] = "It's hard to keep my mind on anything for very long.">
<cfset myQuestions[19,5] = "I find I can't concentrate on anything.">
<!--- Q 20 Choices --->
<cfset myQuestions[20,2] = "I am no more tired or fatigued than usual.">
<cfset myQuestions[20,3] = "I get more tired or fatigued more easily than usual.">
<cfset myQuestions[20,4] = "I am too tired or fatigued to do a lot of the things I used to do.">
<cfset myQuestions[20,5] = "I am too tired or fatigued to do most of the things I used to do.">
<!--- Q 21 Choices --->
<cfset myQuestions[21,2] = "I have not noticed any recent change in my interest in sex.">
<cfset myQuestions[21,3] = "I am less interested in sex than I used to be.">
<cfset myQuestions[21,4] = "I am much less interested in sex now.">
<cfset myQuestions[21,5] = "I have lost interest in sex completely.">