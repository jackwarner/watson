<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfset databaseLive = false>
<cfif isDefined("URL.myID")>
	<cfset databaseLive = true>
</cfif>

<cfset myTable = "latentvar">
<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I was bothered by things that usually don't bother me.">
	<cfset myQuestions[2] = "I did not feel like eating; my appetite was poor.">
	<cfset myQuestions[3] = "I felt that I could not shake off the blues even with help from my family or friends.">
    <cfset myQuestions[4] = "I felt that I was just as good as other people.">
	<cfset myQuestions[5] = "I had trouble keeping my mind on what I was doing.">
	<cfset myQuestions[6] = "I felt depressed.">
	<cfset myQuestions[7] = "I felt that everything I did was an effort.">
	<cfset myQuestions[8] = "I felt hopeful about the future.">
	<cfset myQuestions[9]= "I thought my life had been a failure.">
	<cfset myQuestions[10]= "I felt fearful.">
	<cfset myQuestions[11]= "My sleep was restless.">
	<cfset myQuestions[12]= "I was happy.">
	<cfset myQuestions[13]= "I talked less than usual.">
	<cfset myQuestions[14]= "I felt lonely.">
	<cfset myQuestions[15]= "People were unfriendly.">
	<cfset myQuestions[16]= "I enjoyed life.">
	<cfset myQuestions[17]= "I had crying spells.">
	<cfset myQuestions[18]= "I felt sad.">
	<cfset myQuestions[19]= "I felt that people disliked me.">
	<cfset myQuestions[20]= "I could not get ""going.""">

<cfset page = 1>
<cfset qPerPage = 7>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<!---
For compatibility with older surveys, set myIndex and myPages --->
<cfset myIndex = page + 1>
<cfset myPages = totalPages + 1>
<!--- end compability measures --->
<cfset title = "CESD">
<cfset instructions = "Below is a list of some of the ways you may have felt or behaved.  Please indicate how often">
<cfset instructions = instructions & " you have felt this way during the past week by choosing the appropriate answer.">
<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>
<!---
	* Ugly hack here because for some reason IM1 isnt updated with CFUPDATE
--->
<cfif page EQ 2 AND databaseLive>
	<cfquery datasource="roi">
		UPDATE #myTable#_supplemental SET cesd1=#Form.cesd1# WHERE FK=#Form.FK#
	</cfquery>
</cfif>
<cfif page GT 1 AND databaseLive>
	<cfupdate datasource="roi" tablename="#myTable#_supplemental">
</cfif>
<cfif page GT totalPages AND databaseLive>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8">
	<cfinclude template="../../Copy of latentvariable/getcode_cesd.cfm">
	</td>
  </tr>
</table>
<cfelseif page GT totalPages AND NOT databaseLive>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8" align="center">
	<h3>Thank you for completing the questionnaire.</h3>
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="../../Copy of latentvariable/CESD/ew.js"></script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">
if (EW_this.cesd#i# && !EW_hasValue(EW_this.cesd#i#, "RADIO" )) {
	if (!EW_onError(EW_this, EW_this.cesd#i#, "RADIO", "Please enter a response for Question #i#."))
		return false;
}
</cfloop>
</cfoutput>
return true;
}
//-->
</script>

<form method="post" onSubmit="return EW_checkMyForm(this);">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  	<tr>
		<td align="right">
			<cfinclude template="../../Copy of latentvariable/progress.cfm">
		</td>
	</tr>
	<tr>
		<td><hr></td>
	</tr>
</table>
<cfoutput>
<table border="0" width="800" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td colspan="6"><h3><cfoutput>#instructions#</cfoutput></h3></td>
  </tr>
  <tr>
  	<td></td>
    <td></td>
	<cfloop from="1" to="4" index="m">
	<td bgcolor="##E1E1E1">
		<center><img src="#m#.png"></center> 
    </td>
	</cfloop>
  </tr>
 
 
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr <!---<cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>--->>
  	<td valign="top">#i#.</td>
	<td valign="top" width="400">#myQuestions[i]#</td>
	<cfloop from="1" to="4" index="j">
	<td valign="center" bgcolor="##E1E1E1">
	<font size="2"><center>#j#<BR>
	<input type="radio" name="cesd#i#" value="#j#" <cfif debug AND j EQ 1>checked</cfif>>
	</center></font>
	</td>
  </cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
    <td colspan="6" align="center">
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
	<cfif databaseLive>
	<input type="hidden" name="FK" value="<cfoutput>#URL.myID#</cfoutput>">
	</cfif>
	<input type="submit" name="Submit" value="   Continue   "> 
    </td>
  </tr>
</table>
</form>
</cfif>
</body>
</html>
