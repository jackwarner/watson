<!--- use cfsetting to block output of HTML 
outside of cfoutput tags --->
<cfsetting enablecfoutputonly="Yes">
<!--- set vars for special chars --->
<cfset TabChar = Chr(44)>
<cfset NewLine = Chr(13) & Chr(10)>
<!--- set content type to invoke Excel --->
<cfcontent type="application/msexcel">
<cfheader name="Content-Disposition" value="filename=dass_mysql.csv">
<CFQUERY DATASOURCE="watson" NAME="ranks">
SELECT *
FROM dass ORDER BY Id
</CFQUERY>
<cfset delim = #TabChar#>
<cfset myDataBase = "ParticipantCode" & delim & "TakenOn" & delim & "Gender">
<cfloop index="pcrs" from="1" to="42">
    <cfset myDataBase = myDataBase & delim & "DASS" & #pcrs#>
</cfloop>
<cfset myDataBase = myDataBase & NewLine>
<cfprocessingdirective suppressWhiteSpace="true">
    <cfoutput>#myDataBase#</cfoutput>
    <CFOUTPUT QUERY="ranks">
        #Code##delim##datecreated##delim##Gender#
        <cfloop index="pcrs" from="1" to="42">#delim##Evaluate("ranks.dass#pcrs#")#</cfloop>
        #NewLine#</CFOUTPUT>
</cfprocessingdirective>
