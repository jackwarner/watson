
<cfoutput>
<cfquery name="Updaterat" datasource="roi">
  UPDATE #myTable#
  SET stai1 ='#Form.stai1#',
	  stai2 ='#Form.stai2#',
	  stai3 ='#Form.stai3#',
	  stai4 ='#Form.stai4#',
	  stai5 ='#Form.stai5#',
	  stai6 ='#Form.stai6#',
	  stai7 ='#Form.stai7#',
	  stai8 ='#Form.stai8#',
	  stai9 ='#Form.stai9#',
	  stai10 ='#Form.stai10#',
	  stai11 ='#Form.stai11#'
WHERE Id=#URL.myId#
</cfquery>
</cfoutput>

<cfform action="frame.cfm?num=#myIndex+1#&myTo=end&myId=#myID#" method="post">
<center>
   <table width="585" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="9"><div align="left"> <strong>A number of statements which people have used to describe 
            themselves are given below. Read each statement and then select the 
            appropriate number to the right of the statement to indicate how you 
            <em>generally</em> feel. There are no right or wrong answers. Do not 
            spend too much time on any one statement but give the answer which 
            seems to describe how you generally feel. </strong> </div>          </td>
      </tr>
	</table>
	<table width="585" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="371" colspan="2">&nbsp;</td>
        <td colspan="8"><div align="center"><img src="stai.gif" width="211" height="106"></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="top">12.</td>
        <td width="186" rowspan="2" valign="top">I lack self-confidence.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai12" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai12" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai12" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai12" value="4">
            <input type=hidden name="stai12_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="top">13.</td>
        <td width="186" rowspan="2" valign="top">I feel secure.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai13" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai13" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai13" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai13" value="4">
            <input type=hidden name="stai13_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="top">14.</td>
        <td width="186" rowspan="2" valign="top">I make decisions easily.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai14" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai14" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai14" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai14" value="4">
            <input type=hidden name="stai14_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="top">15.</td>
        <td width="186" rowspan="2" valign="top">I feel inadequate.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai15" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai15" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai15" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai15" value="4">
            <input type=hidden name="stai15_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="top">16.</td>
        <td width="186" rowspan="2" valign="top">I am content.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai16" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai16" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai16" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai16" value="4">
            <input type=hidden name="stai16_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="top">17.</td>
        <td width="186" rowspan="2" valign="top">Some unimportant thought 
        runs through my mind and bothers me.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai17" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai17" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai17" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai17" value="4">
            <input type=hidden name="stai17_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="top">18.</td>
        <td width="186" rowspan="2" valign="top">I take disappointments 
        so keenly that I can't put them out of my mind.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="middle"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="middle"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="middle"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="middle"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai18" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai18" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai18" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai18" value="4">
            <input type=hidden name="stai18_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="top">19.</td>
        <td width="186" rowspan="2" valign="top">I am a steady person.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="middle"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="middle"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="middle"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="middle"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai19" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai19" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai19" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai19" value="4">
            <input type=hidden name="stai19_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="top">20.</td>
        <td width="186" rowspan="2" valign="top">I get in a state of tension 
        or turmoil as I think over my recent concerns and interests.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai20" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai20" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai20" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai20" value="4">
            <input type=hidden name="stai20_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      
      <tr valign="middle"> 
        <td colspan="2"> 
          <blockquote> <br /><br />
            <div align="left"> <cfinclude template="cright.cfm">
            </blockquote></td>
        <td colspan="8" align="center"> 
          <input type="submit" name="Submit" value="Continue"></td>
      </tr>
    </table>
</center>
</cfform>
