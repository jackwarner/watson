<!--- variable dSource found in questions.cfm --->
<cfquery name="bdi" datasource="#dSource#">
<cfset mUp = myIndex - 1>
<!--- From.qX where x should be one less than current file Y.cfm --->
<cfset myUpdate = "q" & mUp>
UPDATE #myTable#
	SET #myUpdate# = '#Form.q15#'
	WHERE ID = #myID#
</cfquery>

<table width="585" height="20" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="73" height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="0">
      0</strong></td>
    <td width="529" colspan="7" bgcolor="#FFFFCC">
	<strong>
	I have not experienced any change in my sleeping pattern.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1a </strong></td>
    <td colspan="7"><strong> 
	I sleep somewhat more than usual.
	</strong></td>
  </tr>
    <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1b </strong></td>
    <td colspan="7"><strong> 
	I sleep somewhat less than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2a </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong>
	I sleep a lot more than usual.
	</strong></td>
  </tr>
    <tr> 
    <td height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2b </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong>
	I sleep a lot less than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3a </strong></td>
    <td colspan="7"><strong>
	I sleep most of the day.
	</strong>
	
	</td>
  </tr>
    <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3b </strong></td>
    <td colspan="7"><strong>
	I wake up 1-2 hours early and can't get back to sleep.
	</strong>
	<input type="hidden" name="<cfoutput>q#myIndex#</cfoutput>_required" value= "<cfoutput>#ERROR#</cfoutput>">
	</td>
  </tr>
    <tr> 
    <td height="19"><strong> 
      &nbsp;</td>
    <td colspan="7"><strong><input type="submit" value="Continue"></strong></td>
  </tr>
</table>
