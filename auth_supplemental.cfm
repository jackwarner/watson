<html>
<head>
<title>Survey Application, Neill Watson</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="login.cfm">
	
</cfif>
<body link="#0000FF" vlink="#0000FF" alink="#0000FF">
<table width="100%" border="0">
  <tr valign="top"> 
    <td colspan="2"> <h2><font color="#0000FF">WELCOME TO THE RESEARCH SITE OF<br>
        NEILL WATSON, PH.D.<br>
        THE COLLEGE OF WILLIAM &amp; MARY</font><br>
        <font color="#0000FF"><em>for Authorized Researchers</em></font></h2></td>
  </tr>
  <tr> 
    <td colspan="2"><h4><font color="#FF0000"><br>
        <script language="JavaScript1.2">
<!-- Original:  Craig Lumley -->
<!-- Web Site:  http://www.craiglumley.co.uk -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
<!-- Begin
function fullScreen(theURL) {
window.open(theURL, '', 'fullscreen=yes, scrollbars=yes');
}
//  End -->

var bestwidth = 1024;
var bestheight = 768;
if (screen.width < bestwidth || screen.height < bestheight) {
msg = "<hr>The surveys on this site are intended to be administered on systems with a screen resolution of at least"
+ bestwidth + "x" + bestheight + ". Your"
+ " screen resolution is " + screen.width + "x"
+ screen.height + ".  Please change your screen resolution "
+ "if possible.  You will not see this message if you screen is set to the proper resolution.";
document.write(msg);
}
//  End -->
</script>
        </font></h4></td>
  </tr>
  <tr> 
    <td colspan="2">
      <hr></td>
  </tr>
  <tr>
    <td width="60" align="center" valign="top"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle"><h3><a href="javascript:fullScreen('meta/RIOPC/frame_.cfm?num=0&myID=0&myTo=1&mySkip=x');">Self-Concept and Affect </a><a href="javascript:fullScreen('formatted/metaprogram.cfm?num=0&myID=0&myTo=0&mySkip=x');"></a></h3></td>
  </tr>
  
    <tr>
    <td align="center" valign="top"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle"><h3><a href="javascript:fullScreen('nodb/Demographic/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Demographic Questionnaire</a><br>
    </h3><blockquote>
      <h3><a href="allresults_demographic_nospace.cfm"><font size="2">Data Access</font></a></h3>
    </blockquote></td>
  </tr>
    <tr>
      <td align="center" valign="top"><div align="center"><img src="survey.jpg" alt="Survey" width="30" height="38"></div></td>
      <td valign="middle"><h3><a href="javascript:fullScreen('demographic_mysql/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">LTS Demographic Questionnaire (MySQL)<br>
      </a>
        <blockquote><a href="lts_demographic_mysql.cfm"><font size="2">Data Access</font></a></blockquote>
      </h3>      </td>
    </tr>
 <!--- <tr>
    <td align="center" valign="middle"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle"><h3><a href="javascript:fullScreen('latentvariable/lv_metaprogram.cfm?num=0&myto=0&myID=0');">Original Latent Variable with Uncorrected PC, Microsoft Access</a></h3></td>
  </tr>--->
    <tr>
    <td align="center" valign="top"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle"><h3><a href="javascript:fullScreen('latentvariable_corrected/lv_metaprogram.cfm?num=0&myto=0&myID=0');">PC, CC, AM, Ref, Imp, IM, BDI, BAI, DASS, STAIT, CESD (MySQL)<br>
      </a>
        <blockquote><a href=".cfm"><font size="2">Data Access</font></a><font size="2">, <a href="chars_pc_cc_am_ref_imp_im_bdi_bai_dass_stait_cesd_mysql.cfm">Characteristics</a></font><a href=".cfm"><br>
            </a></blockquote>
    </h3></td>
  </tr>
    <tr>
      <td align="center" valign="top"><div align="center"><img src="survey.jpg" alt="" width="30" height="38"></div></td>
      <td valign="middle"><h3><a href="javascript:fullScreen('IM_mysql/frame.cfm');">IM (MySQL)</a></h3></td>
    </tr>
  <!---
  <tr>
    <td align="center" valign="middle"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle"><h3><a href="javascript:fullScreen('latentvariable_aug08/lv_metaprogram.cfm?num=0&myto=0&myID=0');">Latent Variable with Uncorrected PC (without Importance & IM), MySQL</a></h3></td>
  </tr>
  --->
    <tr>
    <td align="center" valign="top"><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td valign="middle"><h3><a href="javascript:fullScreen('latentvariable_aug08_corrected/lv_metaprogram.cfm?num=0&myto=0&myID=0');">PC, CC, AM, Ref (MySQL)</a></h3></td>
  </tr>
  </tr>
    <tr>
    <td><div align="center"></div></td>
    <td><h3><form action='auth2.cfm'>
	<input type="submit" value="Continue" style="width:150px">
	</form></h3></td>
  </tr>
  <tr> 
    <td colspan="2"><hr></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><font size="1">Copyright 2004<br>
      Neill Watson, Ph.D.<br>
      College of William &amp; Mary<br>
      <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font></td>
  </tr>
</table>

</body>
</html>
