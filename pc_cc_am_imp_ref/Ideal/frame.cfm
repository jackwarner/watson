
<html>
<head>
<title>Ideal Self</title>
</head>
<cfset mySource = "watson">
<cfset myTable = "pc_cc_am_imp_ref">
<cfset myIndex = #URL.num#>
<cfset myID = #URL.myID#>

<cfset myQuest = "5">
<cfquery name="Getroi" datasource="#mySource#">
 SELECT numpick FROM #myTable# WHERE Id=#URL.myId#
</cfquery>

<cfif Getroi.numpick IS 2 OR Getroi.numpick IS 3 OR Getroi.numpick IS 4>
<cfset myQuest = "6">
</cfif>

<cfset myInclude = #URL.myTo#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 2>
<body><center>
<table width="585" border="0" cellpadding="0" cellspacing="0">
  	<tr>
		<td align="right">
		<cfif myIndex LTE myPages>
			<cfinclude template="../progress.cfm">
		</cfif>
		</td>
	</tr>
  </table>
  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <!---<tr valign="top"> 
      <td height="17"> <h5> 
	  <cfif #myIndex# EQUAL 0>Introduction
	  <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	  Questionnaire <cfoutput>#myQuest# of 13.  Page #myIndex-1# of #myPages-1#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>--->
    <tr> 
      <td height="10" valign="middle"> 
        <cfif myIndex EQUAL 0>
          <p><strong>Instructions:</strong> This is a measure of the importance 
            to the person of his or her ideal self (yourself as YOU would like 
            to be in your own eyes).</p>
          <cfelse>
        </cfif></td>
    </tr>
	<cfif myIndex LTE myPages>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
	</cfif>
    <tr valign="top"> 
      <td> <cfif myIndex EQUAL 0>
          <cfform action="frame.cfm?num=1&myID=1&mySkip=x">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td><strong>Participant 3 digit code:</strong></td>
                  <td><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant Gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1">Copyright 2004<br>
          Neill Watson, Ph.D.<br>
          College of William &amp; Mary<br>
          <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font> 
          <!---  <cfelseif myIndex LESS THAN 4>
        <h3><cfoutput>#myIndex#. #aTitles[myIndex]#</cfoutput></h3></td>--->
        </cfif> </tr>
    <tr valign="top"> 
      <td height="46"> 
	  <cfif myIndex EQ 1>
			<cfif #URL.mySkip# EQ "x" >
			 <cfif Len(#Form.Code#) NOT EQUAL 3> 
				<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
				code. Click to go back.</a> 
				<cfelse> 
				<cflock name="#CreateUUID()#" timeout="20">
				  <cftransaction>
					<cfquery name="myQ" datasource="#mySource#">
					INSERT INTO #myTable#(Code, Gender) VALUES ('#Form.Code#', '#Form.Gender#') 
					</cfquery>
					<CFQUERY name="getMaxID" datasource="#mySource#">
					SELECT Max(Id) AS myID FROM #myTable# 
					</CFQUERY>
				  </cftransaction>
				</cflock> 
				<cfset myID = #getMaxID.myID#> 
				<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
				<cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x"> 
				<cfinclude template="#myIndex#.cfm"> &nbsp;</td>
			  <td width="1"> 
			  </cfform><td width="16">
		   
		  </cfif><cfelse>
		  <cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x"> 
				<cfinclude template="#myIndex#.cfm"> &nbsp;</td>
			  <td width="1">
           </cfform>
		  </cfif>
	  </cfif>
	  <cfif myIndex GREATER THAN 1> 
      
        <cfinclude template="#myInclude#.cfm">
        &nbsp; 
        <td width="1"></td>
      
      <td width="1"> </cfif></tr>
    <tr valign="top"> 
      <td> <blockquote>
          <div align="left"></div>
        </blockquote></td>
    </tr>
  </table>
</center>
</body>
</html>
