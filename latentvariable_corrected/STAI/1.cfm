

<cfform action="frame.cfm?num=#myIndex+1#&myTo=2&myId=#myID#" method="post">
<center>
    <table width="585" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="9"><div align="left"> <strong>A number of statements which people have used to describe 
            themselves are given below. Read each statement and then select the 
            appropriate number to the right of the statement to indicate how you 
            <em>generally</em> feel. There are no right or wrong answers. Do not 
            spend too much time on any one statement but give the answer which 
            seems to describe how you generally feel. </strong> </div>          </td>
      </tr>
	</table>
	<table width="585" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="371" colspan="2">&nbsp;</td>
        <td colspan="8"><div align="center"><img src="stai.gif" width="211" height="106"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="10" rowspan="2" valign="middle">1. </td>
        <td width="186" rowspan="2" valign="middle">I feel pleasant.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai1" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai1" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai1" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai1" value="4">
            <input type=hidden name="stai1_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="middle">2.</td>
        <td width="186" rowspan="2" valign="middle"> I feel nervous and restless.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai2" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai2" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai2" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai2" value="4">
            <input type=hidden name="stai2_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="middle">3. </td>
        <td width="186" rowspan="2" valign="middle">I feel satisfied with myself.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai3" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai3" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai3" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai3" value="4">
            <input type=hidden name="stai3_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="middle">4. </td>
        <td width="186" rowspan="2" valign="middle">I wish I could be as happy 
        as others seem to be.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai4" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai4" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai4" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai4" value="4">
            <input type=hidden name="stai4_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="middle">5.</td>
        <td width="186" rowspan="2" valign="middle"> I feel like a failure.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai5" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai5" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai5" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai5" value="4">
            <input type=hidden name="stai5_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="middle">6.</td>
        <td width="186" rowspan="2" valign="middle"> I feel rested.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai6" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai6" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai6" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai6" value="4">
            <input type=hidden name="stai6_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="middle">7. </td>
        <td width="186" rowspan="2" valign="middle">I am &quot;calm, cool, 
        and collected.&quot;</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai7" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai7" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai7" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai7" value="4">
            <input type=hidden name="stai7_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="top">8. </td>
        <td width="186" rowspan="2" valign="top">I feel that difficulties 
        are piling up so that I cannot overcome them.</td>
        <td width="56" valign="bottom">&nbsp;</td>
        <td width="41" valign="middle"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="middle"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="middle"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="middle"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai8" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai8" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai8" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai8" value="4">
            <input type=hidden name="stai8_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="top">9. </td>
        <td width="186" rowspan="2" valign="top">I worry too much over something 
        that really doesn't matter.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai9" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai9" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai9" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai9" value="4">
            <input type=hidden name="stai9_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFFF"> 
        <td rowspan="2" valign="middle">10. </td>
        <td width="186" rowspan="2" valign="middle">I am happy.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="middle"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="middle"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="middle"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="middle"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="41" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai10" value="1">
            </font></div></td>
        <td width="38" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai10" value="2">
            </font></div></td>
        <td width="39" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai10" value="3">
            </font></div></td>
        <td colspan="4" valign="top" bgcolor="#FFFFFF"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai10" value="4">
            <input type=hidden name="stai10_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="middle">11. </td>
        <td width="186" rowspan="2" valign="middle">I have disturbing thoughts.</td>
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"><font size="2">1</font></div></td>
        <td width="38" valign="top"> <div align="center"><font size="2">2</font></div></td>
        <td width="39" valign="top"> <div align="center"><font size="2">3</font></div></td>
        <td width="40" colspan="4" valign="top"> <div align="center"><font size="2">4</font></div>
          <div align="center"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="56" valign="top">&nbsp;</td>
        <td width="41" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai11" value="1">
            </font></div></td>
        <td width="38" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai11" value="2">
            </font></div></td>
        <td width="39" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai11" value="3">
            </font></div></td>
        <td colspan="4" valign="top"> <div align="center"> <font size="2"> 
            <input type="radio" name="stai11" value="4">
            <input type=hidden name="stai11_required" value= "You must enter a rating.">
            </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div>
          <div align="center"> <font size="2"> </font></div></td>
      </tr>
      
      <tr valign="middle"> 
        <td colspan="2"> 
          <blockquote> 
            <p><div align="left"> <cfinclude template="cright.cfm"><br></p>
            </blockquote></td>
        <td colspan="8"> 
          <input type="submit" name="Submit" value="Continue"></td>
      </tr>
    </table>
</center>
</cfform>
