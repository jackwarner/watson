<!--- variable dSource found in questions.cfm --->
<cfquery name="bdi" datasource="#dSource#">
<cfset mUp = myIndex - 1>
<!--- From.qX where x should be one less than current file Y.cfm --->
<cfset myUpdate = "q" & mUp>
UPDATE #myTable#
	SET #myUpdate# = #Form.q7#
	WHERE ID = #myID#
</cfquery>

<table width="585" height="20" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="73" height="19" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="0">
      0</strong></td>
    <td colspan="7" bgcolor="#FFFFCC">
	<strong>
	I don't criticize or blame myself more than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1 </strong></td>
    <td colspan="7"><strong>
	I am more critical of myself than I used to be.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2 </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong>
	I criticize myself for all of my faults.
	</strong></td>
  </tr>
  <tr> 
    <td height="19"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3 </strong></td>
    <td colspan="7"><strong>
	I blame myself for everything bad that happens.
	</strong>
	<input type="hidden" name="<cfoutput>q#myIndex#</cfoutput>_required" value= "<cfoutput>#ERROR#</cfoutput>">
	</td>
  </tr>
    <tr> 
    <td height="19"><strong> 
      &nbsp;</td>
    <td colspan="7"><strong><input type="submit" value="Continue"></strong></td>
  </tr>
</table>
