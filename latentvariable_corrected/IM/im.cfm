<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<cfset databaseLive = false>
<cfif isDefined("URL.myID")>
	<cfset databaseLive = true>
</cfif>

<cfset myTable = "latentvar">
<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I sometimes tell lies if I have to.">
	<cfset myQuestions[2] = "I never cover up my mistakes.">
	<cfset myQuestions[3] = "There have been occasions when I have taken advantage of someone.">
    <cfset myQuestions[4] = "I never swear.">
	<cfset myQuestions[5] = "I sometimes try to get even rather than forgive and forget.">
	<cfset myQuestions[6] = "I always obey laws, even if I'm unlikely to get caught.">
	<cfset myQuestions[7] = "I have said something bad about a friend behind his/her back.">
	<cfset myQuestions[8] = "When I hear people talking privately, I avoid listening.">
	<cfset myQuestions[9] = "I have received too much change from a salesperson without telling him or her.">
	<cfset myQuestions[10]= "I always declare everything at customs.">
	<cfset myQuestions[11]= "When I was young I sometimes stole things.">
	<cfset myQuestions[12]= "I have never dropped litter on the street.">
	<cfset myQuestions[13]= "I sometimes drive faster than the speed limit.">
	<cfset myQuestions[14]= "I never read sexy books or magazines.">
	<cfset myQuestions[15]= "I have done things that I don't tell other people about.">
	<cfset myQuestions[16]= "I never take things that don't belong to me.">
	<cfset myQuestions[17]= "I have taken sick-leave from work even though I wasn't really sick.">
	<cfset myQuestions[18]= "I have never damaged a library book or store merchandise without reporting it.">
	<cfset myQuestions[19]= "I have some pretty awful habits.">
	<cfset myQuestions[20]= "I don't gossip about other people's business.">

<cfset page = 1>

<cfset mySource = "watson">

<cfset qPerPage = 10>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<!---
For compatibility with older surveys, set myIndex and myPages --->
<cfset myIndex = page + 1>
<cfset myPages = totalPages + 1>
<!--- end compability measures --->
<cfset title = "IM">
<cfset instructions = "Please indicate how true each of the following statements is of you.">
<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>
<!---
	* Ugly hack here because for some reason IM1 isnt updated with CFUPDATE
--->
<cfif page EQ 2 AND databaseLive>
	<cfquery datasource="#mySource#">
		UPDATE #myTable#_supplemental SET im1=#Form.im1# WHERE FK=#Form.FK#
	</cfquery>
</cfif>
<cfif page GT 1 AND databaseLive>
	<cfupdate datasource="#mySource#" tablename="#myTable#_supplemental">
</cfif>

<cfif page GT totalPages AND databaseLive>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8">
	<cfinclude template="../getcode_im.cfm">
	</td>
  </tr>
</table>
<cfelseif page GT totalPages AND NOT databaseLive>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8" align="center">
	<h3>Thank you for completing the questionnaire.</h3>
    <input type="button" value="Close the Questionnaire" name="close" onClick=	"window.close()">
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="ew.js"></script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">
if (EW_this.im#i# && !EW_hasValue(EW_this.im#i#, "RADIO" )) {
	if (!EW_onError(EW_this, EW_this.im#i#, "RADIO", "Please enter a response for Question #i#."))
		return false;
}
</cfloop>
</cfoutput>
return true;
}
//-->
</script>

<form method="post" onSubmit="return EW_checkMyForm(this);">
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  	<tr>
		<td align="right">
			<cfinclude template="../progress.cfm">
		</td>
	</tr>
	<tr>
		<td><hr></td>
	</tr>
</table>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8"><h3><cfoutput>#instructions#</cfoutput></h3></td>
  </tr>
  <tr>
  	<td>
    <td width="50%">&nbsp;</td>
    <td colspan="8"><div align="center"><img src="trues.gif" width="291" height="61"></div></td>
  </tr>
  <cfoutput>
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
  	<td rowspan="2" valign="top">#i#.&nbsp;</td>
  	<td width="50%" rowspan="2" valign="top">#myQuestions[i]#</td>
	<cfloop from="1" to="7" index="j">
	<td><div align="center"><font size="2">#j#</font></div></td>
	</cfloop>
  </tr>
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
 	<cfloop from="1" to="7" index="k">
	<td width="41"><div align="center"> <font size="2">
   	     <input type="radio" name="im#i#" value="#k#" <cfif debug and k EQ 1>checked</cfif>>
   	 </font></div></td>
	</cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
  	<td></td>
    <td height="43">&nbsp;</td>
    <td colspan="7"><div align="left"> <br>
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
	<cfif databaseLive>
	<input type="hidden" name="FK" value="<cfoutput>#URL.myID#</cfoutput>">
	</cfif>
	<input type="submit" name="Submit" value="Continue">
    </div></td>
  </tr>
</table>
</form>
</cfif>
</body>
</html>
