<cfform action="frame.cfm?num=#myIndex+1#&amp;myTo=ideal3&amp;myId=#URL.myId#" method="post">
<center>
    <table width="585" border="0" cellpadding="0" cellspacing="0">
      <tr valign="top"> 
        <td colspan="11"> <h3>Describe your IDEAL SELF: This is yourself as YOU 
            would like to be in your own eyes.</h3></td>
      </tr>
      <tr> 
        <td colspan="11"> <div align="center"><img src="../constructs.gif" width="586" height="49"></div></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="50%" rowspan="2" valign="middle">industrious</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td width="41"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td width="41"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="2">
            </font></div></td>
        <td width="41"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="3">
            </font></div></td>
        <td width="41"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="4">
            </font></div></td>
        <td width="42"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="5">
            </font></div></td>
        <td width="41"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="6">
            </font></div></td>
        <td width="42"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir1" value="7">
            <input type=hidden name="ir1_required" value= "You must enter a rating.">
            </font></div></td>
        <td width="42">&nbsp;</td>
        <td width="42">&nbsp;</td>
        <td width="42">&nbsp;</td>
      </tr>
      <tr> 
        <td rowspan="2" valign="middle">worrying</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr> 
        <td height="14"> <div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir2" value="7">
            <input name="ir2_required" type=hidden id="ir2_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td rowspan="2" valign="middle">efficient</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td height="14"> <div align="center"> 
            <input type="radio" name="ir3" value="1" <cfif debug>checked</cfif>>
          </div></td>
        <td> <div align="center"> 
            <input type="radio" name="ir3" value="2">
          </div></td>
        <td> <div align="center"> 
            <input type="radio" name="ir3" value="3">
          </div></td>
        <td> <div align="center"> 
            <input type="radio" name="ir3" value="4">
          </div></td>
        <td> <div align="center"> 
            <input type="radio" name="ir3" value="5">
          </div></td>
        <td> <div align="center"> 
            <input type="radio" name="ir3" value="6">
          </div></td>
        <td> <div align="center"> 
            <input type="radio" name="ir3" value="7">
            <input name="ir3_required" type=hidden id="ir3_required" value= "You must enter a rating.">
          </div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle">cheerful</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir4" value="7">
            <input name="ir4_required" type=hidden id="ir4_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle" bgcolor="#FFFFCC">organized</td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">1</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">2</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">3</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">4</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">5</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">6</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">7</font></div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="1" <cfif debug>checked</cfif>>
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="2">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="3">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="4">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="5">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="6">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir5" value="7">
            <input name="ir5_required" type=hidden id="ir5_required" value= "You must enter a rating.">
          </div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle">emotional</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir6" value="7">
            <input name="ir6_required" type=hidden id="ir6_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle" bgcolor="#FFFFCC">warm</td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">1</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">2</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">3</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">4</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">5</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">6</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">7</font></div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="1" <cfif debug>checked</cfif>>
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="2">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="3">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="4">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="5">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="6">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir7" value="7">
            <input name="ir7_required" type=hidden id="ir7_required" value= "You must enter a rating.">
          </div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle">shallow</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir8" value="7">
            <input name="ir8_required" type=hidden id="ir8_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle" bgcolor="#FFFFCC">outgoing</td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">1</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">2</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">3</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">4</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">5</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">6</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">7</font></div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="1" <cfif debug>checked</cfif>>
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="2">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="3">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="4">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="5">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="6">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir9" value="7">
            <input name="ir9_required" type=hidden id="ir9_required" value= "You must enter a rating.">
          </div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle">rude</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir10" value="7">
            <input name="ir10_required" type=hidden id="ir10_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle" bgcolor="#FFFFCC">polished</td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">1</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">2</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">3</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">4</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">5</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">6</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">7</font></div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="1" <cfif debug>checked</cfif>>
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="2">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="3">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="4">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="5">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="6">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir11" value="7">
            <input name="ir11_required" type=hidden id="ir11_required" value= "You must enter a rating.">
          </div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle">idealistic</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir12" value="7">
            <input name="ir12_required" type=hidden id="ir12_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle" bgcolor="#FFFFCC">talkative</td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">1</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">2</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">3</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">4</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">5</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">6</font></div></td>
        <td bgcolor="#FFFFCC"><div align="center"><font size="2">7</font></div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="1" <cfif debug>checked</cfif>>
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="2">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="3">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="4">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="5">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="6">
          </div></td>
        <td bgcolor="#FFFFCC"> <div align="center"> 
            <input type="radio" name="ir13" value="7">
            <input name="ir13_required" type=hidden id="ir13_required" value= "You must enter a rating.">
          </div></td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
        <td bgcolor="#FFFFCC">&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td rowspan="2" valign="middle">foolish</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="1" <cfif debug>checked</cfif>>
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="2">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="3">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="4">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="5">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="6">
            </font></div></td>
        <td><div align="center"> <font size="2"> 
            <input type="radio" name="ir14" value="7">
            <input name="ir14_required" type=hidden id="ir14_required" value= "You must enter a rating.">
            </font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr valign="top"> 
        <td >&nbsp;</td>
        <td colspan="10"> <div align="left"> <br> <input type="submit" name="Submit" value="Continue"> 
        </td>
      </tr>
    </table>
</center>
</cfform>
