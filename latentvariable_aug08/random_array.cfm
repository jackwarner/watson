<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Thank You.</title>
</head>

<body>
<cfset myRandomOrder = ArrayNew(1)>
<cfset myNormalizedOrder = "">
<cfset myTable = "latentvar_aug08">
<cfset mySource = "watson">

<cfset myFrom = "">
<cfif isDefined("Form.myFrom")>
	<cfset myFrom = #Form.myFrom#>
<cfelseif isDefined("URL.myFrom")>
	<cfset myFrom = #URL.myFrom#>
</cfif>

<cfset participant = Form.code MOD 48>

<cfif participant EQ 0>
	<cfset participant = 48>
</cfif>

<cfset abstractFirst = false>
<cfset idealFirst = false>
<cfset insertPick = "">
<cfset whichBDI = "">
<!---
56 mod 48 = 8 -> 31
59 mod 48 = 11 -> 37
--->
<cfset myRandomOrder = ListToArray("43,7,44,5,15,29,37,25,34,1,42,47,40,12,19,32,23,31,39,22,18,46,26,35,41,33,17,2,21,38,8,45,48,3,36,4,11,28,10,30,14,16,27,13,9,20,6,24", ",")>
<cfloop from="1" to="#ArrayLen(myRandomOrder)#" index="i">
	<cfif myRandomOrder[i] EQ participant>
		<cfset myNormalizedOrder = i>
	</cfif>
	
</cfloop>
<cfif myNormalizedOrder GTE 1 AND myNormalizedOrder LTE 6>
	<cfset abstractFirst = true>
<cfelseif myNormalizedOrder GTE 13 AND myNormalizedOrder LTE 18>
	<cfset abstractFirst = true>
<cfelseif myNormalizedOrder GTE 25 AND myNormalizedOrder LTE 30>
	<cfset abstractFirst = true>
<cfelseif myNormalizedOrder GTE 37 AND myNormalizedOrder LTE 42>
	<cfset abstractFirst = true>
</cfif>

<cfoutput>

<cfset insertPick = myNormalizedOrder MOD 6>
<cfif insertPick EQ 0>
	<cfset insertPick = 6>
</cfif>
<cfif insertPick EQ 1 or insertPick EQ 5 OR insertPick EQ 6>
	<cfset idealFirst = true>
</cfif>

<cfset abstractricURL = "AbstractRIc/frame.cfm">
<cfset abstractrocURL = "AbstractROc/frame.cfm">
<cfset baiURL = "BAI/bai.cfm">
<cfset bdiURL = "BDI/frame.cfm">
<cfset cesdURL = "CESD/cesd.cfm">
<cfset dassURL = "DASS/dass.cfm">
<cfset idealURL = "Ideal/frame.cfm">
<cfset imURL = "IM/im.cfm">
<cfset oughtURL = "Ought/frame.cfm">
<cfset refpersonURL = "RefPerson/frame.cfm">
<cfset rioccURL = "RIOCC/frame.cfm">
<cfset riopcURL = "RIOPC/frame_.cfm">
<cfset staiURL = "STAI/frame.cfm">

<cfset myNext = "DEFAULT">

<cfif myFrom NEQ "">
	<!-- FROM ABSTRACT MEASURES --->
	<cfif myFrom EQ abstractricURL AND idealFirst>
		<cfset myNext = abstractrocURL>
    <cfelseif myFrom EQ abstractricURL AND NOT idealFirst and abstractFirst>   
    	<cfset myNext = riopcURL>  
    <cfelseif myFrom EQ abstractricURL AND NOT idealFirst and NOT abstractFirst> 
    	<cfset myNext = refpersonURL> 
    <cfelseif myFrom EQ abstractrocURL AND idealFirst and abstractFirst> 
    	<cfset myNext = riopcURL> 
    <cfelseif myFrom EQ abstractrocURL AND idealFirst and NOT abstractFirst>  	 
    	<cfset myNext = refpersonURL> 
	<cfelseif myFrom EQ abstractrocURL AND NOT idealFirst>
		<cfset myNext = abstractricURL>
	<!-- FROM PC (PERSONAL CONSTRUCTS) --->
	<cfelseif myFrom EQ riopcURL>
		<cfset myNext = rioccURL>
	<!-- FROM CC (Conventional CONSTRUCTS) --->
	<cfelseif myFrom EQ rioccURL and abstractFirst>
		<cfset myNext = refpersonURL>
    <cfelseif myFrom EQ rioccURL and NOT abstractFirst>
    	<cfif idealFirst>
        	<cfset myNext = abstractricURL>
        <cfelse>
        	<cfset myNext = abstractrocURL>
        </cfif>
	<cfelseif myFrom EQ refpersonURL>
		<cfset myNext = "[END]">
    </cfif>
	
    
    <!---    
	<cfelseif myFrom EQ oughtURL OR myFrom EQ idealURL>
		<cfset myNext = refpersonURL>
	<cfelseif myFrom EQ refpersonURL>
		<cfset myNext = imURL>
	<cfelseif myFrom EQ imURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = bdiURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = staiURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = bdiURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = baiURL>
		</cfif>
	<cfelseif myFrom EQ bdiURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = baiURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = "[END]">
		<cfelseif whichBDI EQ 3>
			<cfset myNext = staiURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = "[END]">
		</cfif>
	<cfelseif myFrom EQ baiURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = dassURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = bdiURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = "[END]">
		<cfelseif whichBDI EQ 4>
			<cfset myNext = cesdURL>
		</cfif>
	<cfelseif myFrom EQ dassURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = cesdURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = baiURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = cesdURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = staiURL>
		</cfif>
	<cfelseif myFrom EQ cesdURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = staiURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = dassURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = baiURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = dassURL>
		</cfif>
	<cfelseif myFrom EQ staiURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = "[END]">
		<cfelseif whichBDI EQ 2>
			<cfset myNext = cesdURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = dassURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = bdiURL>
		</cfif>
	</cfif>
	--->
<cfelse>
	<cfif abstractFirst AND idealFirst>
		<cfset myNext = abstractricURL>
	</cfif>
	<cfif abstractFirst AND NOT idealFirst>
		<cfset myNext = abstractrocURL>
	</cfif>
	<cfif NOT abstractFirst>
		<cfset myNext = riopcURL>
	</cfif>
	
	<!--- update the database --->
	<cflock name="#CreateUUID()#" timeout="20">
		<cftransaction>
			<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable#(Code, Gender, BigOrder, numpick) VALUES ('#Form.Code#', '#Form.Gender#', #myNormalizedOrder#, '#insertPick#') 
			</cfquery>
			<CFQUERY name="getMaxID" datasource="#mySource#">
				SELECT Max(Id) AS myID FROM #myTable# 
			</CFQUERY>
		</cftransaction>
	</cflock> 
	<cfset myID = #getMaxID.myID#> 
		
	<cfquery datasource="#mySource#">
		INSERT INTO #myTable#_supplemental (FK) VALUES (#myID#)
	</cfquery>
	
	<br><br><br><br><br><br><br>
	<table width="585" align="center"><tr><td>
	<h3>Thank you for volunteering to participate in this study. You are 
              asked first to complete five questionnaires on the computer. Three 
              of them are very brief.<br>
              <br>
			   
			  <form method="post" action="#myNext#?num=2&myID=<cfoutput>#myID#</cfoutput>&myTo=1">
				<input type="hidden" value="#myNext#" name="myFrom">
				<input type="hidden" value="#Form.Code#" name="Code">
				<input type="submit" value="Click here to begin the questionnaires.">
			  </form>
               &nbsp; 
			</h3>
			</td></tr>
		</table>

	
</cfif>
<!--- determine other blocks of orders --->
<cfif false>
Participant: #participant#.
<BR>Normal Order: #myNormalizedOrder#.<BR>
	
	Implies: #insertPick# and abstractFirst: #abstractFirst#.<BR>
	BDI Order: #whichBDI#.<br>
	From: #myFrom#.<br>
	Next: #myNext#.</cfif>

<cfif myNext EQ "[END]">
		<cfcookie name="npwatscookie" value="true" expires="NOW">
		<cfcookie name="whichQuestionnaire" value="0" expires="NOW">
		<br><br><br><br><br><br><br>
		<table width="585" align="center"><tr><td>
            <h3>Thank you for completing the questionnaires. Please let the 
			researcher know that you have finished.</h3> <br>
            <!--- Or, go 
			<a href="http://cfdev.wm.edu/watson/results.cfm?Id=<cfoutput>#URL.myID#</cfoutput>">here</a> 
			to see the results of your questionnaire. For testing purposes only. --->
            </strong><BR><input type="button" value="Close the Questionnaire" name="close" onClick=	"window.close()"></td></tr></table>
            
<cfelseif myFrom EQ "">
	<!---do not redirect--->
<cfelseif myNext EQ bdiURL>
	<cflocation url="#myNext#?num=1&myID=#myID#&myTo=2&mySkip=x" addtoken="no">
<cfelse>
	<cflocation url="#myNext#?num=2&myID=#myID#&myTo=1&mySkip=x" addtoken="no">
</cfif>
	
</cfoutput>
</body>
</html>
