 <cfform action="../../Copy of latentvariable/Ought/frame.cfm?num=#myIndex+1#&amp;myID=#myID#&amp;myTo=2&amp;mySkip=x">
<center>
  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr> 
      <td colspan="8"><h3>Please consider the following statements about your 
          OUGHT/SHOULD SELF (yourself as OTHERS think you ought or should be). 
          For each statement, indicate how true it is for you.</h3></td>
    </tr>
    <tr> 
      <td width="50%">&nbsp;</td>
      <td colspan="7"><div align="center"><img src="../../Copy of latentvariable/trues.gif" width="291" height="61"></div></td>
    </tr>
    <tr bgcolor="#FFFFCC"> 
      <td width="50%" rowspan="2">My ought/should self is important in how I think 
        about myself.</td>
      <td><div align="center"><font size="2">1</font></div></td>
      <td><div align="center"><font size="2">2</font></div></td>
      <td><div align="center"><font size="2">3</font></div></td>
      <td><div align="center"><font size="2">4</font></div></td>
      <td><div align="center"><font size="2">5</font></div></td>
      <td><div align="center"><font size="2">6</font></div></td>
      <td><div align="center"><font size="2">7</font></div></td>
    </tr>
    <tr bgcolor="#FFFFCC"> 
      <td width="41"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="1">
          </font></div></td>
      <td width="41"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="2">
          </font></div></td>
      <td width="41"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="3">
          </font></div></td>
      <td width="41"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="4">
          </font></div></td>
      <td width="42"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="5">
          </font></div></td>
      <td width="41"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="6">
          </font></div></td>
      <td width="42"> <div align="center"> <font size="2"> 
          <input type="radio" name="os1" value="7">
		  <input type="hidden" name="os1_required" value= "You must enter a rating.">
          </font></div></td>
    </tr>
    <tr> 
      <td rowspan="2">My ought/should self is important in how I feel about myself.</td>
      <td><div align="center"><font size="2">1</font></div></td>
      <td><div align="center"><font size="2">2</font></div></td>
      <td><div align="center"><font size="2">3</font></div></td>
      <td><div align="center"><font size="2">4</font></div></td>
      <td><div align="center"><font size="2">5</font></div></td>
      <td><div align="center"><font size="2">6</font></div></td>
      <td><div align="center"><font size="2">7</font></div></td>
    </tr>
    <tr> 
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="1">
          </font></div></td>
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="2">
          </font></div></td>
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="3">
          </font></div></td>
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="4">
          </font></div></td>
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="5">
          </font></div></td>
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="6">
          </font></div></td>
      <td><div align="center"> <font size="2"> 
          <input type="radio" name="os2" value="7">
		  <input type="hidden" name="os2_required" value= "You must enter a rating.">
          </font></div></td>
    </tr>
    <tr bgcolor="#FFFFCC"> 
      <td rowspan="2">My ought/should self is important in what I decide to do.</td>
      <td><div align="center"><font size="2">1</font></div></td>
      <td><div align="center"><font size="2">2</font></div></td>
      <td><div align="center"><font size="2">3</font></div></td>
      <td><div align="center"><font size="2">4</font></div></td>
      <td><div align="center"><font size="2">5</font></div></td>
      <td><div align="center"><font size="2">6</font></div></td>
      <td><div align="center"><font size="2">7</font></div></td>
    </tr>
    <tr bgcolor="#FFFFCC"> 
      <td> <div align="center"> 
          <input type="radio" name="os3" value="1">
        </div></td>
      <td> <div align="center"> 
          <input type="radio" name="os3" value="2">
        </div></td>
      <td> <div align="center"> 
          <input type="radio" name="os3" value="3">
        </div></td>
      <td> <div align="center"> 
          <input type="radio" name="os3" value="4">
        </div></td>
      <td> <div align="center"> 
          <input type="radio" name="os3" value="5">
        </div></td>
      <td> <div align="center"> 
          <input type="radio" name="os3" value="6">
        </div></td>
      <td> <div align="center"> 
          <input type="radio" name="os3" value="7">
		  <input type="hidden" name="os3_required" value= "You must enter a rating.">
        </div></td>
    </tr>
    <tr valign="top"> 
      <td height="43">&nbsp;</td>
      <td colspan="7"> <div align="left"> 
        
            <br>
            <input type="submit" name="Submit" value="Continue">
         
</td>
    </tr>
  </table>
</center>
 </cfform> 
