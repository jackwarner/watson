<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="../login.cfm">
	
</cfif>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Watson Colleagues</title>
<script language="JavaScript1.2">
<!-- Original:  Craig Lumley -->
<!-- Web Site:  http://www.craiglumley.co.uk -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
<!-- Begin
function fullScreen(theURL) {
window.open(theURL, '', 'fullscreen=yes,scrollbars=yes,left=0,top=0,status=no,toolbar=no,location=no,menubar=no,titlebar=no');
}
</script>
</head>
<body link="#0000FF" vlink="#0000FF" alink="#0000FF">
<table width="100%" border="0">
  <tr valign="top"> 
    <td colspan="2"> <h2><font color="#0000FF">WELCOME TO THE RESEARCH SITE OF<br>
        NEILL WATSON, PhD<br>
        COLLEGE OF WILLIAM AND MARY</font><br>
       <font color="#0000FF"><em>for Authorized Researchers</em></font></h2></td>
  </tr>
  <tr> 
    <td colspan="2">
      <hr></td>
  </tr>
  <tr>
    <td style="vertical-align:text-top; padding-top: 15px;">
    	<div align="center"><img src="../survey.jpg" width="30" height="38"></div></td>
    <td>
    	<h3>
        	<a href="javascript:fullScreen('../pc_cc_am_imp_ref/pc_cc_am_imp_ref.cfm?num=0&myto=0&myID=0');">Self-Discrepancy Questionnaires</a>
        </h3>
        <ul>
        	<li><a href="javascript:fullScreen('../pc_cc_am_imp_ref/pc_cc_am_imp_ref.cfm?num=0&myto=0&myID=0');">http://watsonresearch.wm.edu/pc_cc_am_imp_ref/pc_cc_am_imp_ref.cfm?num=0&amp;myto=0&amp;myID=0</a></li>
        </ul>
        <h4>
        	Data Files for Self-Discrepancy Questionnaires
        </h4>
        <ul>
        	<li><a href="../allresults_pc_cc_am_imp_ref_mysql.cfm">http://watsonresearch.wm.edu/allresults_pc_cc_am_imp_ref_mysql.cfm</a></li>
            <li><a href="../chars_pc_cc_am_imp_ref_mysql.cfm">http://watsonresearch.wm.edu/chars_pc_cc_am_imp_ref_mysql.cfm</a></li>
        </ul>
     </td>
  </tr>
  <tr>
    <td style="vertical-align:text-top; padding-top: 15px;">
    	<div align="center"><img src="../survey.jpg" width="30" height="38"></div></td>
    <td>
    	<h3>
        	<a href="javascript:fullScreen('../latentvariable_corrected/lv_metaprogram.cfm?num=0&myto=0&myID=0');">Self-Discrepancy, Anxiety, and Depression Questionnaires</a>
        </h3>
        <ul>
        	<li><a href="javascript:fullScreen('../latentvariable_corrected/lv_metaprogram.cfm?num=0&myto=0&myID=0');">http://watsonresearch.wm.edu/latentvariable_corrected/lv_metaprogram.cfm?num=0&amp;myto=0&amp;myID=0</a></li>
        </ul>
        <h4>
        	Data Files for Self-Discrepancy, Anxiety, and Depression Questionnaires
        </h4>
        <ul>
        	<li><a href="../allresults_pc_cc_am_imp_ref_im_bdi_bai_dass_stait_cesd_mysql.cfm">http://watsonresearch.wm.edu/allresults_pc_cc_am_imp_ref_im_bdi_bai_dass_stait_cesd_mysql.cfm</a></li>
            <li><a href="../chars_pc_cc_am_imp_ref_im_bdi_bai_dass_stait_cesd_mysql.cfm">http://watsonresearch.wm.edu/chars_pc_cc_am_imp_ref_im_bdi_bai_dass_stait_cesd_mysql.cfm</a></li>
        </ul>
     </td>
  </tr>
  <tr>
    <td style="vertical-align:text-top; padding-top: 15px;">
    	<div align="center"><img src="../survey.jpg" width="30" height="38"></div></td>
    <td>
    	<h3>
        	<a href="javascript:fullScreen('../IM_mysql/frame.cfm');">Impression Management scale of the Balanced Inventory of Desirable Responding (Paulhus, 1984)</a>
        </h3>
        <ul>
        	<li><a href="javascript:fullScreen('../IM_mysql/frame.cfm');">http://watsonresearch.wm.edu/IM_mysql/frame.cfm</a></li>
        </ul>
        <h4>
        	Data Files for Impression Management scale
        </h4>
        <ul>
        	<li><a href="../allresults_im_mysql.cfm">http://watsonresearch.wm.edu/allresults_im_mysql.cfm</a></li>
        </ul>
     </td>
  </tr>
    </tr>
  <tr>
    <td style="vertical-align:text-top; padding-top: 15px;">
    	<div align="center"><img src="../survey.jpg" width="30" height="38"></div></td>
    <td>
    	<h3>
        	<a href="javascript:fullScreen('../mysql/DASS/dass.cfm?num=0&myto=0&myID=0');">Depression Anxiety Stress Scales (Lovibond & Lovibond, 1993)</a>
        </h3>
        <ul>
        	<li><a href="javascript:fullScreen('../mysql/DASS/dass.cfm?num=0&myto=0&myID=0');">http://watsonresearch.wm.edu/mysql/DASS/dass.cfm?num=0&amp;myto=0&amp;myID=0</a></li>
        </ul>
        <h4>
        	Data Files for Depression Anxiety Stress Scales
        </h4>
        <ul>
        	<li><a href="../allresults_dass_mysql.cfm">http://watsonresearch.wm.edu/allresults_dass_mysql.cfm</a></li>
        </ul>
     </td>
  </tr>
  </table>
</body>
</html>