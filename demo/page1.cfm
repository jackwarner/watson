<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I sometimes tell lies if I have to.">
	<cfset myQuestions[2] = "I never cover up my mistakes.">
	<cfset myQuestions[3] = "There have been occasions when I have taken advantage of someone.">
    <cfset myQuestions[4] = "I never swear.">
	<cfset myQuestions[5] = "I sometimes try to get even rather than forgive and forget.">
	<cfset myQuestions[6] = "I always obey laws, even if I'm unlikely to get caught.">
	<cfset myQuestions[7] = "I have said something bad about a friend behind his/her back.">
	<cfset myQuestions[8] = "When I hear people talking privately, I avoid listening.">
	<cfset myQuestions[9] = "I have received too much change from a salesperson without telling him or her.">
	<cfset myQuestions[10]= "I always declare everything at customs.">
	<cfset myQuestions[11]= "When I was young I sometimes stole things.">
	<cfset myQuestions[12]= "I have never dropped litter on the street.">
	<cfset myQuestions[13]= "I sometimes drive faster than the speed limit.">
	<cfset myQuestions[14]= "I never read sexy books or magazines.">
	<cfset myQuestions[15]= "I have done things that I don't tell other people about.">
	<cfset myQuestions[16]= "I never take things that don't belong to me.">
	<cfset myQuestions[17]= "I have taken sick-leave from work even though I wasn't really sick.">
	<cfset myQuestions[18]= "I have never damaged a library book or store merchandise without reporting it.">
	<cfset myQuestions[19]= "I have some pretty awful habits.">
	<cfset myQuestions[20]= "I don't gossip about other people's business.">

<cfset page = 1>
<cfset qPerPage = 10>
<cfset totalQs = ArrayLen(myQuestions)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<cfset title = "Impression Management Scale">
<cfset instructions = "Please indicate how true each of the following statements is of you.">
<cfset start = 1>
<cfset end = 10>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>
<form action="page2.cfm" method="post">
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8"><h3><cfoutput>#instructions#</cfoutput></h3></td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td colspan="7"><div align="center"><img src="trues.gif" width="291" height="61"></div></td>
  </tr>
  <cfoutput>
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
  	<td width="50%" rowspan="2">#i#. #myQuestions[i]#</td>
	<cfloop from="1" to="7" index="j">
	<td><div align="center"><font size="2">#j#</font></div></td>
	</cfloop>
  </tr>
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
 	<cfloop from="1" to="7" index="k">
	<td width="41"><div align="center"> <font size="2">
   	     <input type="radio" name="im#i#" value="#k#">
   	 </font></div></td>
	</cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
    <td height="43">&nbsp;</td>
    <td colspan="7"><div align="left"> <br>
            <input type="submit" name="Submit" value="Continue to Page <cfoutput>#page+1#</cfoutput>">
    </div></td>
  </tr>
</table>
</form>
</body>
</html>
