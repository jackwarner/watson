<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I was bothered by things that usually don't bother me.">
	<cfset myQuestions[2] = "I did not feel like eating; my appetite was poor.">
	<cfset myQuestions[3] = "I felt that I could not shake off the blues even with help from my family or friends.">
    <cfset myQuestions[4] = "I felt that I was just as good as other people.">
	<cfset myQuestions[5] = "I had trouble keeping my mind on what I was doing.">
	<cfset myQuestions[6] = "I felt depressed.">
	<cfset myQuestions[7] = "I felt that everything I did was an effort.">
	<cfset myQuestions[8] = "I felt hopeful about the future.">
	<cfset myQuestions[9]= "I thought my life had been a failure.">
	<cfset myQuestions[10]= "I felt fearful.">
	<cfset myQuestions[11]= "My sleep was restless.">
	<cfset myQuestions[12]= "I was happy">
	<cfset myQuestions[13]= "I talked less than usual.">
	<cfset myQuestions[14]= "I felt lonely.">
	<cfset myQuestions[15]= "People were unfriendly.">
	<cfset myQuestions[16]= "I enjoyed life.">
	<cfset myQuestions[17]= "I had crying spells.">
	<cfset myQuestions[18]= "I felt sad.">
	<cfset myQuestions[19]= "I felt that people disliked me.">
	<cfset myQuestions[20]= "I could not get 'going'.">

<cfset page = 1>
<cfset qPerPage = 10>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<cfset title = "Center for Epidemiological Studies Depression Scale">
<cfset instructions = "Below is a list of some of the ways you may have felt or behaved.  Please indicate how often">
<cfset instructions = instructions & " you have felt this way during the past week by choosing the appropriate answer.">
<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>


<cfif page GT totalPages>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8"><h3>Thank you for completing the <cfoutput>#title#.</cfoutput></h3>
	<input type="button" value="Close the Window" name="close" onClick="window.close()">
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="ew.js"></script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">
if (EW_this.cesd#i# && !EW_hasValue(EW_this.cesd#i#, "RADIO" )) {
	if (!EW_onError(EW_this, EW_this.cesd#i#, "RADIO", "Please enter a response for Question #i#."))
		return false;
}
</cfloop>
</cfoutput>
return true;
}
//-->
</script>

<form method="post" onSubmit="return EW_checkMyForm(this);">
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8"><h3><cfoutput>#instructions#</cfoutput></h3></td>
  </tr>
  <tr>
    <td colspan="8"><div align="right"><img src="cesd.gif" width="412" height="133"></div></td>
    </tr>
  <cfoutput>
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
  	<td width="185" rowspan="2">#i#. #myQuestions[i]#</td>
	<cfloop from="1" to="4" index="j">
	<td width="100"><div align="center"><font size="2">#j#</font></div></td>
	</cfloop>
  </tr>
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
 	<cfloop from="1" to="4" index="k">
	<td width="100" valign="top"><div align="center"> <font size="2">
   	     <input type="radio" name="cesd#i#" value="#k#">
   	 </font></div></td>
	</cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
    <td height="43">&nbsp;</td>
    <td colspan="7"><div align="left"> <br>
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
		<cfif (page+1) GT totalPages>
		 	<input type="submit" name="Submit" value="Continue to Final Page">
		<cfelse>
		 	 <input type="submit" name="Submit" value="Continue to Page <cfoutput>#page+1# of #totalPages#</cfoutput>">
		</cfif>    
    </div></td>
  </tr>
</table>
</form>
</cfif>
</body>
</html>
