<!--- use cfsetting to block output of HTML 
outside of cfoutput tags --->
<cfsetting enablecfoutputonly="Yes">
<!--- set vars for special chars --->
<cfset TabChar = Chr(44)>
<cfset NewLine = Chr(13) & Chr(10)>
<!--- set content type to invoke Excel --->
<cfcontent type="application/msexcel">
<cfheader name="Content-Disposition" value="filename=im_mysql.csv">
<cfset delim = ",">
	<cfset myDataBase = "ParticipantCode" & delim & "TakenOn" & delim & "Gender">
	<cfloop index="pcrs" from="1" to="20">
		<cfset myDataBase = myDataBase & delim & "IM" & #pcrs#>
	</cfloop>
	<cfset myDataBase = myDataBase & "<BR>">
	<cfoutput>#myDataBase#</cfoutput>
	<cfquery datasource="watson" name="ranks2">Select * from im order by id </cfquery>
    <cfprocessingdirective suppressWhiteSpace="true">
    <cfoutput query="ranks2">#ranks2.Code#,#ranks2.CreatedOn#,#ranks2.Gender#,<cfloop index="pcrs" from="1" to="20">#Evaluate("ranks2.im#pcrs#")#<cfif #pcrs# LT 20>,</cfif></cfloop>#NewLine#</CFOUTPUT>
	</cfprocessingdirective>