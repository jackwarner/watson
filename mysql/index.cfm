<!DOCTYPE html PUBLIC "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="GENERATOR" content="Mozilla/4.77 [en]C-CCK-MCD NSComm475CWM  (Windows NT 5.0; U) [Netscape]">
  <title>Survey Application, Neill Watson</title>
  <style type="text/css">
<!--
a.special:link {
     color: #000000;
     }
a.special:visited {
     color: #000000;
}-->
  </style>
</head>


<body alink="#0000ff" link="#0000ff" vlink="#0000ff">







&nbsp;
<table border="0" width="100%">







  <tbody>






    <tr valign="CENTER">







      <td colspan="2">
      
      
      
      
      
      
      <h2><font color="#0000FF">WELCOME TO THE RESEARCH SITE OF<br>
NEILL WATSON, PH.D.<br>
THE COLLEGE OF WILLIAM &amp; MARY</font><br>
<font color="#0000FF"><em>for Authorized Researchers</em></font></h2>      </td>
    </tr>








    <tr>







      <td colspan="2">
      
      
      
      
      
      
      <hr></td>
    </tr>








    <tr>







      <td colspan="2">
      
      
      
      
      
      
      <h3> The testing questionnaires have been disabled temporarily to preserve the integrity of research data.
<br>
      <script language="JavaScript1.2">
<!-- Original:  Craig Lumley -->
<!-- Web Site:  http://www.craiglumley.co.uk -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
<!-- Begin
function fullScreen(theURL) {
window.open(theURL, '', 'fullscreen=yes, scrollbars=yes');
}
//  End -->

var bestwidth = 1024;
var bestheight = 768;
if (screen.width < bestwidth || screen.height < bestheight) {
msg = "<hr>The surveys on this site are intended to be administered on systems with a screen resolution of at least"
+ bestwidth + "x" + bestheight + ". Your"
+ " screen resolution is " + screen.width + "x"
+ screen.height + ".  Please change your screen resolution "
+ "if possible.  You will not see this message if you screen is set to the proper resolution.";
document.write(msg);
}
//  End -->
      </script>
      </h3>      </td>
    </tr>








    <tr valign="top">







      <td colspan="2">
      
      
      
      
      
      
      <hr></td>
    </tr>









 <tr>
      <td valign="top"><center>
          <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('../meta/RIOPC/frame_.cfm?num=0&myID=0&myTo=1&mySkip=x');">Metaprogram - Last Modified Date: Sept 16, 2007 </a></h3>
        <p>Change Log:<br>
        Initial Revision 2007-09-15 10:50 AM <br>
        2007-09-16 05:40 PM: Remove pre-filled questionnaire values </p>
      </td>
    </tr>

<!---
      <td rowspan="2" valign="top">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>








      <td>
      
      
      
      
      
      
      <h3>
      <a href="javascript:fullScreen('RIOPC/frame_.cfm?num=0&myID=0&myTo=1&mySkip=x');">Self-Concept
Questionnaire - Personal Constructs</a>&nbsp;</h3>      </td>
    </tr>








    <tr>







      <td>
      
      
      
      
      
      
      <blockquote>
        
        
        
        
        
        
        <h4>
An idiographic measure of real self, ideal self, and ought self components
of self-concept, from which self-discrepancies are computed.</h4>
      </blockquote>      </td>
    </tr>








    <tr>







      <td rowspan="2" valign="top" width="9%">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>








      <td width="91%">
      
      
      
      
      
      
      <h3>
        <a href="javascript:fullScreen('RIOCC/frame.cfm?num=0&myID=0&myTo=1&mySkip=x');">Self
Concept Questionnaire - Conventional Constructs</a></h3>      </td>
    </tr>








    <tr>







      <td>
      
      
      
      
      
      
      <blockquote>
        
        
        
        
        
        
        <h4>
A nonidiographic measure of real self, ideal self, and ought self components
of self-concept, from which self-discrepancies are computed.</h4>
      </blockquote>      </td>
    </tr>








    <tr>







      <td rowspan="2" valign="top">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>
      <td>
      
      
      
      
      
      
      <h3>
      <a href="javascript:fullScreen('AbstractRIc/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Real-Ideal
Discrepancy Abstract Measure</a></h3>      </td>
    </tr>








    <tr>







      <td>
      
      
      
      
      
      
      <blockquote>
        
        
        
        
        
        
        <h4>
A very brief, content-free measure of the discrepancy between real self
and ideal self.</h4>
      </blockquote>      </td>
    </tr>








    <tr>







      <td rowspan="2" valign="top">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>








      <td>
      
      
      
      
      
      
      <h3><a href="javascript:fullScreen('AbstractROc/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">
      Real-Ought
Discrepancy Abstract Measure</a></h3>      </td>
    </tr>








    <tr>







      <td>
      
      
      
      
      
      
      <blockquote>
        
        
        
        
        
        
        <h4>
A very brief, content-free measure of the discrepancy between real self
and ought self.</h4>
      </blockquote>      </td>
    </tr>








    <tr>







      <td valign="top">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>








      <td>
      
      
      
      
      
      
      <h3>
        <a href="javascript:fullScreen('Ideal/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Importance
of Ideal Self</a></h3>      </td>
    </tr>








    <tr>







      <td valign="top">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>








      <td>
      
      
      
      
      
      
      <h3>
        <a href="javascript:fullScreen('Ought/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Importance
of Ought Self</a></h3>      </td>
    </tr>








    <tr>







      <td valign="top">
      
      
      
      
      
      
      <center><img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30"></center>      </td>








      <td>
      
      
      
      
      
      
      <h3>
        <a href="javascript:fullScreen('RefPerson/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Reference
Person(s) for Ought Self&nbsp;</a></h3>      </td>
    </tr>
    <tr>
      <td valign="top"><center>
          <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('DASS/dass.cfm');">DASS</a></h3></td>
    </tr>
    <tr>
      <td valign="top"><center>
          <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('BDI/frame.cfm?myID=123');">BDI</a></h3></td>
    </tr>
    <tr>
      <td valign="top"><center>
          <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('CESD/cesd.cfm?myID=123');">CESD</a></h3></td>
    </tr>
    <tr>
      <td valign="top"><center>
          <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('BAI/bai.cfm?myID=123');">BAI</a></h3></td>
    </tr>
    <tr>
      <td valign="top"><center>
          <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('STAI/frame.cfm?myID=0&num=0&myTo=0');">STAI</a></h3></td>
    </tr>
    <tr>
      <td valign="top"><center>
        <img src="http://www.wm.edu/research/watson/survey.jpg" nosave="" height="38" width="30">
      </center></td>
      <td><h3> <a href="javascript:fullScreen('DEQ/deq.cfm');">DEQ</a></h3></td>
    </tr>
  --->
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><font size="-2">Copyright 2004</font>
      <br>






      <font size="-2">Neill Watson, Ph.D.</font>
      <br>






      <font size="-2">College of William &amp; Mary</font>
      <br>






      <font color="#000066"><font size="-2"><a href="mailto:npwats@wm.edu">npwats@wm.edu</a></font></font></td>
    </tr>
  </tbody>
</table>








</body>
</html>
