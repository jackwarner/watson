<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfset mySource = "watson">
<cfset myTable = "dass">
<cfif NOT isDefined("Form.myID")>
	<cfset myID = 0>
<cfelse>
	<cfset myID = #Form.myID#>
</cfif>

<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I found myself getting upset by quite trivial things.">
	<cfset myQuestions[2] = "I was aware of dryness of my mouth.">
	<cfset myQuestions[3] = "I couldn't seem to experience any positive feeling at all.">
    <cfset myQuestions[4] = "I experienced breathing difficulty (e.g., excessively rapid breathing, breathlessness in the absence of physical exertion).">
	<cfset myQuestions[5] = "I just couldn't seem to get going.">
	<cfset myQuestions[6] = "I tended to over-react to situations.">
	<cfset myQuestions[7] = "I had a feeling of shakiness (e.g., legs going to give way).">
	<cfset myQuestions[8] = "I found it difficult to relax.">
	<cfset myQuestions[9]= "I found myself in situations that made me so anxious I was most relieved when they ended.">
	<cfset myQuestions[10]= "I felt that I had nothing to look forward to.">
	<cfset myQuestions[11]= "I found myself getting upset rather easily.">
	<cfset myQuestions[12]= "I felt that I was using a lot of nervous energy.">
	<cfset myQuestions[13]= "I felt sad and depressed.">
	<cfset myQuestions[14]= "I found myself getting impatient when I was delayed in any way (e.g., elevators, traffic lights, being kept waiting).">
	<cfset myQuestions[15]= "I had a feeling of faintness.">
	<cfset myQuestions[16]= "I felt that I had lost interest in just about everything.">
	<cfset myQuestions[17]= "I felt I wasn't worth much as a person.">
	<cfset myQuestions[18]= "I felt that I was rather touchy.">
	<cfset myQuestions[19]= "I perspired noticeably (e.g., hands sweaty) in the absence of high temperatures or physical exertion.">
	<cfset myQuestions[20]= "I felt scared without any good reason.">
	<cfset myQuestions[21]= "I felt that life wasn't worthwhile.">
	<cfset myQuestions[22]= "I found it hard to wind down.">
	<cfset myQuestions[23]= "I had difficulty in swallowing.">
	<cfset myQuestions[24]= "I couldn't seem to get any enjoyment out of the things I did.">
	<cfset myQuestions[25]= "I was aware of the action of my heart in the absence of physical exertion (e.g., sense of heart rate increase, heart missing a beat).">
	<cfset myQuestions[26]= "I felt down-hearted and blue.">
	<cfset myQuestions[27]= "I found that I was very irritable.">
	<cfset myQuestions[28]= "I felt I was close to panic.">
	<cfset myQuestions[29]= "I found it hard to calm down after something upset me.">
	<cfset myQuestions[30]= "I feared that I would be 'thrown' by some trivial but unfamiliar task.">
	<cfset myQuestions[31]= "I was unable to become enthusiastic about anything.">
	<cfset myQuestions[32]= "I found it difficult to tolerate interruptions to what I was doing.">
	<cfset myQuestions[33]= "I was in a state of nervous tension.">
	<cfset myQuestions[34]= "I felt I was pretty worthless.">
	<cfset myQuestions[35]= "I was intolerant of anything that kept me from getting on with what I was doing.">
	<cfset myQuestions[36]= "I felt terrified.">
	<cfset myQuestions[37]= "I could see nothing in the future to be hopeful about.">
	<cfset myQuestions[38]= "I felt that life was meaningless.">
	<cfset myQuestions[39]= "I found myself getting agitated.">
	<cfset myQuestions[40]= "I was worried about situations in which I might panic and make a fool of myself.">
	<cfset myQuestions[41]= "I experienced trembling (e.g., in the hands).">
	<cfset myQuestions[42]= "I found it difficult to work up the initiative to do things.">
<cfset page = 1>
<cfset qPerPage = 11>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
<cfelseif isDefined("Form.Code")>
	<cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> 
	<cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  	<cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  	<cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  	<cfset finalDate = #todayFormatDate# & #todayFormatTime#>
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable#(Code, Gender, datecreated) VALUES ('#Form.Code#', '#Form.Gender#', '#finalDate#')  
				</cfquery>
				<CFQUERY name="getMaxID" datasource="#mySource#">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			<cfset page = 1>
	</cfif> 
<cfelse>
	 <cfform action="dass.cfm" method="post">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant 3-digit code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>	
</cfif>

<cfset title = "Depression, Anxiety, and Stress Scale">

<cfset instructions = "Please read each statement and choose a number 0, 1, 2 or 3 that indicates how much the statement applied to you <i>over the past week</i>.">

<cfset instructions = instructions & "  There are no right or wrong answers.  Do not spend too much time on any statement.">

<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>


<cfif page GT totalPages>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8"><h3>Thank you for completing the <cfoutput>#title#.</cfoutput></h3>
	<input type="button" value="Close the Window" name="close" onClick="window.close()">
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="ew.js"></script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">
if (EW_this.dass#i# && !EW_hasValue(EW_this.dass#i#, "RADIO" )) {
	if (!EW_onError(EW_this, EW_this.dass#i#, "RADIO", "Please enter a response for Question #i#."))
		return false;
}
</cfloop>
</cfoutput>
return true;
}
//-->
</script>

<form method="post" onSubmit="return EW_checkMyForm(this);">
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8"><h3><cfoutput>#instructions#</cfoutput></h3></td>
  </tr>
  <tr>
    <td colspan="8"><div align="right"><img src="dass.gif" width="412" height="133"></div></td>
    </tr>
  <cfoutput>
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
  	<td width="185" rowspan="2"><font size="2">#i#. #myQuestions[i]#</font></td>
	<cfloop from="0" to="3" index="j">
	<td width="100"><div align="center"><font size="2">#j#</font></div></td>
	</cfloop>
  </tr>
  <tr <cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
 	<cfloop from="0" to="3" index="k">
	<td width="100" valign="top"><div align="center"> <font size="2">
   	     <input type="radio" name="dass#i#" value="#k#">
   	 </font></div></td>
	</cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
    <td height="43">&nbsp;</td>
    <td colspan="7"><div align="left"> <br>
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
		<cfif (page+1) GT totalPages>
		 	<input type="submit" name="Submit" value="Continue to Final Page">
		<cfelse>
		 	 <input type="submit" name="Submit" value="Continue to Page <cfoutput>#page+1# of #totalPages#</cfoutput>">
		</cfif>    
    </div></td>
  </tr>
</table>
</form>
</cfif>
</body>
</html>
