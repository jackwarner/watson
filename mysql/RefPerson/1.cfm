

<cfform action="frame.cfm?num=#myIndex+1#&myTo=end&myId=#myID#" method="post">
<center>
    <table width="585" border="0" cellpadding="0" cellspacing="0">
      <tr valign="top"> 
        <td height="10" colspan="3"> 
          <div align="left">
            <h3><strong>When you answered the questions about yourself as others 
              think you ought or should be, what person(s) did you have in mind? 
              Please check all that apply.</strong></h3>
          </div></td>
      </tr>
      <tr> 
        <td colspan="3" valign="middle"><hr></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td valign="middle"> <div align="right">Mother</div></td>
        <td width="10" colspan="-6" valign="top">&nbsp;</td>
        <td width="261" valign="top"> <input name="checkbox1" type="checkbox" id="checkbox1" value="Mother"></td>
      </tr>
      <tr> 
        <td valign="top"><div align="right">Father</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"><input type="checkbox" name="checkbox2" value="Father"></td>
      </tr>
      <tr bordercolor="#FFFFCC" bgcolor="#FFFFCC"> 
        <td valign="top"> <div align="right">Sibling(s)</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"> <input type="checkbox" name="checkbox3" value="Sibling"></td>
      </tr>
      <tr> 
        <td valign="top"><div align="right">Friends</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"><input type="checkbox" name="checkbox4" value="Friend"></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td valign="top"> <div align="right">Close Friend(s)</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"> <input name="checkbox5" type="checkbox" id="checkbox5" value="Close Friend"></td>
      </tr>
      <tr> 
        <td valign="top"><div align="right">Girlfriend</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"><input name="checkbox6" type="checkbox" id="checkbox2" value="Girl Friend"></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td valign="top"> <div align="right">Boyfriend</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"> <input name="checkbox7" type="checkbox" id="checkbox7" value="Boy Friend"></td>
      </tr>
      <tr> 
        <td valign="top"><div align="right">Spouse</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"><input name="checkbox8" type="checkbox" id="checkbox5" value="Spouse"></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td valign="top"> <div align="right">Peers</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"> <input name="checkbox9" type="checkbox" id="checkbox7" value="Peers"></td>
      </tr>
      <tr> 
        <td valign="top"><div align="right">Society</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"><input name="checkbox10" type="checkbox" id="checkbox8" value="Society"></td>
      </tr>
      <tr bgcolor="#FFFFCC"> 
        <td valign="top"> <div align="right">Other</div></td>
        <td colspan="-6" valign="top">&nbsp;</td>
        <td valign="top"> <input name="others" type="text" id="others" size="40" maxlength="50" value="Please type whom you had in mind." onFocus="if(this.value=='Please type whom you had in mind.')this.value='';"></td>
      </tr>
      <tr> 
        <td colspan="3" valign="top"> <blockquote> 
            <p><div align="left"> <br>
            <input type="submit" name="Submit" value="Continue"></p> 
          </blockquote></td>
      </tr>
    </table>
</center>
</cfform>
