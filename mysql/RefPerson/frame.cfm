
<html>
<head>
<title>Reference Person(s) for Ought Self</title>
</head>
<cfset mySource = "watson">
<cfset myTable = "refperson">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 2>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="20"> 
        <h5> 
          <cfif #myIndex# EQUAL 0>Introduction for Researcher
	    <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	  Page 1 of 1.
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        <cfif myIndex EQUAL 0>
          <h3>Reference Person(s) for Ought Self 
            <cfelse>
          </h3>
          </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
        <cfif myIndex EQUAL 0>
          <cfform action="frame.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant 3-digit code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1"></font> 
        </cfif> </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  	<cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  	<cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  	<cfset finalDate = #todayFormatDate# & #todayFormatTime#>
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable#(Code, Gender, datecreated) VALUES ('#Form.Code#', '#Form.Gender#', '#finalDate#')  
				</cfquery>
				<CFQUERY name="getMaxID" datasource="#mySource#">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
			 
			<cfinclude template="#myIndex#.cfm"> &nbsp;
		  
		  
	  </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>     
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>
