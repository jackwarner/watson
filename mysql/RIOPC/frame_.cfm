<html>
<head>
<title>RIOPC</title>
</head>
<cfset mySource = "watson">
<cfset myTable = "riopc">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 21>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
  <cfif myIndex EQUAL 0>
    <tr valign="top"> 
      <td height="17"> <h5>
          <cfif #myIndex# EQUAL 0>Introduction for Researcher
		  <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	  Page <cfoutput>#myIndex# of #myPages#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td height="10" valign="middle"> 
        <cfif myIndex EQUAL 0>
          <p>This program allows a researcher to 
            measure the respondent's REAL, IDEAL, and OUGHT (other) components 
            of self-concept.</p>
          <p>The program<br>
            <blockquote>1. elicits six personality characteristics for each of 
              these three components of self-concept,<br>
              2. elicits the opposite of each of the 18 characteristics, and<br>
              3. obtains ratings of all 36 characteristics in response to instructions 
              for each of the three components of self-concept. <BR><BR>These characteristics 
              are presented in the same random order for ratings for each of the 
              three components of self-concept.</blockquote>
            <cfelse></p>
          </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td> <cfif myIndex EQUAL 0>
          <cfform action="frame_.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="58%"><strong>Participant 3-digit code:</strong></td>
                  <td width="42%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr>
                  <td><strong>Choose from the following elicitation orders:</strong>&nbsp;</td>
                  <td>	<select name="Order">
						  <option value="1">R/I/O
						  <option value="2">R/O/I
						  <option value="3">O/R/I
						  <option value="4">O/I/R
						  <option value="5">I/O/R
						  <option value="6">I/R/O
						</select>&nbsp;</td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1">Copyright 2004<br>
          Neill Watson, Ph.D.<br>
          College of William &amp; Mary<br>
          <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font> 
          <!---  <cfelseif myIndex LESS THAN 4>
        <h3><cfoutput>#myIndex#. #aTitles[myIndex]#</cfoutput></h3></td>--->
        </cfif> 
	  </td>
	</tr>
	<cfelse>
	<tr valign="top"> 
      <td height="17"> 
	  <h5>Page <cfoutput>#myIndex# of #myPages#.</cfoutput></h5></td>
    </tr>
	</cfif>
    <tr valign="top"> 
      <td> 
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  	<cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  	<cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  	<cfset finalDate = #todayFormatDate# & #todayFormatTime#>
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable#(Code, Gender, numpick, datecreated) VALUES ('#Form.Code#', '#Form.Gender#', '#Form.Order#', '#finalDate#') 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="#mySource#">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			<cfset URL.myID = #myID#>
			<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
			 <!---<cfinclude template="#myInclude#_.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x">--->
			 
				<center>
				  <table width="585" border="0" cellpadding="0" cellspacing="0">
					<tr valign="top"> 
					  <td width="50%" colspan="8"> <h3>&nbsp; </h3></td>
					</tr>
					<tr align="left" valign="top"> 
					  <td height="43" colspan="8"> 
						<blockquote> 
						  <h3>First, you are asked to list personality characteristics that describe 
							three different ways of viewing yourself.</h3>
						  <ul>
							<li>
							  <h3>REAL SELF - Yourself as YOU see yourself in your own eyes.</h3>
							</li>
							<li>
							  <h3>IDEAL SELF - Yourself as YOU would like to be in your own eyes.</h3>
							</li>
							<li>
							  <h3>OUGHT/SHOULD SELF - Yourself as OTHERS think you ought or should 
								be.</h3>
							</li>
						  </ul>
						  <h3>For each characteristic, use one word or a very short phrase. Just 
							type the characteristic; do not type a sentence like &quot;I am ...&quot;</h3>
						  <h3>You are asked for your own view. There are no right or wrong answers.</h3>
						</blockquote></td>
					</tr>
					<tr valign="top"> 
					<!--- 	REAL 	= 2.cfm
						IDEAL 	= 3.cfm
						OUGHT	= 4.cfm --->
						<cfquery name="Getroi" datasource="#mySource#"> 
							SELECT numpick FROM #myTable# WHERE Id=#myId#
						</cfquery>
				<cfif Getroi.numpick EQ 1 OR Getroi.numpick EQ 2>
					<!--- real --->
				<cfset going="2">
				</cfif>
				<cfif Getroi.numpick EQ 3 OR Getroi.numpick EQ 4>
				<!--- ought --->
				<cfset going="4">
				</cfif>
				<cfif Getroi.numpick EQ 5 OR Getroi.numpick EQ 6>
				<!--- ideal --->
				<cfset going="3">
				</cfif>
				
				<cfform action="frame_.cfm?num=#myIndex+1#&myTo=#going#&myID=#myID#">
				
					  <td height="43">&nbsp;</td>
					  <td colspan="7"> <div align="left"> <br> <input type="submit" name="Submit" value="Continue"> 
					  </td>
				</cfform>
					</tr>
				  </table>
				</center>


	  </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>     
	  		<cfset URL.num = #myIndex#+1>
			<cfset URL.myTo = #myInclude#>
			<cfset URL.myID = #myID#>
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>