
<html>
<head>
<title>STAI</title>
</head>
<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="login.cfm">
	
</cfif>
<cfset mySource="watson">
<cfset myTable = "stai">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>


<!---
<cfquery name="Getroi" datasource="roi">
 		SELECT numpick, BigOrder FROM survey WHERE Id=#URL.myId#
</cfquery>

<cfset myQuest = "8">
	<cfif Getroi.BigOrder EQ 2 OR Getroi.BigOrder EQ 3 OR Getroi.BigOrder EQ 4 OR Getroi.BigOrder EQ 6 OR Getroi.BigOrder EQ 9 OR Getroi.BigOrder EQ 11>
			<cfset myQuest = "9">
	</cfif>
--->
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 3>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="17"> <h5> 
          <cfif #myIndex# EQUAL 0>Introduction
		  <cfelseif #myIndex# GTE #myPages#>
	  <cfelse>
	  
	  
	  <cfoutput>Questionnaire 1 of 1. Page #myIndex# of #myPages-1#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td valign="top"> 
        <cfif myIndex EQUAL 0>
          <p align="left"><strong>Instructions for researcher:</strong> This is 
            the Trait Scale of the State-Trait Anxiety Inventory.<br>
            <br>
            <font size="1">&copy; Copyright 1968, 1977 by Charles Spielberger. 
            All rights reserved.<br>
            Published by Mind Garden, Inc., 1960 Woodside Rd, Suite 202, Redwood 
            City, CA 94061</font></p>
          <p align="center"><font size="4"><strong>Directions</strong></font> 
          </p>
          <p>
            <cfelseif myIndex LESS THAN myPages>
            <strong>A number of statements which people have used to describe 
            themselves are given below. Read each statement and then select the 
            appropriate number to the right of the statement to indicate how you 
            <em>generally</em> feel. There are no right or wrong answers. Do not 
            spend too much time on any one statement but give the answer which 
            seems to describe how you generally feel. </strong> </p>
        </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td valign="middle"> 
        <cfif myIndex EQUAL 0>
          <cfform action="frame.cfm?num=1&myID=1&myTo=#myInclude#&mySkip=x">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
	
		
        </cfif> </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  	<cfif #URL.mySkip# EQ "x">
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
				<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
				code. Click to go back.</a> <cfelse> 
				<cflock name="#CreateUUID()#" timeout="20">
				  <cftransaction>
					<cfquery name="myQ" datasource="#mySource#">
					INSERT INTO #myTable#(Code, Gender) VALUES ('#Form.Code#', '#Form.Gender#') 
					</cfquery>
					<CFQUERY name="getMaxID" datasource="#mySource#">
					SELECT Max(Id) AS myID FROM #myTable# 
					</CFQUERY>
				  </cftransaction>
				</cflock> 
				<cfset myID = #getMaxID.myID#> 
				<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
				 
				<cfinclude template="#myIndex#.cfm"> &nbsp;
				</cfif>
		  <cfelse>
		   
			<cfinclude template="#myIndex#.cfm">
	  </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>     
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	   
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>
