
<html>
<head>
<title>Ought Self</title>
</head>
<cfset mySource = "watson">
<cfset myTable = "oughtself">
<cfset myIndex = #URL.num#>
<cfset myID = #URL.myID#>

<cfset myQuest = "6">


<cfset myInclude = #URL.num#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 2>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="17"> <h5>  
          <cfif #myIndex# EQUAL 0>Introduction for Researcher
	    <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	   <cfoutput>Page #myIndex# of #myPages#.</cfoutput>
	  
	  </cfif></h5></td>
    </tr>
    <tr> 
      <td height="10" valign="middle"> 
        <cfif myIndex EQUAL 0>
          <p><strong>Instructions:</strong> This is a measure of the importance 
            to the person of his or her ought/should self (oneself as OTHERS think 
            one ought or should be).</p>
          <cfelse>
        </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td> <cfif myIndex EQUAL 0>
          <cfform action="frame.cfm?num=1&myID=1&mySkip=x">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant 3-digit code:</strong></td>
                  <td><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1">Copyright 2004<br>
          Neill Watson, Ph.D.<br>
          College of William &amp; Mary<br>
          <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font> 
          <!---  <cfelseif myIndex LESS THAN 4>
        <h3><cfoutput>#myIndex#. #aTitles[myIndex]#</cfoutput></h3></td>--->
        </cfif> </tr>
    <tr valign="top"> 
      <td height="46"> 
	  
	  <cfif myIndex EQUAL 1> 
	  	<cfif #URL.mySkip# EQ "x" >
		 	 <cfif Len(#Form.Code#) NOT EQUAL 3> 
				<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
				code. Click to go back.</a> 
			<cfelse> 
				<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  <cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  <cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  <cfset finalDate = #todayFormatDate# & #todayFormatTime#>
				  <cftransaction>
					<cfquery name="myQ" datasource="#mySource#">
					INSERT INTO #myTable#(Code, Gender, datecreated) VALUES ('#Form.Code#', '#Form.Gender#', '#finalDate#')  
					</cfquery>
					<CFQUERY name="getMaxID" datasource="#mySource#">
					SELECT Max(Id) AS myID FROM #myTable# 
					</CFQUERY>
				  </cftransaction>
				</cflock> 
				<cfset myID = #getMaxID.myID#> 
				<!--- <cfoutput>#getMaxID.myID#!!!</cfoutput> --->
				<cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x"> 
				<cfinclude template="#myIndex#.cfm"> &nbsp;</td>
				  <td width="1"></cfform> 
				  <td width="16">
			</cfif>
		<cfelse>
				  <cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&mySkip=x"> 
					<cfinclude template="#myIndex#.cfm"> &nbsp;</td>
				  <td width="1"></cfform> 
	
	  </cfif> 
	
	  
	  
	  <cfelseif myIndex GREATER THAN 1> 
      <cfform action="frame.cfm?num=#myIndex+1#&myID=#myID#&myTo=2&mySkip=x">
        <cfinclude template="#myInclude#.cfm">
        &nbsp; 
        <td width="1"></td>
      </cfform>
      <td width="1"></cfif> </tr>
    <tr valign="top"> 
      <td> <blockquote>
          <div align="left"></div>
        </blockquote></td>
    </tr>
  </table>
</center>
</body>
</html>
