<!--- use cfsetting to block output of HTML 
outside of cfoutput tags --->
<cfsetting enablecfoutputonly="Yes">
<!--- set vars for special chars --->
<cfset TabChar = ",">
<cfset NewLine = "#Chr(13)##Chr(10)#">
<!--- set content type to invoke Excel --->
<cfcontent type="application/msexcel">
<!--- suggest default name for XLS file --->
<!--- use "Content-Disposition" in cfheader for 
Internet Explorer  --->
<cfheader name="Content-Disposition" value="filename=characteristics.csv">
<!---
PC#delim# CC#delim# AM#delim# Ref#delim# Imp#delim# IM#delim# BDI#delim# BAI#delim# DASS#delim# STAIT#delim# CESD (MySQL)
--->
<CFQUERY DATASOURCE="watson" NAME="ranks">
SELECT *
FROM pc_cc_am_imp_ref_users 
WHERE UserId = #URL.myUser#
ORDER BY Id
</CFQUERY>
<body>
<cfset delim = #TabChar#>
	<cfset myDataBase = "ParticipantCode" & delim & "TakenOn">
	<cfloop index="j" from="1" to="6">
		<cfset myDataBase = myDatabase & delim & "REAL" & #j#>
	</cfloop>
    <cfloop index="j" from="1" to="6">
		<cfset myDataBase = myDatabase & delim & "OP_REAL" & #j#>
	</cfloop>
	<cfloop index="k" from="1" to="6">
		<cfset myDataBase = myDatabase & delim & "IDEAL" & #k#>
	</cfloop>
    <cfloop index="k" from="1" to="6">
		<cfset myDataBase = myDatabase & delim & "OP_IDEAL" & #k#>
	</cfloop>
	<cfloop index="pcrs" from="1" to="6">
		<cfset myDataBase = myDatabase & delim & "OUGHT" & #pcrs#>
	</cfloop>
	<cfloop index="pcrs" from="1" to="6">
		<cfset myDataBase = myDatabase & delim & "OP_OUGHT" & #pcrs#>
	</cfloop>
	<!--- <cfset myDataBase = myDataBase & ",ImpI1,ImpI2,ImpI3,ImpO1,ImpO2,ImpO3<BR>"> --->
	<cfset myDataBase = myDataBase & NewLine>
	<cfoutput>#myDataBase#</cfoutput>
	  	<CFOUTPUT QUERY="ranks">#Code#,#TakenOn#,#real1#,#real2#,#real3#,#real4#,#real5#,#real6#,#opr1#,#opr2#,#opr3#,#opr4#,#opr5#,#opr6#,#ideal1#,#ideal2#,#ideal3#,#ideal4#,#ideal5#,#ideal6#,#opid1#,#opid2#,#opid3#,#opid4#,#opid5#,#opid6#,#ought1#,#ought2#,#ought3#,#ought4#,#ought5#,#ought6#,#opou1#,#opou2#,#opou3#,#opou4#,#opou5#,#opou6##NewLine#
  		</CFOUTPUT>