<cfset myTitle = "PC, CC, AM, Imp, Ref">
<html>
<head>
<title><cfoutput>#myTitle#</cfoutput></title>
</head>

<body><center>
<h3><div style="text-decoration: underline;">Introduction and Instructions</div></h3>
  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr> 
      <td valign="middle"> 
        
          <h4> 	
This program administers the following questionnaires together in a single
administration:
      <blockquote>
       Self-Discrepancy Questionnaire - Personal Constructs<br>
       Self-Discrepancy Questionnaire - Conventional Constructs<br>
       Real-Ideal Discrepancy and Real-Ought Discrepancy Abstract Measures<br>
       Importance of Ideal Self and Importance of Ought Self<br>
       Reference Person(s) for Ought Self<br>
       </blockquote>
The group of questionnaires is administered in 12 partially counterbalanced
orders to each successive group of 12 participant code numbers. In the
personal construct and conventional construct questionnaires, the orders of
the real, ideal, and ought selves are completely counterbalanced.
            <table>
             <tr>
                 <td colspan="2">
                <br>Description of the Self-Discrepancy Questionnaires:&nbsp;<a href="../Watson_Description.pdf" target="_dinstructions">PDF</a><br>
                 </td>
                </tr>
                
                <tr>
                 <td colspan="2">
                <br>Instructions for Downloading Data &amp; Importing into PASW Statistics 18 (SPSS):&nbsp;<a href="../Instructions.pdf" target="_instructions">PDF</a><br>
                 </td>
                </tr>
                <tr>
                <td><br></td>
                </tr>
                <tr>
                 <td colspan="2">
                 Syntax for Scoring the Questionnaires:&nbsp;<a href="../Syntax_file.sps.zip" target="_syntax">Zip</a>&nbsp;|&nbsp;<a href="../Syntax_file.txt" target="_syntax">Txt</a>
                 </td>
                </tr>
                <tr>
                <td><br></td>
                </tr>
                <tr>
                 <td colspan="2">
                 Instructions for Downloading Syntax File:&nbsp;<a href="../Syntax_Instructions.pdf" target="_syntax_i">PDF</a></a>
                 </td>
                </tr>
                 <tr>
                <td><br></td>
                </tr>
                <tr>
                 <td colspan="2">
                 <a href="../auth4.cfm">Return to previous page to download the Self-Discrepancy Questionnaires - Zip File with Shortcut</a>.
                 </td>
                </tr>
            </table>
        </td>
    </tr>
  </table>
</center>
</body>
</html>
