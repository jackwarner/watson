<!---<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="../login.cfm">
	
</cfif>--->
<cfset myTitle = "PC, CC, AM, Imp, Ref">
<html>
<head>
<title><cfoutput>#myTitle#</cfoutput></title>
</head>
<cfset mySource = "watson">
<cfset myTable = "pc_cc_am_imp_ref_users">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<cfset myUser = #URL.myUser#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfcookie name="whichQuestionnaire" value="1" expires="1">
<cfset myPages = 2>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="20"> 
        <h5> 
         <!---  <cfif #myIndex# EQUAL 0><cfelse>
	  Page <cfoutput>#myIndex# of #myPages#</cfoutput>
	  
	  </cfif>---> </h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        <cfif myIndex EQUAL 0>
          <h3> 	
<cfoutput>#myTitle#</cfoutput>
            <cfelse>
          </h3>
        </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
        <cfif myIndex EQUAL 0>
          <cfform action="random_array.cfm?num=1&myID=1&myTo=#myInclude#&myUser=#myUser#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td width="50%"><strong>Participant age:</strong></td>
                  <td width="47%"><cfinput name="Age" type="text" maxlength="3" width="5"></td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender.">
                    <!--<input type="hidden" name="Age_required" value="Please enter an age."> -->
                  </td>
                  <td>
				  <input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1"></font> 
        </cfif> </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3 OR NOT LSIsNumeric(#Form.Code#) OR #Form.Code# LTE 0 > 
			<a href="javascript:history.go(-1)">Please enter a 3 digit numeric researcher 
			code greater than 000. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				<cfset myBigOrder = (#Form.Code#) MOD 48>
					<!--- 
					BDI First: 2, 3, 4, 6, 9, 11
					STAI first: 1, 5, 7, 8, 10, 0
					Types in | Calculates To | 
					001        1
					002        2
					003        3
					004        4
					005        5
					006        6
					007        7
					008        8
					009        9
					010        10
					011        11
					012        0
						1> 10 = O/I/R/  (4) STAI/BDI
						2> 04 = O/I/R/	(4) BDI/STAI
						3> 02 = R/O/I/	(2) BDI/STAI
						4> 05 = I/O/R/	(5) BDI/STAI
						5> 08 = R/O/I/	(2) STAI/BDI
						6> 06 = I/R/O/	(6) BDI/STAI
						7> 09 = O/R/I/	(3) STAI/BDI
						8> 07 = R/I/O/	(1) STAI/BDI
						9> 01 = R/I/O/  (1) BDI/STAI
						10> 11 = I/O/R/	(5) STAI/BDI
						11> 03 = O/R/I/ (3) BDI/STAI
						12 a.k.a 0> 12 = I/R/O/ (6) STAI/BDI 
						--------------------------------------
						NEW STUFF FOR LATENT VARIABLE:
						
						--->
					<cfif myBigOrder EQ 5 or myBigOrder EQ 1 or myBigOrder EQ 32 or myBigOrder EQ 46 or myBigOrder EQ 2 or myBigOrder EQ 3 or myBigOrder EQ 30 or myBigOrder EQ 20>
						<cfset insertPick = 4>
					</cfif>
					<cfif myBigOrder EQ 7 or myBigOrder EQ 25 or myBigOrder EQ 12 or myBigOrder EQ 22 or myBigOrder EQ 33 or myBigOrder EQ 45 or myBigOrder EQ 28 or myBigOrder EQ 13>
						<cfset insertPick = 2>
					</cfif>
					<cfif myBigOrder EQ 15 or myBigOrder EQ 42 or myBigOrder EQ 23 or myBigOrder EQ 26 or myBigOrder EQ 21 or myBigOrder EQ 36 or myBigOrder EQ 14 or myBigOrder EQ 6>
						<cfset insertPick = 5>
					</cfif>
					<cfif myBigOrder EQ 29 or myBigOrder EQ 47 or myBigOrder EQ 31 or myBigOrder EQ 35 or myBigOrder EQ 38 or myBigOrder EQ 4 or myBigOrder EQ 16 or myBigOrder EQ 24>
						<cfset insertPick = 6>
					</cfif>
					<cfif myBigOrder EQ 44 or myBigOrder EQ 34 or myBigOrder EQ 19 or myBigOrder EQ 18 or myBigOrder EQ 17 or myBigOrder EQ 48 or myBigOrder EQ 10 or myBigOrder EQ 9>
						<cfset insertPick = 3>
					</cfif>
					<cfif myBigOrder EQ 43 or myBigOrder EQ 37 or myBigOrder EQ 40 or myBigOrder EQ 39 or myBigOrder EQ 41 or myBigOrder EQ 8 or myBigOrder EQ 11 or myBigOrder EQ 27>
						<cfset insertPick = 1>
					</cfif>
				INSERT INTO #myTable#(Code, Gender, BigOrder, numpick, UserId) VALUES ('#Form.Code#', '#Form.Gender#', #myBigOrder#, '#insertPick#', #myUser#) 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="#mySource#">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			
			<cfquery datasource="#mySource#">
				INSERT INTO #myTable#_supplemental_users (FK) VALUES (#myID#)
			</cfquery>
            <!--- <cfoutput>#getMaxID.myID#!!!</cfoutput>
			<cfinclude template="#myIndex#.cfm"> --->
			
			
			<cfif myFrom EQ "">
            <h3>Thank you for volunteering to participate in this study. You are 
              asked first to complete thirteen questionnaires on the computer. Four 
              of them are very brief. After that, you are asked to complete the 
              two paper-and-pencil questionnaires in the envelope that you were 
              given. <br>
              <br>
			  <cfif insertPick EQ 1 or insertPick EQ 5 or insertPick EQ 6>
			  Click <a href="RIOPC/frame_.cfm?num=2&myID=<cfoutput>#myID#</cfoutput>&myTo=1">here</a> 
              to begin the questionnaires.
			  <cfelseif insertPick EQ 2 or insertPick EQ 3 or insertPick EQ 4>
			  		
			  </cfif>
               &nbsp; 
			</h3>
			<cfelse>
			
			</cfif>
			  <cfif true>
				  <cfoutput>
				 	 Order for Participant #Form.Code#: 
				  </cfoutput>
			  </cfif>
          </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>   
	  	You should never see this message.  
        <!---  <cfinclude template="#myInclude#.cfm">--->	
				
      </cfif> 
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>
