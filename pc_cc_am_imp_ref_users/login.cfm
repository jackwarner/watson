<!---<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="../login.cfm">
	
</cfif>--->
<cfset myTitle = "PC, CC, AM, Imp, Ref">
<html>
<cfinclude template="../ga.js">
<head>
<title><cfoutput>#myTitle#</cfoutput></title>
</head>
<cfset mySource = "watson">
<cfset myTable = "pc_cc_am_imp_ref">
<cfset mySubmitted = "">
<cfif isDefined("URL.submitted")>
  <cfset mySubmitted = #URL.submitted#>
</cfif>

<cfset myPages = 2>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <!---<tr> 
      <td valign="middle"> 
        
          <h4> 	
This program administers the following questionnaires together in a single
administration:
      <blockquote>
       Self-Discrepancy Questionnaire - Personal Constructs<br>
       Self-Discrepancy Questionnaire - Conventional Constructs<br>
       Real-Ideal Discrepancy and Real-Ought Discrepancy Abstract Measures<br>
       Importance of Ideal Self and Importance of Ought Self<br>
       Reference Person(s) for Ought Self<br>
       </blockquote>
The group of questionnaires is administered in 12 partially counterbalanced
orders to each succesive group of 12 participant code numbers. In the
personal construct and conventional construct questionnaires, the orders of
the real, ideal, and ought selves are completely counterbalanced.
            <table>
             <tr>
                 <td colspan="2">
                <br>Description of the Self-Discrepancy Questionnaires:&nbsp;<a href="../Watson_Description.pdf" target="_dinstructions">PDF</a><br>
                 </td>
                </tr>
                
                <tr>
                 <td colspan="2">
                <br>Instructions for Downloading Data &amp; Importing into PASW Statistics 18 (SPSS):&nbsp;<a href="../Instructions.pdf" target="_instructions">PDF</a><br>
                 </td>
                </tr>
                <tr>
                <td><br></td>
                </tr>
                <tr>
                 <td colspan="2">
                 Syntax for Scoring the Questionnaires:&nbsp;<a href="../Syntax_file.sps.zip" target="_syntax">Zip</a>&nbsp;|&nbsp;<a href="../Syntax_file.txt" target="_syntax">Txt</a>
                 </td>
                </tr>
                <tr>
                <td><br></td>
                </tr>
                <tr>
                 <td colspan="2">
                 Instructions for Downloading Syntax File:&nbsp;<a href="../Syntax_Instructions.pdf" target="_syntax_i">PDF</a></a>
                 </td>
                </tr>
            </table>
          </h4>
          RESEARCHERS ARE TO LOGIN ON THIS PAGE,  COMPLETE THE PARTICIPANT CODE # AND GENDER ON THE NEXT PAGE, AND CLICK THE CONTINUE BUTTON ON THAT PAGE. THESE ACTIONS LEAD TO THE FIRST PAGE FOR THE PARTICIPANT.
        </td>
    </tr>--->
    <tr>
    <td>
    RESEARCHERS ARE TO LOGIN ON THIS PAGE,  COMPLETE THE PARTICIPANT CODE # AND GENDER ON THE NEXT PAGE, AND CLICK THE CONTINUE BUTTON ON THAT PAGE. THESE ACTIONS LEAD TO THE FIRST PAGE FOR THE PARTICIPANT.
    </td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
       <cfif mySubmitted NOT EQUAL "true">
          <cfform action="login.cfm?submitted=true">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Email Address:</strong></td>
                  <td width="47%"><cfinput name="Email" type="text" maxlength="255"></td>
                </tr>
                <tr> 
                  <td><strong>PIN (4-digit number): </strong></td>
                  <td><cfinput name="Password" type="password" maxlength="255"></td>
                </tr>
                <tr> 
                  <td><strong>Actions:</strong></td>
                  <td><input type="radio" name="MyChoice" value="survey" checked="true">
                    Administer Questionnaires <br>
                    <input type="radio" value="data" name="MyChoice">
                    Access Data <br>
                    <input type="radio" name="MyChoice" value="chars" >
                    Access Personal Constructs <br></td>
                </tr>
                <tr>
                <td colspan="2">
                <hr>
                </td>
                </tr>
                
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Email_required" value="Please enter your email address."> 
                    <input type="hidden" name="Password_required" value="Please enter your password."> 
                  </td>
                  <td>
				  <input name="submit" type="submit" value="Go"> 
                  </td>
                </tr>
               
              </table>
            </blockquote>
          </cfform>
          
          <font size="1"></font> 
          <cfelse>
          <cfset myEmail = Trim(lCase(#Form.Email#))>
              <CFQUERY name="getID" datasource="#mySource#">
				<!---SELECT Id AS myID FROM users--->
                SELECT Id AS myID FROM users WHERE `Email`='#myEmail#' AND `password`='#Form.Password#' 
		       </CFQUERY>
               
               <cfif #getID.myID# NEQ "">
                <cfif #Form.MyChoice# EQ "survey">
               		<cflocation url="pc_cc_am_imp_ref.cfm?num=0&myto=0&myID=0&myUser=#getID.myID#">
                 <cfelseif #Form.MyChoice# EQ "chars">
                 	<cflocation url="../chars_self_discrepancy.cfm?myUser=#getID.myID#">
                <cfelse>
                	<cflocation url="../allresults_pc_cc_am_imp_ref_users_mysql.cfm?myUser=#getID.myID#">
                </cfif>
               <cfelse>
               Sorry, no user exists with that email address and password.  Please try again.  If you believe you are receiving this message in error, please contact Neill Watson.
               <cfform action="login.cfm?submitted=true">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Email Address:</strong></td>
                  <td width="47%"><cfinput name="Email" type="text" maxlength="255"></td>
                </tr>
                <tr> 
                  <td><strong>Password (case sensitive): </strong></td>
                  <td><cfinput name="Password" type="password" maxlength="255"></td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Email_required" value="Please enter your email address."> 
                    <input type="hidden" name="Password_required" value="Please enter your password."> 
                  </td>
                  <td>
				  <input name="submit" type="submit" value="Login"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
               </cfif>
          
          
          </cfif>
         </td>
	</tr>
  </table>
</center>
</body>
</html>
