<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<CFQUERY DATASOURCE="roi" NAME="getall">
SELECT *
FROM survey
WHERE Id=#URL.Id#
</CFQUERY>
<body>
<p>General Information:</p>
<table width="100%" border="0">
  <tr> 
    <td width="20">Participant Code</td>
    <td width="20">Taken On</td>
    <td width="20">Gender</td>
    <td width="20">Order</td>
    <td width="20">Unique ID</td>
  </tr>
    <cfoutput query="getall"><tr> 

    <td width="20">#Code#</td>
    <td width="20">#TakenOn#</td>
    <td width="20">#Gender#</td>
    <td width="20">N/A</td>
    <td width="20">#Id#</td>
  </tr></cfoutput>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>BDI Ratings </p>
<table align=�center� border=�1�>
  <tr> 
    <td width="20">BDI1</td>
    <td width="20">BDI2</td>
    <td width="20">BDI3</td>
    <td width="20">BDI4</td>
    <td width="20">BDI5</td>
    <td width="20">BDI6</td>
    <td width="20">BDI7</td>
    <td width="20">BDI8</td>
    <td width="20">BDI9</td>
    <td width="20">BDI10</td>
    <td width="20">BDI11</td>
    <td width="20">BDI12</td>
    <td width="20">BDI13</td>
    <td width="20">BDI14</td>
    <td width="20">BDI15</td>
    <td width="20">BDI16</td>
    <td width="20">BDI17</td>
    <td width="20">BDI18</td>
    <td width="20">BDI19</td>
    <td width="20">BDI20</td>
    <td width="20">BDI21</td>
  </tr>
  <CFOUTPUT QUERY="getall"> 
    <tr> 
      <td width="20">#q1#&nbsp;</td>
      <td width="20">#q2#&nbsp;</td>
      <td width="20">#q3#&nbsp;</td>
      <td width="20">#q4#</td>
      <td width="20">#q5#</td>
      <td width="20">#q6#</td>
      <td width="20">#q7#</td>
      <td width="20">#q8#</td>
      <td width="20">#q9#</td>
      <td width="20">#q10#</td>
      <td width="20">#q11#</td>
      <td width="20">#q12#</td>
      <td width="20">#q13#</td>
      <td width="20">#q14#</td>
      <td width="20">#q15#</td>
      <td width="20">#q16#</td>
      <td width="20">#q17#</td>
      <td width="20">#q18#</td>
      <td width="20">#q19#</td>
      <td width="20">#q20#</td>
      <td width="20">#q21#</td>
    </tr>
  </CFOUTPUT> 
</table>

<p>STAI Ratings </p>
<table align=�center� border=�1�>
  <tr> 
    <td width="20">STAI1</td>
    <td width="20">STAI2</td>
    <td width="20">STAI3</td>
    <td width="20">STAI4</td>
    <td width="20">STAI5</td>
    <td width="20">STAI6</td>
    <td width="20">STAI7</td>
    <td width="20">STAI8</td>
    <td width="20">STAI9</td>
    <td width="20">STAI10</td>
    <td width="20">STAI11</td>
    <td width="20">STAI12</td>
    <td width="20">STAI13</td>
    <td width="20">STAI14</td>
    <td width="20">STAI15</td>
    <td width="20">STAI16</td>
    <td width="20">STAI17</td>
    <td width="20">STAI18</td>
    <td width="20">STAI19</td>
    <td width="20">STAI20</td>
  </tr>
  <CFOUTPUT QUERY="getall"> 
    <tr> 
      <td width="20">#stai1#&nbsp;</td>
      <td width="20">#stai2#&nbsp;</td>
      <td width="20">#stai3#&nbsp;</td>
      <td width="20">#stai4#</td>
      <td width="20">#stai5#</td>
      <td width="20">#stai6#</td>
      <td width="20">#stai7#</td>
      <td width="20">#stai8#</td>
      <td width="20">#stai9#</td>
      <td width="20">#stai10#</td>
      <td width="20">#stai11#</td>
      <td width="20">#stai12#</td>
      <td width="20">#stai13#</td>
      <td width="20">#stai14#</td>
      <td width="20">#stai15#</td>
      <td width="20">#stai16#</td>
      <td width="20">#stai17#</td>
      <td width="20">#stai18#</td>
      <td width="20">#stai19#</td>
      <td width="20">#stai20#</td>
    </tr>
  </CFOUTPUT> 
</table>

<p>Personal Constructs Real Self Ratings</p>
<table width="1000" border="1" cellpadding="1" cellspacing="1">
  <tr valign="top"> 
  <cfset vals = ArrayNew(1)>
    <cfoutput>
	<cfloop index="pcrs" from="1" to="36">
	
	<cfset vals[#pcrs#] = "rr" & #pcrs#>
	<td width="20">PCRS#pcrs# <br></td>
	</cfloop>
	</cfoutput>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks">
	Select * from survey
	where Id = #URL.Id#</cfquery>
	<td width="20">#ranks.rr1#</td>
	<td width="20">#ranks.rr2#</td>
	<td width="20">#ranks.rr3#</td>
	<td width="20">#ranks.rr4#</td>
	<td width="20">#ranks.rr5#</td>
	<td width="20">#ranks.rr6#</td>
	<td width="20">#ranks.rr7#</td>
	<td width="20">#ranks.rr8#</td>
	<td width="20">#ranks.rr9#</td>
	<td width="20">#ranks.rr10#</td>
	<td width="20">#ranks.rr11#</td>
	<td width="20">#ranks.rr12#</td>
	
	<td width="20">#ranks.ri1#</td>
	<td width="20">#ranks.ri2#</td>
	<td width="20">#ranks.ri3#</td>
	<td width="20">#ranks.ri4#</td>
	<td width="20">#ranks.ri5#</td>
	<td width="20">#ranks.ri6#</td>
	<td width="20">#ranks.ri7#</td>
	<td width="20">#ranks.ri8#</td>
	<td width="20">#ranks.ri9#</td>
	<td width="20">#ranks.ri10#</td>
	<td width="20">#ranks.ri11#</td>
	<td width="20">#ranks.ri12#</td>

	<td width="20">#ranks.ro1#</td>
	<td width="20">#ranks.ro2#</td>
	<td width="20">#ranks.ro3#</td>
	<td width="20">#ranks.ro4#</td>
	<td width="20">#ranks.ro5#</td>
	<td width="20">#ranks.ro6#</td>
	<td width="20">#ranks.ro7#</td>
	<td width="20">#ranks.ro8#</td>
	<td width="20">#ranks.ro9#</td>
	<td width="20">#ranks.ro10#</td>
	<td width="20">#ranks.ro11#</td>
	<td width="20">#ranks.ro12#</td>

	<!---
	<cfquery datasource="roi" name="ranks2">
	Select * from realrate
	where FK = #URL.Id#</cfquery>
	<td width="20">#ranks2.rr1#</td>
	<td width="20">#ranks2.rr2#</td>
	<td width="20">#ranks2.rr3#</td>
	<td width="20">#ranks2.rr4#</td>
	<td width="20">#ranks2.rr5#</td>
	<td width="20">#ranks2.rr6#</td>
	<td width="20">#ranks2.rr7#</td>
	<td width="20">#ranks2.rr8#</td>
	<td width="20">#ranks2.rr9#</td>
	<td width="20">#ranks2.rr10#</td>
	<td width="20">#ranks2.rr11#</td>
	<td width="20">#ranks2.rr12#</td>
	<td width="20">#ranks2.rr13#</td>
	<td width="20">#ranks2.rr14#</td>
	<td width="20">#ranks2.rr15#</td>
	<td width="20">#ranks2.rr16#</td>
	<td width="20">#ranks2.rr17#</td>
	<td width="20">#ranks2.rr18#</td>
	<td width="20">#ranks2.rr19#</td>
	<td width="20">#ranks2.rr20#</td>
	<td width="20">#ranks2.rr21#</td>
	<td width="20">#ranks2.rr22#</td>
	<td width="20">#ranks2.rr23#</td>
	<td width="20">#ranks2.rr24#</td>
	<td width="20">#ranks2.rr25#</td>
	<td width="20">#ranks2.rr26#</td>
	<td width="20">#ranks2.rr27#</td>
	<td width="20">#ranks2.rr28#</td>  --->
	
	</cfoutput>
  </tr>
</table>

<p>Personal Constructs Ideal Self Ratings</p>
<table width="1000" border="1" cellpadding="1" cellspacing="1">
  <tr valign="top"> 
  <cfset vals = ArrayNew(1)>
    <cfoutput>
	<cfloop index="pcrs" from="1" to="36">
	
	<cfset vals[#pcrs#] = "rr" & #pcrs#>
	<td width="20">PCIS#pcrs# <br></td>
	</cfloop>
	</cfoutput>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks">
	Select * from survey
	where Id = #URL.Id#</cfquery>
	<td width="20">#ranks.ir1#</td>
	<td width="20">#ranks.ir2#</td>
	<td width="20">#ranks.ir3#</td>
	<td width="20">#ranks.ir4#</td>
	<td width="20">#ranks.ir5#</td>
	<td width="20">#ranks.ir6#</td>
	<td width="20">#ranks.ir7#</td>
	<td width="20">#ranks.ir8#</td>
	<td width="20">#ranks.ir9#</td>
	<td width="20">#ranks.ir10#</td>
	<td width="20">#ranks.ir11#</td>
	<td width="20">#ranks.ir12#</td>
	
	<td width="20">#ranks.ii1#</td>
	<td width="20">#ranks.ii2#</td>
	<td width="20">#ranks.ii3#</td>
	<td width="20">#ranks.ii4#</td>
	<td width="20">#ranks.ii5#</td>
	<td width="20">#ranks.ii6#</td>
	<td width="20">#ranks.ii7#</td>
	<td width="20">#ranks.ii8#</td>
	<td width="20">#ranks.ii9#</td>
	<td width="20">#ranks.ii10#</td>
	<td width="20">#ranks.ii11#</td>
	<td width="20">#ranks.ii12#</td>

	<td width="20">#ranks.io1#</td>
	<td width="20">#ranks.io2#</td>
	<td width="20">#ranks.io3#</td>
	<td width="20">#ranks.io4#</td>
	<td width="20">#ranks.io5#</td>
	<td width="20">#ranks.io6#</td>
	<td width="20">#ranks.io7#</td>
	<td width="20">#ranks.io8#</td>
	<td width="20">#ranks.io9#</td>
	<td width="20">#ranks.io10#</td>
	<td width="20">#ranks.io11#</td>
	<td width="20">#ranks.io12#</td>

	</cfoutput>
  </tr>
</table>

<p>Personal Constructs Ought Self Ratings</p>
<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr valign="top"> 
  <cfset vals = ArrayNew(1)>
    <cfoutput>
	<cfloop index="pcrs" from="1" to="36">
	
	<cfset vals[#pcrs#] = "rr" & #pcrs#>
	<td width="20">PCOS#pcrs# <br></td>
	</cfloop>
	</cfoutput>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks">
	Select * from survey
	where Id = #URL.Id#</cfquery>
	<td width="20">#ranks.or1#</td>
	<td width="20">#ranks.or2#</td>
	<td width="20">#ranks.or3#</td>
	<td width="20">#ranks.or4#</td>
	<td width="20">#ranks.or5#</td>
	<td width="20">#ranks.or6#</td>
	<td width="20">#ranks.or7#</td>
	<td width="20">#ranks.or8#</td>
	<td width="20">#ranks.or9#</td>
	<td width="20">#ranks.or10#</td>
	<td width="20">#ranks.or11#</td>
	<td width="20">#ranks.or12#</td>
	
	<td width="20">#ranks.oi1#</td>
	<td width="20">#ranks.oi2#</td>
	<td width="20">#ranks.oi3#</td>
	<td width="20">#ranks.oi4#</td>
	<td width="20">#ranks.oi5#</td>
	<td width="20">#ranks.oi6#</td>
	<td width="20">#ranks.oi7#</td>
	<td width="20">#ranks.oi8#</td>
	<td width="20">#ranks.oi9#</td>
	<td width="20">#ranks.oi10#</td>
	<td width="20">#ranks.oi11#</td>
	<td width="20">#ranks.oi12#</td>

	<td width="20">#ranks.oo1#</td>
	<td width="20">#ranks.oo2#</td>
	<td width="20">#ranks.oo3#</td>
	<td width="20">#ranks.oo4#</td>
	<td width="20">#ranks.oo5#</td>
	<td width="20">#ranks.oo6#</td>
	<td width="20">#ranks.oo7#</td>
	<td width="20">#ranks.oo8#</td>
	<td width="20">#ranks.oo9#</td>
	<td width="20">#ranks.oo10#</td>
	<td width="20">#ranks.oo11#</td>
	<td width="20">#ranks.oo12#</td>

	</cfoutput>
  </tr>
</table>

<p>Conventional Constructs Real Self Ratings</p>
<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr valign="top"> 
  <cfset vals = ArrayNew(1)>
    <cfoutput>
	<cfloop index="pcrs" from="1" to="28">
	
	<cfset vals[#pcrs#] = "rr" & #pcrs#>
	<td width="20">CCRS#pcrs# <br></td>
	</cfloop>
	</cfoutput>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks2">
	Select * from realrate
	where FK = #URL.Id#</cfquery>
	<td width="20">#ranks2.rr1#</td>
	<td width="20">#ranks2.rr2#</td>
	<td width="20">#ranks2.rr3#</td>
	<td width="20">#ranks2.rr4#</td>
	<td width="20">#ranks2.rr5#</td>
	<td width="20">#ranks2.rr6#</td>
	<td width="20">#ranks2.rr7#</td>
	<td width="20">#ranks2.rr8#</td>
	<td width="20">#ranks2.rr9#</td>
	<td width="20">#ranks2.rr10#</td>
	<td width="20">#ranks2.rr11#</td>
	<td width="20">#ranks2.rr12#</td>
	<td width="20">#ranks2.rr13#</td>
	<td width="20">#ranks2.rr14#</td>
	<td width="20">#ranks2.rr15#</td>
	<td width="20">#ranks2.rr16#</td>
	<td width="20">#ranks2.rr17#</td>
	<td width="20">#ranks2.rr18#</td>
	<td width="20">#ranks2.rr19#</td>
	<td width="20">#ranks2.rr20#</td>
	<td width="20">#ranks2.rr21#</td>
	<td width="20">#ranks2.rr22#</td>
	<td width="20">#ranks2.rr23#</td>
	<td width="20">#ranks2.rr24#</td>
	<td width="20">#ranks2.rr25#</td>
	<td width="20">#ranks2.rr26#</td>
	<td width="20">#ranks2.rr27#</td>
	<td width="20">#ranks2.rr28#</td>

	</cfoutput>
  </tr>
</table>


<p>Conventional Constructs Ideal Self Ratings</p>
<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr valign="top"> 
  <cfset vals = ArrayNew(1)>
    <cfoutput>
	<cfloop index="pcrs" from="1" to="28">
	
	<cfset vals[#pcrs#] = "rr" & #pcrs#>
	<td width="20">CCIS#pcrs# <br></td>
	</cfloop>
	</cfoutput>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks2">
	Select * from realrate
	where FK = #URL.Id#</cfquery>
	<td width="20">#ranks2.ir1#</td>
	<td width="20">#ranks2.ir2#</td>
	<td width="20">#ranks2.ir3#</td>
	<td width="20">#ranks2.ir4#</td>
	<td width="20">#ranks2.ir5#</td>
	<td width="20">#ranks2.ir6#</td>
	<td width="20">#ranks2.ir7#</td>
	<td width="20">#ranks2.ir8#</td>
	<td width="20">#ranks2.ir9#</td>
	<td width="20">#ranks2.ir10#</td>
	<td width="20">#ranks2.ir11#</td>
	<td width="20">#ranks2.ir12#</td>
	<td width="20">#ranks2.ir13#</td>
	<td width="20">#ranks2.ir14#</td>
	<td width="20">#ranks2.ir15#</td>
	<td width="20">#ranks2.ir16#</td>
	<td width="20">#ranks2.ir17#</td>
	<td width="20">#ranks2.ir18#</td>
	<td width="20">#ranks2.ir19#</td>
	<td width="20">#ranks2.ir20#</td>
	<td width="20">#ranks2.ir21#</td>
	<td width="20">#ranks2.ir22#</td>
	<td width="20">#ranks2.ir23#</td>
	<td width="20">#ranks2.ir24#</td>
	<td width="20">#ranks2.ir25#</td>
	<td width="20">#ranks2.ir26#</td>
	<td width="20">#ranks2.ir27#</td>
	<td width="20">#ranks2.ir28#</td>

	</cfoutput>
  </tr>
</table>


<p>Conventional Constructs Ought Self Ratings</p>
<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr valign="top"> 
  <cfset vals = ArrayNew(1)>
    <cfoutput>
	<cfloop index="pcrs" from="1" to="28">
	
	<cfset vals[#pcrs#] = "rr" & #pcrs#>
	<td width="20">CCOS#pcrs# <br></td>
	</cfloop>
	</cfoutput>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks2">
	Select * from realrate
	where FK = #URL.Id#</cfquery>
	<td width="20">#ranks2.or1#</td>
	<td width="20">#ranks2.or2#</td>
	<td width="20">#ranks2.or3#</td>
	<td width="20">#ranks2.or4#</td>
	<td width="20">#ranks2.or5#</td>
	<td width="20">#ranks2.or6#</td>
	<td width="20">#ranks2.or7#</td>
	<td width="20">#ranks2.or8#</td>
	<td width="20">#ranks2.or9#</td>
	<td width="20">#ranks2.or10#</td>
	<td width="20">#ranks2.or11#</td>
	<td width="20">#ranks2.or12#</td>
	<td width="20">#ranks2.or13#</td>
	<td width="20">#ranks2.or14#</td>
	<td width="20">#ranks2.or15#</td>
	<td width="20">#ranks2.or16#</td>
	<td width="20">#ranks2.or17#</td>
	<td width="20">#ranks2.or18#</td>
	<td width="20">#ranks2.or19#</td>
	<td width="20">#ranks2.or20#</td>
	<td width="20">#ranks2.or21#</td>
	<td width="20">#ranks2.or22#</td>
	<td width="20">#ranks2.or23#</td>
	<td width="20">#ranks2.or24#</td>
	<td width="20">#ranks2.or25#</td>
	<td width="20">#ranks2.or26#</td>
	<td width="20">#ranks2.or27#</td>
	<td width="20">#ranks2.or28#</td>

	</cfoutput>
  </tr>
</table>


<p>Reference Person for Ought Self</p>
<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr>
  <td>Mother</td>
  <td>Father</td>
  <td>Sibling(s)</td>
  <td>Friends</td>
  <td>Close Friend(s)</td>
  <td>Girlfriend</td>
  <td>Boyfriend</td>
   <td>Spouse</td>
    <td>Peers</td>
	 <td>Society</td>
	 <td>Others</td>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks2">
	Select * from survey
	where Id = #URL.Id#</cfquery>
	<td>#ranks2.mother#</td>
  <td>#ranks2.father#</td>
  <td>#ranks2.sibling#</td>
  <td>#ranks2.friend#</td>
  <td>#ranks2.closefriend#</td>
  <td>#ranks2.Girlfriend#</td>
  <td>#ranks2.Boyfriend#</td>
   <td>#ranks2.Spouse#</td>
    <td>#ranks2.Peers#</td>
	 <td>#ranks2.Society#</td>
	 <td>#ranks2.Others#</td>
	</cfoutput>
  </tr>
</table>


<p>Abstract Measures (Real and Ought)</p>
<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr>
  <td>AMRI</td>
  <td>AMR0</td>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks2">
	Select * from survey
	where Id = #URL.Id#</cfquery>
	<td>#ranks2.ri_abstract#</td>
  <td>#ranks2.ro_abstract#</td>
 
	</cfoutput>
  </tr>
</table>

<p>Importance of Ideal and Ought Self </p>

<table width="100%" border="1" cellpadding="1" cellspacing="1">
  <tr>
  <td>ImpI1</td>
  <td>ImpI2</td>
  <td>ImpI3</td>
  <td>ImpO1</td>
  <td>ImpO2</td>
  <td>ImpO3</td>
  </tr>
  <tr>
  <cfoutput>
  	<!--- rating personal constructs --->
  
	<cfquery datasource="roi" name="ranks2">
	Select * from survey
	where Id = #URL.Id#</cfquery>
	<td>#ranks2.is1#</td>
  <td>#ranks2.is2#</td>
  <td>#ranks2.is3#</td>
  <td>#ranks2.os1#</td>
  <td>#ranks2.os2#</td>
  <td>#ranks2.os3#</td>
	</cfoutput>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
