<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<h2><font color="#0000ff">REGISTRATION FOR AUTHORIZED RESEARCHERS</font></h2>
<h3>To administer individual questionnaires, you must first register your email address. It is the only personal information that will be collected. Your email address allows us to:</h3>
<ol>
  <li>    Determine if you are registering from within William &amp; Mary (you are registering from within W&amp;M   '@wm.edu' is found in your email address)</li>
  <li>Send the results of questionnaire responses to an active email account to which you have access</li>
</ol>
<h3>Your email address will be used for no other purpose than those stated here.</h3>
<div align="center">
<cfform name="form1" id="form1" method="post" action="register.cfm">
  <table width="200" border="0">
    <caption>
      Researcher Registration
    </caption>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td valign="top">Your Email Address <br /></td>
      <td colspan="2" valign="top">
        <label>
          <cfinput type="text" name="email" id="email" accesskey="E" tabindex="0" maxlength="255">
          <font size="2">(please use '@wm.edu' email if you are a W&amp;M researcher)</font>          </label>           </td>
    </tr>
    <tr>
      <td valign="top">Authorization Code<br />
        <font size="2">(not required)</font></td>
      <td colspan="2" valign="top"><cfinput type="text" name="authorization" id="authorization" accesskey="A" tabindex="1" maxlength="15">
        <font size="2">If you were given an authorization code, enter it here.  It will allow you to begin administering questionnaires immediately after your receive the confirmation email.</font></td>
    </tr>
    <tr>
      <td valign="top">&nbsp;</td>
      <td colspan="2" valign="top"><label>
        <input type="submit" name="savedSubmit" value="Submit">
      </label></td>
    </tr> 
  </table>  
  </cfform>
</div>
<p align="center">&nbsp;</p>
</body>
</html>
