<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>
<cfif isDefined("form.savedSubmit")>
	<cfif ReFind(".+@.+..+", Form.email)>
    	<cfset theEmail = UCase(Trim(Form.email))>
        <cfset isFound = Find('@WM.EDU', theEmail)>
        <cfset isWM = false>
        <cfif isFound NEQ 0>
        	<cfset isWM = true>
        </cfif>
        
        <cfset isAuth = false>
        <cfif trim(Form.authorization) EQ "DHuxQ9" AND isWM>
          	<cfset isAuth = true>
        <cfelseif trim(Form.authorization) EQ "DHuxQ10" AND  NOT isWM>
        	 <cfset isAuth = true>
        <cfelse>
        	 <cfset isAuth = false>
        </cfif>
        
        <cfset emailSubject = "Thank You for Requesting Access to the Research Site of Neill P. Watson">
        <cfset emailBody 	= "Dear Interested Researcher,
		
You have successfully requested access to the research site of Neill P. Watson.  Neill will review your request and grant or deny access as appropriate.

If you do receive an email within the next 72 hours, you may contact Neill directly at npwats@wm.edu.

If you did not request access to the Research Site of Neill P. Watson, you may safely ignore this email.">
        <cfif isAuth>
        	<cfset emailSubject = "You've Been Granted Access to the Research Site of Neill P. Watson">
            <cfset emailBody 	= "Dear Interested Researcher,
		
You have been granted access to the research site of Neill P. Watson.  Click the link below to activate your sign-in information:

http://www.wm.edu/research/watson?blahblahblah

If you did not request access to the Research Site of Neill P. Watson, you may safely ignore this email.">
        
        </cfif>
        
        <cfmail to="#Form.email#"
            from="npwats@wm.edu"
            subject="#emailSubject#"
            type="text">
            #emailBody#
        </cfmail>
        Thank you.  You should receive an email at '<cfoutput>#Form.email#</cfoutput>' within the next ten (10) minutes with instruction on what to do next.
    <cfelse>
    	The email address you provided does not seem to be valid.  Please <a href="index.cfm"> go back</a> and provide a valid email address.
    </cfif>
<cfelse>
	You have arrived at this page improperly.  Error 3578.
</cfif>
<body>
</body>
</html>
