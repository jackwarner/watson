 <cfif NOT IsNumeric(#Form.Age#)>
          	<a href="javascript:history.go(-1)">Please enter an age between 0 and 110 (you entered '<cfoutput>#Form.Age#</cfoutput>'). Click to go back.</a> 
           <cfelseif (#Form.Age#/1) GTE 110>
            <a href="javascript:history.go(-1)">Please enter an age between 0 and 110 (you entered '<cfoutput>#Form.Age#</cfoutput>'). Click to go back.</a> 
            <cfelse>
<CFSET yourDate = CreateDate(Mid(form.BirthDate,7,4), Mid(form.BirthDate,1,2), Mid(form.BirthDate,4,2))>
<cfset todayFormatDate = #DateFormat(yourDate, "yyyy-mm-dd ")#>
<cfset todayFormatTime = #TimeFormat(yourDate, "HH:mm:ss")#>
<cfset finalDate = #todayFormatDate# & #todayFormatTime#>

<cfquery datasource="#mySource#">
	UPDATE #myTable#
	SET BirthDate='#finalDate#', Race = #Form.Race#, RaceOther='#Form.RaceOther#', Age='#Form.Age#'
	WHERE ID = #myID#
</cfquery>

<cfform action="frame.cfm?num=#myIndex+1#&myTo=end&myId=#myID#" method="post" onsubmit="return checkRadios()">
  <div align="center"> </div>
  <TABLE WIDTH=850 BORDER=0 align="center" CELLPADDING=0 CELLSPACING=0>
    <TR>
      <TD height="30" colspan="2"><p><strong><font size="4">Please fill in each item with the requested information.&nbsp; Please remember all your responses are confidential and will not be identified as coming from you.</font></strong></p>
        <hr></TD>
    </TR>
	<script language = "Javascript">
	function checkRadios() {
			 var el = document.forms[0].elements;
			 for(var i = 0 ; i < el.length ; ++i) {
			  if(el[i].type == "radio") {
			   var radiogroup = el[el[i].name]; // get the whole set of radio buttons.
			   var itemchecked = false;
			   for(var j = 0 ; j < radiogroup.length ; ++j) {
				if(radiogroup[j].checked) {
				 itemchecked = true;
				 break;
				}
			   }
			   if(!itemchecked) { 
				alert("Please enter a response for all questions.");
				if(el[i].focus)
				 el[i].focus();
				return false;
			   }
			  }
			 }
			 return true;
		} 
	</script>
    <TR align="center">
      <TD width="40%" bgcolor="#FFFFCC"><div align="right">Have you been in counseling or psychotherapy since the first time you participated in this study?</div></TD>
      <TD width="60%" bgcolor="#FFFFCC"><div align="left">
        <input name="PastCounseling" type="radio" value="1" />
        Yes<br />
  <input name="PastCounseling" type="radio" value="0" />
  <input type="hidden" name="PastCounseling_required" value="Please enter values for all fields." />
      No</div></TD>
    </TR>
    <TR align="center">
      <TD width="40%" valign="middle" bgcolor="#FFFFFF">
        <div align="right">Are you currently in counseling or psychotherapy?&nbsp;</div>      </TD>
      <TD width="60%" valign="middle" bgcolor="#FFFFFF"><div align="left">
     
          <input name="Counseling" type="radio" value="1" />
          Yes<br />
          <input name="Counseling" type="radio" value="0" />
		  <input type="hidden" name="Counseling_required" value="Please enter values for all fields."> 
        No
        </div></TD>
    </TR>
    <TR align="center">
      <TD valign="middle" bgcolor="FFFFCC"><div align="right">Are you currently taking medication for depression?&nbsp;</div></TD>
      <TD valign="middle" bgcolor="FFFFCC"><div align="left">
        <input name="Depression" type="radio" value="1" />
Yes<br />
<input name="Depression" type="radio" value="0" />
<input type="hidden" name="Depression_required" value="Please enter values for all fields."> 
No</div></TD>
    </TR>
    <TR align="center">
      <TD valign="middle" bgcolor="#FFFFFF"><div align="right">Are you currently taking medication for anxiety?</div></TD>
      <TD valign="middle" bgcolor="#FFFFFF"><div align="left">
       
          <input name="Anxiety" type="radio" value="1" />
Yes<br />
<input name="Anxiety" type="radio" value="0" />
<input type="hidden" name="Anxiety_required" value="Please enter values for all fields."> 
No

      </div></TD>
    </TR>
    <TR align="center"> 
      <TD colspan="2"> <input type="submit" name="Submit" value="Complete the Questionnaire">      </TD>
    </TR>
  </TABLE>
</cfform>
</cfif>