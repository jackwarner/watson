<cfform action="frame.cfm?num=#myIndex+1#&myTo=2&myId=#myID#" method="post" name="frmSample" onSubmit="return ValidateForm()">
  <div align="center"> </div>
  <TABLE WIDTH=850 BORDER=0 align="center" CELLPADDING=0 CELLSPACING=0>
    <TR>
      <TD height="30" colspan="2"><p><strong><font size="4">Please fill in each item with the requested information.&nbsp; Please remember all your responses are confidential and will not be identified as coming from you.</font></strong></p>
        <hr></TD>
    </TR>
	<!---
    <TR align="center">
      <TD width="35%" bgcolor="#FFFFCC">
      <div align="right">Your Age: &nbsp;</div>      </TD>
      <TD width="65%" bgcolor="#FFFFCC"><div align="left"><input type="text" name="Age" maxlength="3" tabindex="1"/></div></TD>
    </TR>
	--->
	<script language = "Javascript">
		/**
		 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
		 */
		// Declaring valid date character, minimum year and maximum year
		function checkRadios() {
			 var el = document.forms[0].elements;
			 for(var i = 0 ; i < el.length ; ++i) {
			  if(el[i].type == "radio") {
			   var radiogroup = el[el[i].name]; // get the whole set of radio buttons.
			   var itemchecked = false;
			   for(var j = 0 ; j < radiogroup.length ; ++j) {
				if(radiogroup[j].checked) {
				 itemchecked = true;
				 if (itemchecked && radiogroup[j].value == 6) {
				 	if (document.frmSample.RaceOther.value == '') {
						alert("Please enter a response in the 'Other' text field.");
						document.frmSample.RaceOther.focus();
						return false;
					}
				 }
				 break;
				}
			   }
			   if(!itemchecked) { 
				alert("Please enter a response for your Race/Ethnicity.");
				if(el[i].focus)
				 el[i].focus();
				return false;
			   }
			  }
			 }
			 return true;
		} 
//  End -->

		
		var dtCh= "/";
		var minYear=1900;
		var maxYear=2000;
		
		function isInteger(s){
			var i;
			for (i = 0; i < s.length; i++){   
				// Check that current character is number.
				var c = s.charAt(i);
				if (((c < "0") || (c > "9"))) return false;
			}
			// All characters are numbers.
			return true;
		}
		
		function stripCharsInBag(s, bag){
			var i;
			var returnString = "";
			// Search through string's characters one by one.
			// If character is not in bag, append to returnString.
			for (i = 0; i < s.length; i++){   
				var c = s.charAt(i);
				if (bag.indexOf(c) == -1) returnString += c;
			}
			return returnString;
		}
		
		function daysInFebruary (year){
			// February has 29 days in any year evenly divisible by four,
			// EXCEPT for centurial years which are not also divisible by 400.
			return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
		}
		function DaysArray(n) {
			for (var i = 1; i <= n; i++) {
				this[i] = 31
				if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
				if (i==2) {this[i] = 29}
		   } 
		   return this
		}
		
		function isDate(dtStr){
			var daysInMonth = DaysArray(12)
			var pos1=dtStr.indexOf(dtCh)
			var pos2=dtStr.indexOf(dtCh,pos1+1)
			var strMonth=dtStr.substring(0,pos1)
			var strDay=dtStr.substring(pos1+1,pos2)
			var strYear=dtStr.substring(pos2+1)
			strYr=strYear
			if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
			if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
			for (var i = 1; i <= 3; i++) {
				if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
			}
			month=parseInt(strMonth)
			day=parseInt(strDay)
			year=parseInt(strYr)
			if (pos1==-1 || pos2==-1){
				alert("The date format should be : mm/dd/yyyy")
				return false
			}
			if (strMonth.length<1 || month<1 || month>12){
				alert("Please enter a valid month")
				return false
			}
			if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
				alert("Please enter a valid day")
				return false
			}
			if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
				alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
				return false
			}
			if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
				alert("Please enter a valid date")
				return false
			}
		return true
		}
		
		function ValidateForm(){
			var dt=document.frmSample.BirthDate
			if (isDate(dt.value)==false){
				dt.focus()
				return false
			} 
			else {
				return checkRadios()
			}
			return true
		 }
		
		function isChecked() {
			
		}
</script>
 <TR align="center">
      <TD bgcolor="#FFFFCC"><div align="right">Age:</div></TD>
      <TD bgcolor="#FFFFCC"><div align="left"><input name="Age" type="text" size="5" maxlength="3" /></div></TD>
    </TR>
    <TR align="center">
      <TD bgcolor="#FFFFCC"><div align="right">Date of Birth:&nbsp; </div></TD>
      <TD bgcolor="#FFFFCC"><div align="left">
	  <!---
        <select name="Month" tabindex="2">
          <option value="0" selected="selected">Month</option>
          <option value="1">January</option>
          <option value="2">February</option>
          <option value="3">March</option>
          <option value="4">April</option>
          <option value="5">May</option>
          <option value="6">June</option>
          <option value="7">July</option>
          <option value="8">August</option>
          <option value="9">September</option>
          <option value="10">October</option>
          <option value="11">November</option>
          <option value="12">December</option>
        </select>
        <input name="Day" type="text" value="DD" size="3" maxlength="2" onClick="this.value=''" />
        <input name="Year" type="text" value="YYYY" size="5" maxlength="4" onClick="this.value=''" />
		--->
		<input name="BirthDate" type="text" size="11" maxlength="10" /><font color="gray">(mm/dd/yyyy)</font>
      </div></TD>
    </TR>
    <TR align="center">
      <TD valign="top" bgcolor="#FFFFFF"><div align="right">Race/Ethnicity:&nbsp;</div></TD>
      <TD bgcolor="#FFFFFF"><div align="left">
        <p>
          <input name="Race" type="radio" value="1" />
          White/European American<br />
          <input name="Race" type="radio" value="2" />
African American/Black<br />
         <input name="Race" type="radio" value="3" />
 Hispanic/Latino<br />
          <input name="Race" type="radio" value="4" />
Asian American<br />
          <input name="Race" type="radio" value="5" />
Native American/American Indian<br />
          <input name="Race" type="radio" value="6" />
Other (Please Specify) 
<input name="RaceOther" type="text" size="25" maxlength="255" />
        </p>
      </div></TD>
    </TR>
    <TR align="center"> 
      <TD colspan="2"> <input type="submit" name="Submit" value="Continue to the Final Page">      </TD>
    </TR>
  </TABLE>
</cfform>
