
<html>
<head>
<title>Survey Application, Neill Watson</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="login.cfm">
	
</cfif>
<body link="#0000FF" vlink="#0000FF" alink="#0000FF">
<table width="100%" border="0">
  <tr valign="top"> 
    <td colspan="2"> <h2><font color="#0000FF">WELCOME TO THE RESEARCH SITE OF<br>
        NEILL WATSON, PH.D.<br>
        THE COLLEGE OF WILLIAM &amp; MARY</font><br>
        <font color="#0000FF"><em>for Authorized Researchers</em></font></h2></td>
  </tr>
  <tr> 
    <td colspan="2"><h4><font color="#FF0000"><br>
        <script language="JavaScript1.2">
<!-- Original:  Craig Lumley -->
<!-- Web Site:  http://www.craiglumley.co.uk -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
<!-- Begin
function fullScreen(theURL) {
window.open(theURL, '', 'fullscreen=yes, scrollbars=yes');
}
//  End -->

var bestwidth = 1024;
var bestheight = 768;
if (screen.width < bestwidth || screen.height < bestheight) {
msg = "<hr>The surveys on this site are intended to be administered on systems with a screen resolution of at least"
+ bestwidth + "x" + bestheight + ". Your"
+ " screen resolution is " + screen.width + "x"
+ screen.height + ".  Please change your screen resolution "
+ "if possible.  You will not see this message if you screen is set to the proper resolution.";
document.write(msg);
}
//  End -->
</script>
        </font></h4></td>
  </tr>
  <tr> 
    <td colspan="2">
      <hr></td>
  </tr>
  <tr>
    <td><div align="center"><img src="survey.jpg" width="30" height="38"></div></td>
    <td><h3><a href="javascript:fullScreen('metaprogram.cfm?num=0&myID=0&myTo=0&mySkip=x');">Study 
        of Self-Perception and Emotions</a></h3></td>
  </tr>
  <tr> 
    <td colspan="2"><hr></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><font size="1">Copyright 2004<br>
      Neill Watson, Ph.D.<br>
      College of William &amp; Mary<br>
      <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font></td>
  </tr>
</table>

</body>
</html>
