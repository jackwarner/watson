
<center>
  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td width="50%" colspan="8"> <h3>&nbsp; </h3></td>
    </tr>
    <tr align="left" valign="top"> 
      <td height="43" colspan="8"> 
        <blockquote> 
          <h3>First, you are asked to list personality characteristics that describe 
            three different ways of viewing yourself.</h3>
          <ul>
            <li>
              <h3>REAL SELF - Yourself as YOU see yourself in your own eyes.</h3>
            </li>
            <li>
              <h3>IDEAL SELF - Yourself as YOU would like to be in your own eyes.</h3>
            </li>
            <li>
              <h3>OUGHT/SHOULD SELF - Yourself as OTHERS think you ought or should 
                be.</h3>
            </li>
          </ul>
          <h3>For each characteristic, use one word or a very short phrase. Just 
            type the characteristic; do not type a sentence like &quot;I am ...&quot;</h3>
          <h3>You are asked for your own view. There are no right or wrong answers.</h3>
        </blockquote></td>
    </tr>
    <tr valign="top"> 
	<!--- 	REAL 	= 2.cfm
		IDEAL 	= 3.cfm
		OUGHT	= 4.cfm --->
		<cfquery name="Getroi" datasource="roi"> 
 			SELECT numpick FROM survey WHERE Id=#URL.myId#
		</cfquery>
<cfif Getroi.numpick EQ 1 OR Getroi.numpick EQ 2>
	<!--- real --->
<cfset going="2">
</cfif>
<cfif Getroi.numpick EQ 3 OR Getroi.numpick EQ 4>
<!--- ought --->
<cfset going="4">
</cfif>
<cfif Getroi.numpick EQ 5 OR Getroi.numpick EQ 6>
<!--- ideal --->
<cfset going="3">
</cfif>

<cfform action="frame_.cfm?num=#myIndex+1#&myTo=#going#&myID=#myID#">

      <td height="43">&nbsp;</td>
      <td colspan="7"> <div align="left"> <br> <input type="submit" name="Submit" value="Continue"> 
      </td>
</cfform>
    </tr>
  </table>
</center>

