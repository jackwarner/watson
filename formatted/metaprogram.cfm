<cfif IsDefined("cookie.npwatscookie") >

<cfelse>
	<cflocation url="login.cfm">
	
</cfif>
<html>
<head>
<title>Random Survey System</title>
</head>
<cfset myTable = "survey">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 2>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="20"> 
        <h5> 
         <!---  <cfif #myIndex# EQUAL 0><cfelse>
	  Page <cfoutput>#myIndex# of #myPages#</cfoutput>
	  
	  </cfif>---> </h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        <cfif myIndex EQUAL 0>
          <h3>Study of Self-Perception and Emotions 
            <cfelse>
          </h3>
          </cfif></td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
        <cfif myIndex EQUAL 0>
          <cfform action="metaprogram.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="0">
                    Male 
                    <input type="radio" value="1" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1"></font> 
        </cfif> </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3 OR NOT LSIsNumeric(#Form.Code#) OR #Form.Code# LTE 0 > 
			<a href="javascript:history.go(-1)">Please enter a 3 digit numeric researcher 
			code greater than 000. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
			  <cftransaction>
				<cfquery name="myQ" datasource="roi">
				<cfset myBigOrder = (#Form.Code#) MOD 12>
					<!--- 
					BDI First: 2, 3, 4, 6, 9, 11
					STAI first: 1, 5, 7, 8, 10, 0
					Types in | Calculates To | 
					001        1
					002        2
					003        3
					004        4
					005        5
					006        6
					007        7
					008        8
					009        9
					010        10
					011        11
					012        0
						1> 10 = O/I/R/  (4) STAI/BDI
						2> 04 = O/I/R/	(4) BDI/STAI
						3> 02 = R/O/I/	(2) BDI/STAI
						4> 05 = I/O/R/	(5) BDI/STAI
						5> 08 = R/O/I/	(2) STAI/BDI
						6> 06 = I/R/O/	(6) BDI/STAI
						7> 09 = O/R/I/	(3) STAI/BDI
						8> 07 = R/I/O/	(1) STAI/BDI
						9> 01 = R/I/O/  (1) BDI/STAI
						10> 11 = I/O/R/	(5) STAI/BDI
						11> 03 = O/R/I/ (3) BDI/STAI
						12 a.k.a 0> 12 = I/R/O/ (6) STAI/BDI --->
					<cfif myBigOrder EQ 1 or myBigOrder EQ 2>
						<cfset insertPick = 4>
					</cfif>
					<cfif myBigOrder EQ 3 or myBigOrder EQ 5>
						<cfset insertPick = 2>
					</cfif>
					<cfif myBigOrder EQ 4 or myBigOrder EQ 10>
						<cfset insertPick = 5>
					</cfif>
					<cfif myBigOrder EQ 6 or myBigOrder EQ 0>
						<cfset insertPick = 6>
					</cfif>
					<cfif myBigOrder EQ 7 or myBigOrder EQ 11>
						<cfset insertPick = 3>
					</cfif>
					<cfif myBigOrder EQ 8 or myBigOrder EQ 9>
						<cfset insertPick = 1>
					</cfif>
				INSERT INTO #myTable#(Code, Gender, BigOrder, numpick) VALUES ('#Form.Code#', '#Form.Gender#', #myBigOrder#, '#insertPick#') 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="roi">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			
			<cfquery datasource="roi">
				INSERT INTO realrate (FK) VALUES (#myID#)
			</cfquery>
            <!--- <cfoutput>#getMaxID.myID#!!!</cfoutput>
			<cfinclude template="#myIndex#.cfm"> --->
            <h3>Thank you for volunteering to participate in this study. You are 
              asked first to complete nine questionnaires on the computer. Four 
              of them are very brief. After that, you are asked to complete the 
              two paper-and-pencil questionnaires in the envelope that you were 
              given. <br>
              <br>
              Click <a href="RIOPC/frame_.cfm?num=2&myID=<cfoutput>#myID#</cfoutput>&myTo=1">here</a> 
              to begin the questionnaires. &nbsp; </h3>
          </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>   
	  	You should never see this message.  
        <!---  <cfinclude template="#myInclude#.cfm">--->	
				
      </cfif> 
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>
