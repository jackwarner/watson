<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Thank You.</title>
</head>

<body>
<cfset myTable = "riopc">
<cfset mySource = "watson">
<cfset myId = #URL.myId#>
<cfquery name="getCode" datasource="#mySource#">
select Code from #myTable# where Id='#myId#'
</cfquery>

<cfset myFrom = "">
<cfif isDefined("Form.myFrom")>
	<cfset myFrom = #Form.myFrom#>
<cfelseif isDefined("URL.myFrom")>
	<cfset myFrom = #URL.myFrom#>
</cfif>

<cfset participant = getCode.Code MOD 24>

<cfif participant EQ 0>
	<cfset participant = 24>
</cfif>

<cfset whichBDI = "">

<cfif participant EQ 2 OR participant EQ 6 OR participant EQ 10 OR participant EQ 16 OR participant EQ 21  OR participant EQ 22>
	<cfset whichBDI = 1>
<cfelseif participant EQ 1 OR participant EQ 9 OR participant EQ 11 OR participant EQ 13 OR participant EQ 18  OR participant EQ 20>
	<cfset whichBDI = 2>
<cfelseif participant EQ 3 OR participant EQ 4 OR participant EQ 12 OR participant EQ 14 OR participant EQ 15  OR participant EQ 19>
	<cfset whichBDI = 3>
<cfelseif participant EQ 5 OR participant EQ 7 OR participant EQ 8 OR participant EQ 17 OR participant EQ 23  OR participant EQ 24>
	<cfset whichBDI = 4>
</cfif>

<cfoutput>

<cfset abstractricURL = "AbstractRIc/frame.cfm">
<cfset abstractrocURL = "AbstractROc/frame.cfm">
<cfset baiURL = "BAI/bai.cfm">
<cfset bdiURL = "BDI/frame.cfm">
<cfset cesdURL = "CESD/cesd.cfm">
<cfset dassURL = "DASS/dass.cfm">
<cfset idealURL = "Ideal/frame.cfm">
<cfset imURL = "IM/im.cfm">
<cfset oughtURL = "Ought/frame.cfm">
<cfset refpersonURL = "RefPerson/frame.cfm">
<cfset rioccURL = "RIOCC/frame.cfm">
<cfset riopcURL = "RIOPC/frame_.cfm">
<cfset staiURL = "STAI/frame.cfm">
<cfset deqURL = "deq">

<cfset myNext = "DEFAULT">
	<cfif myFrom EQ deqURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = bdiURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = staiURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = bdiURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = baiURL>
		</cfif>
	<cfelseif myFrom EQ bdiURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = baiURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = "[END]">
		<cfelseif whichBDI EQ 3>
			<cfset myNext = staiURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = "[END]">
		</cfif>
	<cfelseif myFrom EQ baiURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = dassURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = bdiURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = "[END]">
		<cfelseif whichBDI EQ 4>
			<cfset myNext = cesdURL>
		</cfif>
	<cfelseif myFrom EQ dassURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = cesdURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = baiURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = cesdURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = staiURL>
		</cfif>
	<cfelseif myFrom EQ cesdURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = staiURL>
		<cfelseif whichBDI EQ 2>
			<cfset myNext = dassURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = baiURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = dassURL>
		</cfif>
	<cfelseif myFrom EQ staiURL>
		<cfif whichBDI EQ 1>
			<cfset myNext = "[END]">
		<cfelseif whichBDI EQ 2>
			<cfset myNext = cesdURL>
		<cfelseif whichBDI EQ 3>
			<cfset myNext = dassURL>
		<cfelseif whichBDI EQ 4>
			<cfset myNext = bdiURL>
		</cfif>
	</cfif>

	
<!--- determine other blocks of orders --->
<cfif false>
Participant: #participant#.
<BR>BDI Order: #whichBDI#.<BR>
	
	
	BDI Order: #whichBDI#.<br>
	From: #myFrom#.<br>
	Next: #myNext#?num=2&myID=#myID#&myTo=1&mySkip=x.
</cfif>
<cfif true>
	<cfif myNext EQ "[END]">
			<cfcookie name="npwatscookie" value="true" expires="NOW">
			<cfcookie name="whichQuestionnaire" value="0" expires="NOW">
			<br><br><br><br><br><br><br>
			<table width="585" align="center"><tr><td>
				<h3>Thank you for completing the questionnaires. Please let the 
				researcher know that you have finished.</h3> <br>
				<!--- Or, go 
				<a href="http://cfdev.wm.edu/watson/results.cfm?Id=<cfoutput>#URL.myID#</cfoutput>">here</a> 
				to see the results of your questionnaire. For testing purposes only. --->
				<center>
				</strong><BR><input type="button" value="Close the Questionnaire" name="close" onClick=	"window.close()">
				</center>
				</td></tr></table>
				
	<cfelseif myNext EQ bdiURL>
		<cflocation url="#myNext#?num=1&myID=#myID#&myTo=2&mySkip=x" addtoken="no">
	<cfelse>
		<cflocation url="#myNext#?num=1&myID=#myID#&myTo=1&mySkip=x" addtoken="no">
	</cfif>
</cfif>
</cfoutput>
</body>
</html>
