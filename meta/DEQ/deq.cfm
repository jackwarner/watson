<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<cfset databaseLive = false>
<cfset mySource = "watson">
<cfset myTable = "deq">
<cfset myID = -1>
<cfif isDefined("URL.myId") >
	<cfset myID = #URL.myId#> 
</cfif>
<cfinclude template="../debug.cfm">
<cfset databaseLive = true>



<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I set my personal goals and standards as high as possible.">
	<cfset myQuestions[2] = "Without support from others who are close to me, I would be helpless.">
	<cfset myQuestions[3] = "I tend to be satisfied with my current plans and goals, rather than striving for higher goals.">
    <cfset myQuestions[4] = "Sometimes I feel very big, and other times I feel very small.">
	<cfset myQuestions[5] = "When I am closely involved with someone, I never feel jealous.">
	<cfset myQuestions[6] = "I urgently need things that only other people can provide.">
	<cfset myQuestions[7] = "I often find that I don't live up to my own standards or ideals.">
	<cfset myQuestions[8] = "I feel I am always making full use of my potential abilities.">
	<cfset myQuestions[9] = "The lack of permanence in human relationships doesn't bother me.">
	<cfset myQuestions[10]= "If I fail to live up to expectations, I feel unworthy.">
	<cfset myQuestions[11]= "Many times I feel helpless.">
	<cfset myQuestions[12]= "I seldom worry about being criticized for things I have said or done.">
	<cfset myQuestions[13]= "There is a considerable difference between how I am now and how I would like to be.">
	<cfset myQuestions[14]= "I enjoy sharp competition with others.">
	<cfset myQuestions[15]= "I feel I have many responsibilities that I must meet.">
	<cfset myQuestions[16]= "There are times when I feel ""empty"" inside.">
	<cfset myQuestions[17]= "I tend not to be satisfied with what I have.">
	<cfset myQuestions[18]= "I don't care whether or not I live up to what other people expect of me.">
	<cfset myQuestions[19]= "I become frightened when I feel alone.">
	<cfset myQuestions[20]= "I would feel like I'd be losing an important part of myself if I lost a very close friend.">
	<cfset myQuestions[21]= "People will accept me no matter how many mistakes I have made.">
	<cfset myQuestions[22]= "I have difficulty breaking off a relationship that is making me unhappy.">
	<cfset myQuestions[23]= "I often think about the danger of losing someone who is close to me.">
	<cfset myQuestions[24]= "Other people have high expectations of me.">
	<cfset myQuestions[25]= "When I am with others, I tend to devalue or ""undersell"" myself.">
	<cfset myQuestions[26]= "I am not very concerned with how other people respond to me.">
	<cfset myQuestions[27]= "No matter how close a relationship between two people is, there is always a large amount of uncertainty and conflict.">
	<cfset myQuestions[28]= "I am very sensitive to others for signs of rejection.">
	<cfset myQuestions[29]= "It's important for my family that I succeed.">
	<cfset myQuestions[30]= "Often, I feel I have disappointed others.">
	<cfset myQuestions[31]= "If someone makes me angry, I let him (her) know how I feel.">
	<cfset myQuestions[32]= "I constantly try, and very often go out of my way, to please or help people I am close to.">
	<cfset myQuestions[33]= "I have many inner resources (abilities, strengths).">
	<cfset myQuestions[34]= "I find it very difficult to say ""No"" to the requests of friends.">
	<cfset myQuestions[35]= "I never really feel secure in a close relationship.">
	<cfset myQuestions[36]= "The way I feel about myself frequently varies:  there are times when I feel extremely good about myself and other times when I see only the bad in me and feel like a total failure.">
	<cfset myQuestions[37]= "Often, I feel threatened by change.">
	<cfset myQuestions[38]= "Even if the person who is closest to me were to
leave, I could still ""go it alone.""">
	<cfset myQuestions[39]= "One must continually work to gain love from another person:  that is, love has to be earned.">
	<cfset myQuestions[40]= "I am very sensitive to the effects my words or actions have on the feelings of other people.">
	<cfset myQuestions[41]= "I often blame myself for things I have done or said to someone.">
	<cfset myQuestions[42]= "I am a very independent person.">
	
	<cfset myQuestions[43]= "I often feel guilty.">
	<cfset myQuestions[44]= "I think of myself as a very complex person, one who has ""many sides.""">
	<cfset myQuestions[45]= "I worry a lot about offending or hurting someone who is close to me.">
	<cfset myQuestions[46]= "Anger frightens me.">
	<cfset myQuestions[47]= "It is not ""who you are,"" but ""what you have accomplished"" that counts.">
	<cfset myQuestions[48]= "I feel good about myself whether I succeed or fail.">
	<cfset myQuestions[49]= "I can easily put my own feelings and problems aside, and devote my complete attention to the feelings and problems of someone else.">
	<cfset myQuestions[50]= "If someone I cared about became angry with me, I would feel threatened that he (she) might leave me.">
	<cfset myQuestions[51]= "I feel comfortable when I am given important responsibilities.">
	<cfset myQuestions[52]= "After a fight with a friend, I must make amends as soon as possible.">
	<cfset myQuestions[53]= "I have a difficult time accepting weaknesses in myself.">
	<cfset myQuestions[54]= "It is more important that I enjoy my work than it is for me to have my work approved.">
	<cfset myQuestions[55]= "After an argument, I feel very lonely.">
	<cfset myQuestions[56]= "In my relationships with others, I am very concerned about what they can give to me.">
	<cfset myQuestions[57]= "I rarely think about my family.">
	<cfset myQuestions[58]= "Very frequently, my feelings toward someone close to me vary: there are times when I feel completely angry and other times when I feel all-loving towards that person.">
	<cfset myQuestions[59]= "What I do and say has a very strong impact on those around me.">
	<cfset myQuestions[60]= "I sometimes feel that I am ""special.""">
	<cfset myQuestions[61]= "I grew up in an extremely close family.">
	<cfset myQuestions[62]= "I am very satisfied with myself and my accomplishments.">
	<cfset myQuestions[63]= "I want many things from someone I am close to.">
	<cfset myQuestions[64]= "I tend to be very critical of myself.">
	<cfset myQuestions[65]= "Being alone doesn't bother me at all.">
	<cfset myQuestions[66]= "I very frequently compare myself to standards or goals.">
	

<cfset page = 1>
<cfset qPerPage = 11>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<!---
For compatibility with older surveys, set myIndex and myPages --->
<cfset myIndex = page + 1>
<cfset myPages = totalPages + 1>
<!--- end compability measures --->
<cfset title = "DEQ">
<cfset instructions = "Listed below are a number of statements concerning personal characteristics and traits.  Read each item and decide whether you agree or disagree and to what extent.  If you <u>strongly agree</u>, select 7; if you <u>strongly disagree</u>, select 1. The midpoint, if you are neutral or undecided, is 4.">
<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>

<cfif isDefined("URL.myId") and not isDefined("Form.Id")>
	   		<cfquery name="getcode" datasource="#mySource#">
				  	select Id, Code, numpick, Gender from riopc where Id=#URL.myId#
	    	</cfquery>
			<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  	<cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  	<cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  	<cfset finalDate = #todayFormatDate# & #todayFormatTime#>
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable# (Id, Code, Gender, datecreated) VALUES ('#URL.myId#', '#getcode.Code#', '#getcode.Gender#', '#finalDate#')  
				</cfquery>
				
			  </cftransaction>
			</cflock> 
			<cfset myID = #URL.myID#> 
	
</cfif>

<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>

<cfif NOT isDefined("URL.myId")>
<center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">

    <tr valign="top"> 
      <td height="20"> 
        <h5> 
          Introduction for Researcher
	    </h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        
          <h3>DEQ
            </td>
    </tr>
    <tr valign="top"> 
      <td valign="top"> 
	 <cfform action="deq.cfm" method="post">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant 3-digit code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey">
				  <input type="hidden" name="nextPage" value="<cfoutput>#page#</cfoutput>">
					<cfif databaseLive>
					<input type="hidden" name="Id" value="<cfoutput>#myID#</cfoutput>">
					</cfif> 
                  </td>
                </tr>
              </table>
            </blockquote>
			<hr>
          </cfform>	
		</td>
		</tr>
		</table>
<cfelse>

<!---
	* Ugly hack here because for some reason IM1 isnt updated with CFUPDATE

<cfif page EQ 2 AND databaseLive>
	<cfquery datasource="roi">
		UPDATE #myTable#_supplemental SET dass1=#Form.dass1# WHERE FK=#Form.FK#
	</cfquery>
</cfif>
--->
<cfif page GT 1 AND databaseLive>
	<cfupdate datasource="#mySource#" tablename="#myTable#">
</cfif>

<cfif page GT totalPages>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
		<td align="left" colspan="11">
			<h5><cfoutput>Questionnaire #deqCount# of #qCount#. Page #page# of #myPages#.</cfoutput> </h5>
		</td>
	</tr>
  <tr>
    <td colspan="8" align="center">
	<h3>Thank you for completing the questionnaire.</h3>
	<form action="../random_array.cfm">
	<cfoutput>
	  		<input type="hidden" name="num" value="0" />
			<input type="hidden" name="myTo" value="1" />
			<input type="hidden" name="myFrom" value="deq" />
				<input type="hidden" name="myId" value="#URL.myId#" />
				  </cfoutput>
    	  <input type="submit" value="Continue to the next Questionnaire">
	  </form>
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="ew.js"></script>
<script type="text/javascript">
<!--
function checkMyForm(myForm) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">

if (myForm.deq#i#[0]) {
 alert("array");
}
/*
			for (i=0; i < myForm.deq_#i#.length; i++) {
				if (myForm.deq_#i#[i].checked) {
					alert ("checked!");
					
				}
			}
		} else {
			alert ("unchecked");
		}
		*/
</cfloop>
</cfoutput>
return false;
}
//-->
</script>

<cfform method="post" onSubmit="return checkMyForm(this);">

<cfoutput>
<table border="0" width="600" align="center" cellpadding="1" cellspacing="0">
		
	<tr>
		<td align="left" colspan="11">
			<h5><cfoutput>Questionnaire #deqCount# of #qCount#. Page #page# of #myPages#.</cfoutput> </h5>
		</td>
	</tr>
  	  <tr valign="top"> 
        <td colspan="11"> <h3><cfoutput>#instructions#</cfoutput></h3></td>
      </tr>
      <tr> 
        <td colspan="12" align="right"> <div align="right" style="margin:0"><img src="scale.gif" width="586" height="49"></div></td>
      </tr>
	  <cfset row=0>
	  <cfloop from="#start#" to="#end#" step="1" index="i">
	   <cfset row=#row#+1>
       <tr <cfif #row# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>> 
	   	<td width="10" rowspan="2" valign="top">#i#.</td>
        <td width="45%" rowspan="2" valign="top"> #myQuestions[i]#</td>
		<td>&nbsp;</td>
        <td><div align="center"><font size="2">1</font></div></td>
        <td><div align="center"><font size="2">2</font></div></td>
        <td><div align="center"><font size="2">3</font></div></td>
        <td><div align="center"><font size="2">4</font></div></td>
        <td><div align="center"><font size="2">5</font></div></td>
        <td><div align="center"><font size="2">6</font></div></td>
        <td><div align="center"><font size="2">7</font></div></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>
      <tr <cfif #row# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>>
	    <td>&nbsp;</td> 
		<cfloop from="1" to="7" index="j">
		<td valign="top" width="41">
		<font size="2">
			<center>
			<input type="radio" name="deq_#i#" value="#j#" <cfif debug AND j EQ 1>checked</cfif>>
			</center>
		</font>
		</td>
  		</cfloop>
		  <input type=hidden name="deq_#i#_required" value= "You must enter a rating.">
        <td width="42">&nbsp;</td>
        <td width="42">&nbsp;</td>
	  </tr>
	  </cfloop>
  </cfoutput>
 
  <tr valign="top">
    <td colspan="12" align="center">
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
	<cfif databaseLive>
	<input type="hidden" name="Id" value="<cfoutput>#myID#</cfoutput>">
	</cfif>
	<input type="submit" name="Submit" value="   Continue   " style="width:150px">
    </td>
  </tr>
</table>
</cfform>
</cfif>
</cfif>
</body>
</html>
