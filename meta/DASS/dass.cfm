<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<cfset mySource = "watson">
<cfset myTable = "dass">
<cfset myID = -1>
<cfif isDefined("URL.myId") >
	<cfset myID = #URL.myId#> 
</cfif>
<cfinclude template="../debug.cfm">
<cfset databaseLive = true>



<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "I found myself getting upset by quite trivial things.">
	<cfset myQuestions[2] = "I was aware of dryness of my mouth.">
	<cfset myQuestions[3] = "I couldn't seem to experience any positive feeling at all.">
    <cfset myQuestions[4] = "I experienced breathing difficulty (e.g., excessively rapid breathing, breathlessness in the absence of physical exertion).">
	<cfset myQuestions[5] = "I just couldn't seem to get going.">
	<cfset myQuestions[6] = "I tended to over-react to situations.">
	<cfset myQuestions[7] = "I had a feeling of shakiness (e.g., legs going to give way).">
	<cfset myQuestions[8] = "I found it difficult to relax.">
	<cfset myQuestions[9]= "I found myself in situations that made me so anxious I was most relieved when they ended.">
	<cfset myQuestions[10]= "I felt that I had nothing to look forward to.">
	<cfset myQuestions[11]= "I found myself getting upset rather easily.">
	<cfset myQuestions[12]= "I felt that I was using a lot of nervous energy.">
	<cfset myQuestions[13]= "I felt sad and depressed.">
	<cfset myQuestions[14]= "I found myself getting impatient when I was delayed in any way (e.g., elevators, traffic lights, being kept waiting).">
	<cfset myQuestions[15]= "I had a feeling of faintness.">
	<cfset myQuestions[16]= "I felt that I had lost interest in just about everything.">
	<cfset myQuestions[17]= "I felt I wasn't worth much as a person.">
	<cfset myQuestions[18]= "I felt that I was rather touchy.">
	<cfset myQuestions[19]= "I perspired noticeably (e.g., hands sweaty) in the absence of high temperatures or physical exertion.">
	<cfset myQuestions[20]= "I felt scared without any good reason.">
	<cfset myQuestions[21]= "I felt that life wasn't worthwhile.">
	<cfset myQuestions[22]= "I found it hard to wind down.">
	<cfset myQuestions[23]= "I had difficulty in swallowing.">
	<cfset myQuestions[24]= "I couldn't seem to get any enjoyment out of the things I did.">
	<cfset myQuestions[25]= "I was aware of the action of my heart in the absence of physical exertion (e.g., sense of heart rate increase, heart missing a beat).">
	<cfset myQuestions[26]= "I felt down-hearted and blue.">
	<cfset myQuestions[27]= "I found that I was very irritable.">
	<cfset myQuestions[28]= "I felt I was close to panic.">
	<cfset myQuestions[29]= "I found it hard to calm down after something upset me.">
	<cfset myQuestions[30]= "I feared that I would be ""thrown"" by some trivial but unfamiliar task.">
	<cfset myQuestions[31]= "I was unable to become enthusiastic about anything.">
	<cfset myQuestions[32]= "I found it difficult to tolerate interruptions to what I was doing.">
	<cfset myQuestions[33]= "I was in a state of nervous tension.">
	<cfset myQuestions[34]= "I felt I was pretty worthless.">
	<cfset myQuestions[35]= "I was intolerant of anything that kept me from getting on with what I was doing.">
	<cfset myQuestions[36]= "I felt terrified.">
	<cfset myQuestions[37]= "I could see nothing in the future to be hopeful about.">
	<cfset myQuestions[38]= "I felt that life was meaningless.">
	<cfset myQuestions[39]= "I found myself getting agitated.">
	<cfset myQuestions[40]= "I was worried about situations in which I might panic and make a fool of myself.">
	<cfset myQuestions[41]= "I experienced trembling (e.g., in the hands).">
	<cfset myQuestions[42]= "I found it difficult to work up the initiative to do things.">


<cfset page = 1>
<cfset qPerPage = 7>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<!---
For compatibility with older surveys, set myIndex and myPages --->
<cfset myIndex = page + 1>
<cfset myPages = totalPages + 1>
<!--- end compability measures --->
<cfset title = "DASS">
<cfset instructions = "Please read each statement and choose a number 0, 1, 2 or 3 that indicates how much the statement applied to you <i>over the past week</i>.">
<cfset instructions = instructions & "  There are no right or wrong answers.  Do not spend too much time on any statement.">
<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>

<cfif isDefined("URL.myId") AND NOT isDefined("Form.Id")>
			<cfquery datasource="#mySource#" name="getVals">
				select Code, Gender from riopc where Id = '#URL.myId#'
			</cfquery>
			<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  	<cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  	<cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  	<cfset finalDate = #todayFormatDate# & #todayFormatTime#>
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable#(Id, Code, Gender, datecreated) VALUES ('#URL.myId#', '#getVals.Code#', '#getVals.Gender#', '#finalDate#')  
				</cfquery>
			  </cftransaction>
			</cflock> 
			<cfset myID = #URL.myID#> 
	
</cfif>

<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>

<cfif false>
<center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">

    <tr valign="top"> 
      <td height="20"> 
        <h5> 
          Introduction for Researcher
	    </h5></td>
    </tr>
    <tr> 
      <td valign="middle"> 
        
          <h3>DASS
            </td>
    </tr>
    <tr> 
      <td height="21"> <hr></td>

    </tr>
    <tr valign="top"> 
      <td valign="top"> 
	 <cfform action="dass.cfm" method="post">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="50%"><strong>Participant 3-digit code:</strong></td>
                  <td width="47%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Survey">
				  <input type="hidden" name="nextPage" value="<cfoutput>#page#</cfoutput>">
				
					<input type="hidden" name="Id" value="<cfoutput>#myID#</cfoutput>">
				 
                  </td>
                </tr>
              </table>
            </blockquote>
			<hr>
          </cfform>	
		</td>
		</tr>
		</table>
<cfelse>

<!---
	* Ugly hack here because for some reason IM1 isnt updated with CFUPDATE

<cfif page EQ 2 AND databaseLive>
	<cfquery datasource="roi">
		UPDATE #myTable#_supplemental SET dass1=#Form.dass1# WHERE FK=#Form.FK#
	</cfquery>
</cfif>
--->
<cfif page GT 1>
	<cfupdate datasource="#mySource#" tablename="#myTable#">
</cfif>

<cfif page GT totalPages>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8" align="center">
	<div align='left'><h5>      
	  <cfoutput>Questionnaire #dassCount# of #qCount#. Page #page# of #myPages#.</cfoutput>
	</h5></div>
	<h3>Thank you for completing this questionnaire.</h3>
	<form action="../random_array.cfm">
	<cfoutput>
	  		<input type="hidden" name="num" value="0" />
			<input type="hidden" name="myTo" value="1" />
			<input type="hidden" name="myFrom" value="DASS/dass.cfm" />
				<input type="hidden" name="myId" value="#myId#" />
				  </cfoutput>
    	  <input type="submit" value="Continue to the next Questionnaire">
	  </form>
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="ew.js"></script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">
if (EW_this.dass#i# && !EW_hasValue(EW_this.dass#i#, "RADIO" )) {
	if (!EW_onError(EW_this, EW_this.dass#i#, "RADIO", "Please enter a response for Question #i#."))
		return false;
}
</cfloop>
</cfoutput>
return true;
}
//-->
</script>

<cfform method="post" onSubmit="return EW_checkMyForm(this);">

<cfoutput>
<table border="0" width="800" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td colspan="6">
	<h5>      
	  <cfoutput>Questionnaire #dassCount# of #qCount#. Page #page# of #myPages#.</cfoutput>
	</h5>
	<h3><cfoutput>#instructions#</cfoutput></h3></td>
  </tr>
  <tr>
  	<td></td>
    <td></td>
	<cfloop from="1" to="4" index="m">
	<td bgcolor="##E1E1E1">
		<center><img src="#m#.png"></center> 
    </td>
	</cfloop>
  </tr>
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr <!---<cfif #i# mod 2 NEQ 0>bgcolor="##FFFFCC"</cfif>--->>
  	<td valign="top">#i#.</td>
	<input type=hidden name="dass#i#_required" value= "You must enter a rating.">
	<td valign="top" width="400">#myQuestions[i]#</td>
	<cfloop from="0" to="3" index="j">
		<td valign="center" bgcolor="##E1E1E1">
		<font size="2">
			<center>#j#<BR>
			<input type="radio" name="dass#i#" value="#j#" <cfif debug EQ true AND j EQ 0>checked</cfif>>
			</center>
		</font>
		</td>
  	</cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
    <td colspan="6" align="center">
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
	<cfif databaseLive>
	<input type="hidden" name="Id" value="<cfoutput>#myID#</cfoutput>">
	</cfif>
	<input type="submit" name="Submit" value="   Continue   ">
    </td>
  </tr>
</table>
</cfform>
</cfif>
</cfif>
</body>
</html>
