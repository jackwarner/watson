<!---<cfif IsDefined("cookie.npwatscookie") >
<cfelse>
	<cflocation url="../../login.cfm">
</cfif>--->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<cfset mySource = "watson">
<cfset myTable = "bai">
<cfset databaseLive = true>
<cfset myID = -1>
<cfif isDefined("URL.myId") >
	<cfset myID = #URL.myId#> 
</cfif>
<cfinclude template="../debug.cfm">

<cfset myQuestions=ArrayNew(1)>
	<cfset myQuestions[1] = "Numbness or tingling.">
	<cfset myQuestions[2] = "Feeling hot.">
	<cfset myQuestions[3] = "Wobbliness in legs.">
    <cfset myQuestions[4] = "Unable to relax.">
	<cfset myQuestions[5] = "Fear of the worst happening.">
	<cfset myQuestions[6] = "Dizzy or lightheaded.">
	<cfset myQuestions[7] = "Heart pounding or racing.">
	<cfset myQuestions[8] = "Unsteady.">
	<cfset myQuestions[9] = "Terrified.">
	<cfset myQuestions[10]= "Nervous.">
	<cfset myQuestions[11]= "Feelings of choking.">
	<cfset myQuestions[12]= "Hands trembling.">
	<cfset myQuestions[13]= "Shaky.">
	<cfset myQuestions[14]= "Fear of losing control.">
	<cfset myQuestions[15]= "Difficulty breathing.">
	<cfset myQuestions[16]= "Fear of dying.">
	<cfset myQuestions[17]= "Scared.">
	<cfset myQuestions[18]= "Indigestion or discomfort in abdomen.">
	<cfset myQuestions[19]= "Faint.">
	<cfset myQuestions[20]= "Face flushed.">
	<cfset myQuestions[21]= "Sweating (not due to heat).">

<cfset page = 1>


<cfset qPerPage = 11>
<cfset totalQuestions = ArrayLen(myQuestions)>
<cfset totalPages = Ceiling(totalQuestions / qPerPage)>
<cfif isDefined("Form.nextPage")>
	<cfset page = #Form.nextPage#>
</cfif>
<cfif page EQ 1>
			<cfquery datasource="#mySource#" name="getVals">
				select Code, Gender from riopc where Id = '#URL.myId#'
			</cfquery>
			<cflock name="#CreateUUID()#" timeout="20">
					<cfset todayDate = Now()>
				  	<cfset todayFormatDate = #DateFormat(todayDate, "yyyy-mm-dd ")#>
				  	<cfset todayFormatTime = #TimeFormat(todayDate, "HH:mm:ss")#>
				  	<cfset finalDate = #todayFormatDate# & #todayFormatTime#>
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				INSERT INTO #myTable#(Id, Code, Gender, datecreated) VALUES ('#URL.myId#', '#getVals.Code#', '#getVals.Gender#', '#finalDate#')  
				</cfquery>
			
			  </cftransaction>
			</cflock> 
			<cfset myID = #URL.myID#>
</cfif>
<!---
For compatibility with older surveys, set myIndex and myPages --->
<cfset myIndex = page + 1>
<cfset myPages = totalPages + 1>
<!--- end compability measures --->
<cfset title = "BAI">
<cfset instructions = "Below is a list of common symptoms of anxiety.  Please carefully read each item in the list.  ">
<cfset instructions = instructions & "Indicate how much you have been bothered by each symptom during the PAST WEEK, INCLUDING TODAY, in the column next to each symptom.">
<cfset start = (qPerPage * page) - (qPerPage - 1)>
<cfset end = qPerPage * page>
<cfif totalQuestions LT end>
	<cfset end = totalQuestions>
</cfif>
<title><cfoutput>#title#, Questions #start#-#end#</cfoutput>.</title>
</head>

<body>
<cfif page GT 1 AND databaseLive>
	<cfupdate datasource="#mySource#" tablename="#myTable#">
</cfif>
<!---
	* Ugly hack here because for some reason IM1 isnt updated with CFUPDATE

<cfif databaseLive>
	<cfquery datasource="#mySource#">
		UPDATE #myTable# SET bai1=#Form.bai1# WHERE FK=#Form.FK#
	</cfquery>
</cfif>

<cfif page GT totalPages AND databaseLive>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8">
	<cfinclude template="../getcode_bai.cfm">
	</td>
  </tr>
</table>
--->
<cfif page GT totalPages>
<table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="8" align="center">
	<div align="left">
	<h5>      
	  <cfoutput>Questionnaire #baiCount# of #qCount#. Page #page# of #myPages#.</cfoutput>
	</h5>
	</div>
	<cfif baiCount EQ qCount>
	<h3>Thank you for completing the questionnaires.<br>
	Please let the researcher know that you have finished.</h3>
		 <input type="button" value="Close the Questionnaire" name="close" onClick="window.close()">
	<cfelse>
	<h3>Thank you for completing this questionnaire.</h3>
	<form action="../random_array.cfm">
	<cfoutput>
	  		<input type="hidden" name="num" value="0" />
			<input type="hidden" name="myTo" value="1" />
			<input type="hidden" name="myFrom" value="BAI/bai.cfm" />
				<input type="hidden" name="myId" value="#URL.myId#" />
				  </cfoutput>
    	  <input type="submit" value="Continue to the next Questionnaire">
	  </form>
	  </cfif>
	</td>
  </tr>
</table>
<cfelse>
<script language="JavaScript" src="ew.js"></script>
<script type="text/javascript">
<!--
function EW_checkMyForm(EW_this) {

<cfoutput>
<cfloop from="#start#" to="#end#" step="1" index="i">
if (EW_this.bai#i# && !EW_hasValue(EW_this.bai#i#, "RADIO" )) {
	if (!EW_onError(EW_this, EW_this.bai#i#, "RADIO", "Please enter a response for Question #i#."))
		return false;
}
</cfloop>
</cfoutput>
return true;
}
//-->
</script>

<form method="post" onSubmit="return EW_checkMyForm(this);">
<table width="800" border="0" align="center" cellpadding="5" cellspacing="5">
  <tr>
    <td colspan="8">
	<h5 style="margin-top:-5px">      
	  <cfoutput>Questionnaire #baiCount# of #qCount#. Page #page# of #myPages#.</cfoutput>
	</h5>
	<img src="bai_logo.gif" width="115" height="66"><br>
      <hr>
      <cfoutput><font size="2" face="Arial, Helvetica, sans-serif">#instructions#</font></cfoutput></td>
  </tr>
  <cfoutput>
  <tr>
  	<td></td>
    <td width="300">&nbsp;</td>
	<cfloop from="0" to="3" index="m">
	<td bgcolor="##E1E1E1">
	<center><img src="#m#.gif"></center> 
   	 </td>
	</cfloop>
    </tr>
  
  <cfloop from="#start#" to="#end#" step="1" index="i">
  <tr>
  	<td valign="top"><b>#i#.</b></td>
  	<td width="300" valign="top"><font size="3"><b>#myQuestions[i]#</b></font></td>
 	<cfloop from="1" to="4" index="k">
	<td bgcolor="##E1E1E1"><div align="center"> <font size="2">
   	     <input type="radio" name="bai#i#" value="#k#" <cfif debug and k EQ 1>checked</cfif>>
   	 </font></div></td>
	</cfloop>
  </tr>
  </cfloop>
  </cfoutput>
 
  <tr valign="top">
  	<td></td>
    <td height="43">&nbsp;</td>
    <td colspan="7"><div align="left"> <br>
	<input type="hidden" name="nextPage" value="<cfoutput>#page+1#</cfoutput>">
	<cfif databaseLive>
	<input type="hidden" name="Id" value="<cfoutput>#myID#</cfoutput>">
	</cfif>
	<input type="submit" name="Submit" value="Continue">   
    </div></td>
  </tr>
   <tr valign="top"> 
      <td colspan="5">
	  <!---
	  Beck Anxiety Inventory. Copyright [copyright symbol] 1987 by Aaron T. Beck. Reproduced with permission. All rights reserved.
	  --->
	   <div align="left"><font size="1"><em>
	  Beck Anxiety Inventory.</em> Copyright &copy; 1987 by Aaron T. Beck. Reproduced with permission. All rights reserved.<br>
          <br>
          <em>&quot;Beck Anxiety Inventory&quot;</em> and <em>&quot;BAI&quot; 
          </em>are trademarks of Harcourt Assessment, Inc. registered in the United 
          States of America and/or other jurisdictions.</font>
	   </div>
		</td>
    </tr>
</table>
</form>
</cfif>
</body>
</html>
