<html>
<head>
<title>RIOPC</title>
</head>
<cfset myTable = "riopc">
<cfset mySource = "watson">
<cfset myIndex = #URL.num#>
<cfset myInclude = #URL.myTo#>
<cfset myID = #URL.myID#>
<cfinclude template="../debug.cfm">
<!--- If this is fer real, go ahead and update the database 
myPages should be a CONSTANT!!!!--->
<cfset myPages = 21>
<body><center>

  <table width="585" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top"> 
      <td height="17"> <h5>
          <cfif #myIndex# EQUAL 0>Begin the questionnaires for the Study of Self-Concept and Affect.
		  <cfelseif #myIndex# GT #myPages#>
	  <cfelse>
	  <cfoutput>Questionnaire 1 of #qCount#.  Page #myIndex# of #myPages#.</cfoutput>
	  </cfif></h5></td>
    </tr>
   <!--- <tr> 
      <td height="10" valign="middle"> 
        <cfif myIndex EQUAL 0>
          <p>This program allows a researcher to 
            measure the respondent's REAL, IDEAL, and OUGHT (other) components 
            of self-concept.</p>
          <p>The program<br>
            <blockquote>1. elicits six personality characteristics for each of 
              these three components of self-concept,<br>
              2. elicits the opposite of each of the 18 characteristics, and<br>
              3. obtains ratings of all 36 characteristics in response to instructions 
              for each of the three components of self-concept. <BR><BR>These characteristics 
              are presented in the same random order for ratings for each of the 
              three components of self-concept.</blockquote>
            <cfelse></p>
          </cfif></td>
    </tr>
   --->
    <tr valign="top"> 
      <td> <cfif myIndex EQUAL 0>
          <cfform action="frame_.cfm?num=1&myID=1&myTo=#myInclude#">
            <blockquote> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="58%"><strong>Participant code:</strong></td>
                  <td width="42%"><cfinput name="Code" type="text" maxlength="3"></td>
                </tr>
                <!---<tr>
                  <td><strong>Choose from the following elicitation orders:</strong>&nbsp;</td>
                  <td>	<select name="Order">
						  <option value="1">R/I/O
						  <option value="2">R/O/I
						  <option value="3">O/R/I
						  <option value="4">O/I/R
						  <option value="5">I/O/R
						  <option value="6">I/R/O
						</select>&nbsp;</td>
                </tr>--->
                <tr> 
                  <td><strong>Participant gender: </strong></td>
                  <td><input type="radio" name="Gender" value="M">
                    Male 
                    <input type="radio" value="F" name="Gender">
                    Female </td>
                </tr>
                <tr> 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr> 
                  <td><input type="hidden" name="Code_required" value="Please enter a 3 digit code."> 
                    <input type="hidden" name="Gender_required" value="Please enter a gender."> 
                  </td>
                  <td><input name="submit" type="submit" value="Begin the Questionnaires"> 
                  </td>
                </tr>
              </table>
            </blockquote>
          </cfform>
          <hr>
          <font size="1">Copyright 2004<br>
          Neill Watson, Ph.D.<br>
          College of William &amp; Mary<br>
          <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font> 
          <!---  <cfelseif myIndex LESS THAN 4>
        <h3><cfoutput>#myIndex#. #aTitles[myIndex]#</cfoutput></h3></td>--->
        </cfif> 
	  </td>
	</tr>
	
    <tr valign="top"> 
      <td> 
	  
	  <cfif myIndex EQUAL 1> 
	  
		  <cfif Len(#Form.Code#) NOT EQUAL 3> 
			<a href="javascript:history.go(-1)">Please enter a 3 digit researcher 
			code. Click to go back.</a> <cfelse> 
			<cflock name="#CreateUUID()#" timeout="20">
			  <cftransaction>
				<cfquery name="myQ" datasource="#mySource#">
				
				<cfset myNumPick = -1>
				
				<cfset participant = Form.code MOD 24>
				<cfif participant EQ 0>
					<cfset participant = 24>
				</cfif>
				
				<cfif participant EQ 1 OR participant EQ 10 OR participant EQ 14 OR participant EQ 24>
					<cfset myNumPick = 5>
				<cfelseif participant EQ 2 OR participant EQ 12 OR participant EQ 18 OR participant EQ 23>
					<cfset myNumPick = 1>	
				<cfelseif participant EQ 3 OR participant EQ 5 OR participant EQ 16 OR participant EQ 20>
					<cfset myNumPick = 4>	
				<cfelseif participant EQ 4 OR participant EQ 7 OR participant EQ 11 OR participant EQ 21>
					<cfset myNumPick = 6>	
				<cfelseif participant EQ 6 OR participant EQ 13 OR participant EQ 17 OR participant EQ 19>
					<cfset myNumPick = 2>
				<cfelseif participant EQ 8 OR participant EQ 9 OR participant EQ 15 OR participant EQ 22>
					<cfset myNumPick = 3>	
				<cfelse>
					<cfoutput>Not yet coded!</cfoutput>
				</cfif>
				
				INSERT INTO #myTable#(Code, Gender, numpick) VALUES ('#Form.Code#', '#Form.Gender#', '#myNumPick#') 
				</cfquery>
				<CFQUERY name="getMaxID" datasource="#mySource#">
				SELECT Max(Id) AS myID FROM #myTable# 
				</CFQUERY>
			  </cftransaction>
			</cflock> 
			<cfset myID = #getMaxID.myID#> 
			 
			<cfinclude template="#myIndex#.cfm"> &nbsp;
		  
		  
	  </cfif> 
	  
	  <cfelseif myIndex GREATER THAN 1>     
        	<cfinclude template="#myInclude#.cfm">
      </cfif> 
	  </td>
	  </tr>
  </table>
</center>
</body>
</html>