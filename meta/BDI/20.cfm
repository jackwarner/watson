<!--- variable dSource found in questions.cfm --->
<cfquery name="bdi" datasource="#dSource#">
<cfset mUp = myIndex - 1>
<!--- From.qX where x should be one less than current file Y.cfm --->
<cfset myUpdate = "q" & mUp>
UPDATE #myTable#
	SET #myUpdate# = '#Form.q19#'
	WHERE ID = #myID#
</cfquery>

<table width="585" height="20" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="73" height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="0">
      0</strong></td>
    <td width="529" colspan="7" bgcolor="#FFFFCC">
	<strong>
	I am no more tired or fatigued than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1 </strong></td>
    <td colspan="7"><strong> 
	I get more tired or fatigued more easily than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2 </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong>
	I am too tired or fatigued to do a lot of the things I used to do.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3 </strong></td>
    <td colspan="7"><strong>
	I am too tired or fatigued to do most of the things I used to do.
	</strong>
	<input type="hidden" name="<cfoutput>q#myIndex#</cfoutput>_required" value= "<cfoutput>#ERROR#</cfoutput>">
	</td>
  </tr>
    <tr> 
    <td height="19"><strong> 
      &nbsp;</td>
    <td colspan="7"><strong><input type="submit" value="Continue"></strong></td>
  </tr>
</table>
