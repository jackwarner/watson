<!--- variable dSource found in questions.cfm --->
<cfquery name="bdi" datasource="#dSource#">
<cfset mUp = myIndex - 1>
<!--- From.qX where x should be one less than current file Y.cfm --->
<cfset myUpdate = "q" & mUp>
UPDATE #myTable#
	SET #myUpdate# = '#Form.q17#'
	WHERE ID = #myID#
</cfquery>

<table width="585" height="20" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="73" height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="0">
      0</strong></td>
    <td width="529" colspan="7" bgcolor="#FFFFCC">
	<strong>
	I have not experienced any change in my appetite.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1a </strong></td>
    <td colspan="7"><strong> 
	My appetite is somewhat less than usual.
	</strong></td>
  </tr>
    <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="1">
      1b </strong></td>
    <td colspan="7"><strong> 
	My appetite is somewhat greater than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2a </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong>
	My appetite is much less than before.
	</strong></td>
  </tr>
    <tr> 
    <td height="19" valign="top" bgcolor="#FFFFCC"> <strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="2">
      2b </strong></td>
    <td colspan="7" bgcolor="#FFFFCC"><strong>
	My appetite is much greater than usual.
	</strong></td>
  </tr>
  <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3a </strong></td>
    <td colspan="7"><strong>
	I have no appetite at all.
	</strong>
	
	</td>
  </tr>
    <tr> 
    <td height="19" valign="top"><strong> 
      <CFINPUT TYPE="radio" NAME="q#MyIndex#" value="3">
      3b </strong></td>
    <td colspan="7"><strong>
	I crave food all the time.
	</strong>
	<input type="hidden" name="<cfoutput>q#myIndex#</cfoutput>_required" value= "<cfoutput>#ERROR#</cfoutput>">
	</td>
  </tr>
    <tr> 
    <td height="19"><strong> 
      &nbsp;</td>
    <td colspan="7"><strong><input type="submit" value="Continue"></strong></td>
  </tr>
</table>
