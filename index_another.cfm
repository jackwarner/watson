
<html>
<head>
<title>Survey Application, Neill Watson</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
a.special:link {
     color: #000000;
     }
a.special:visited {
     color: #000000;
}-->
</style>
</head>

<body link="#0000FF" vlink="#0000FF" alink="#0000FF">
<table width="100%" border="0">
  <tr valign="middle"> 
    <td colspan="2"> <h2><font color="#0000FF">WELCOME TO THE RESEARCH SITE OF<br>
        NEILL WATSON, PH.D.&nbsp;<font color="#000000" size="3"> <a href="http://www.wm.edu/psyc/cvWATSON0105.pdf" class="special">CV</a></font><br>
        THE COLLEGE OF WILLIAM &amp; MARY</font></h2></td>
  </tr>
  <tr> 
    <td colspan="2"><hr></td>
  </tr>
  <tr> 
    <td colspan="2"><h3>Select one of the following questionnaires to view, or 
        download a PDF copy of the study of the reliability and validity of the 
        questionnaires.<br>
        <font color="#FF0000"> 
        <script language="JavaScript1.2">
<!-- Original:  Craig Lumley -->
<!-- Web Site:  http://www.craiglumley.co.uk -->

<!-- This script and many more are available free online at -->
<!-- The JavaScript Source!! http://javascript.internet.com -->

<!-- Begin
<!-- Begin
function fullScreen(theURL) {
window.open(theURL, '', 'fullscreen=yes, scrollbars=yes');
}
//  End -->

var bestwidth = 1024;
var bestheight = 768;
if (screen.width < bestwidth || screen.height < bestheight) {
msg = "<hr>The surveys on this site are intended to be administered on systems with a screen resolution of at least"
+ bestwidth + "x" + bestheight + ". Your"
+ " screen resolution is " + screen.width + "x"
+ screen.height + ".  Please change your screen resolution "
+ "if possible.  You will not see this message if you screen is set to the proper resolution.";
document.write(msg);
}
//  End -->
</script>
        </font></h3></td>
  </tr>
  <tr valign="top"> 
    <td colspan="2"> <hr></td>
  </tr>
  <tr> 
    <td rowspan="2" valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3><a href="javascript:fullScreen('nodb/RIOPC/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Self-Concept 
        Questionnaire - Personal Constructs</a> </h3></td>
  </tr>
  <tr> 
    <td><blockquote> 
        <h4>An idiographic measure of real self, ideal self, and ought self components 
          of self-concept, from which self-discrepancies are computed.</h4>
      </blockquote></td>
  </tr>
  <tr> 
    <td width="9%" rowspan="2" valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td width="91%"><h3><a href="javascript:fullScreen('nodb/RIOCC/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Self 
        Concept Questionnaire - Conventional Constructs</a> </h3></td>
  </tr>
  <tr> 
    <td><blockquote> 
        <h4>A non-idiographic measure of real self, ideal self, and ought self 
          components of self-concept, from which self-discrepancies are computed.</h4>
      </blockquote></td>
  </tr>
  <tr> 
    <td rowspan="2" valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3> <a href="javascript:fullScreen('nodb/AbstractRI/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Real-Ideal 
        Discrepancy Abstract Measure</a></h3></td>
  </tr>
  <tr> 
    <td><blockquote> 
        <h4>A very brief, content-free measure of the discrepancy between real 
          self and ideal self.</h4>
      </blockquote></td>
  </tr>
  <tr> 
    <td rowspan="2" valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3> <a href="javascript:fullScreen('nodb/AbstractRO/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');"> 
        Real-Ought Discrepancy Abstract Measure</a></h3></td>
  </tr>
  <tr> 
    <td><blockquote>
        <h4>A very brief, content-free measure of the discrepancy between real 
          self and ought self.</h4>
      </blockquote></td>
  </tr>
  <tr> 
    <td valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3><a href="javascript:fullScreen('nodb/Ideal/frame.cfm?num=0&myID=0&mySkip=x&myTo=1.cfm');">Importance 
        of Ideal Self</a></h3></td>
  </tr>
  <tr> 
    <td valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3><a href="javascript:fullScreen('nodb/Ought/frame.cfm?num=0&myID=0&mySkip=x&myTo=1');">Importance 
        of Ought Self</a></h3></td>
  </tr>
  <tr> 
    <td valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3><a href="javascript:fullScreen('nodb/RefPerson/frame.cfm?num=0&myID=0&myTo=0&mySkip=x');">Reference 
        Person(s) for Ought Self</a> </h3></td>
  </tr>
  <tr> 
    <td colspan="2" valign="top">
<hr></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td><h3><a href="babel-merged.pdf">Reliability and Validity of  Three
    Methods for Measuring Self-Discrepancy</a></h3></td>
  </tr>
  <tr> 
    <td valign="top">
<div align="center"></div></td>
    <td><h3>Related Papers<br>
    </h3></td>
  </tr>
  <tr> 
    <td colspan="2" valign="top">
<hr></td>
  </tr>
  <tr> 
    <td valign="top">
<div align="center"><img src="formatted/survey.jpg" width="30" height="38"></div></td>
    <td><h3><a href="formatted/auth.cfm">Authorized Researchers Only</a></h3></td>
  </tr>
  <tr> 
    <td colspan="2"><hr></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td><font size="1">Copyright 2004<br>
      Neill Watson, Ph.D.<br>
      College of William &amp; Mary<br>
      <a href="mailto:npwats@wm.edu"><font color="#000066">npwats@wm.edu</font></a></font></td>
  </tr>
</table>

</body>
</html>
